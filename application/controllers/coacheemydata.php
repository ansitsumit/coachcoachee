<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class coacheemydata extends CI_Controller {
      function __construct()
    {
        //session_start();
    // Call the Model constructor
        parent::__construct();
		$this->load->model('coachee_model');
		$this->load->model('AddNewCoach_model');
		$this->load->model('button_model');
	$this->load->model('model_data');
     $this->model_data->session_expire_redirect("2");	
		$this->load->model('insert_model');
	$this->load->model('language_model');
$current_lang=$this->language_model-> get_coachee_current_language();


if($current_lang[0]->name!=""){
$this->lang=$current_lang[0]->name;
}else{
    $this->lang="default";
    
}	
    }

	
	public function index($id="")
	{ 
	$data['lang']=$this->language_model->get_languages($this->lang);

	$data['total_score']=$this->AddNewCoach_model->total_Activities("select sum(points) as total   from  log_gamification where idcoachee='".$_SESSION['coach']."'");

	if(isset($_POST['coachee']))
	{
	
	$this->coachee_model->update_coacheemydata($_SESSION['coach']);
	$data['msg']=$this->model_data->update_msg();
	
	
	}
	
	if($id!=""){
	$data['button']=$this->button_model->coachee_update_button();
	$data['get_data_coachee']=$this->AddNewCoach_model->get_data_coach($id);
	
	}else{
	$data['button']=$this->button_model->coachee_update_button();
	
	}
	$this->load->model('sidebar/side_model');
	$data['side']=$this->side_model->coachee($this->lang);
	$data['civil_staes']=$this->model_data->get_civilstates();
	$data['hierachi']=$this->model_data->get_hierachical();
	$data['edu']=$this->model_data->get_education();
		
	$data['edu']=$this->model_data->get_education();
	$data['country']=$this->model_data->get_country();	
    $data['Sessions'] = $this->coachee_model->get_sessions();
   $data['get_data_coachee']=$this->AddNewCoach_model->get_data_coach($_SESSION['coach']);
	
	$image['title']='My Data';
	$image['im']=$current_lang=$this->language_model->get_coachee_current_language();
$this->load->view('header',$image);

	$this->load->view('coacheemydata',$data);
	$this->load->view('footer');
	
		}
	
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */