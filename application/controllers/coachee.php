<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class coachee extends CI_Controller {
var $lang="";
function __construct()
{
parent::__construct();
$this->load->model('coachee_model');
$this->load->model('AddNewCoach_model');
$this->load->model('button_model');
$this->load->model('model_data');
$this->model_data->session_expire_redirect("1");	
$this->load->model('insert_model');
$this->load->model('language_model');
$current_lang=$this->language_model->get_current_language();
if($current_lang[0]->name!=""){
    
$this->lang=$current_lang[0]->name;
}else{
    $this->lang="default";
     }
}


public function index($id="")
{
	if($id >0){
		$data['pageheader'] ='Update Coachee';
	}else{
		$data['pageheader'] ='Add New Coachee';
	}
	$image['title'] = 'New Coachee';
	$data['wheelinfo']=$this->AddNewCoach_model->total_Activities("select * from wheellife where IDCoachee='".$id."'");

$data['total_compelete_session']=$this->AddNewCoach_model->total_session("select count(*) as total  from sessions where IDCoachee='".$id."' and complete='Yes'");

 $data['lang']=$this->language_model->get_languages($this->lang);
if(isset($_POST['coachee']))
{
if(!empty($id) || $id>0){


$this->coachee_model->update_coachee($id);

$data['msg']=$this->model_data->update_msg();
}
else{

$s=$this->insert_model->check_register($_POST['Email1']);

if(count($s)==0){

$this->coachee_model->coachee($this->lang);
$data['msg']=$this->model_data->submit_msg();
$this1->idcoach=$_SESSION['coach'];
$this1->date=date("Y-m-d");
$this1->points="300";
$this1->hour=date("H:i:s");
$this->db->insert("log_gamification",$this1);

}
else{

$data['msg']=$this->model_data->email_allready_msg();
}
}
}

if($id!=""){
$data['button']=$this->button_model->coachee_update_button();
$data['get_data_coachee']=$this->AddNewCoach_model->get_data_coach($id);

}else{
$data['button']=$this->button_model->coachee_Add_button();

}
$this->load->model('sidebar/side_model');
$data['side']=$this->side_model->coach($this->lang);
$data['civil_staes']=$this->model_data->get_civilstates();
$data['hierachi']=$this->model_data->get_hierachical();
$data['edu']=$this->model_data->get_education();

$data['edu']=$this->model_data->get_education();
$data['country']=$this->model_data->get_country();	
$data['Sessions'] = $this->coachee_model->get_sessions();

$data['total_score']=$this->AddNewCoach_model->total_Activities("select sum(points) as total   from  log_gamification where idcoachee='".$id."'");

$data['total_coach']=$this->AddNewCoach_model->total_Activities("select count(*) total from coachee where IDCoach='".$_SESSION['coach']."'");
$data['credit']=$this->AddNewCoach_model->total_Activities("select Credits from coach where  	IDCoach='".$_SESSION['coach']."'");

$image['im']=$current_lang=$this->language_model->get_current_language();
$this->load->view('header',$image);

$this->load->view('Mydata',$data);
$this->load->view('footer');

}



}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */