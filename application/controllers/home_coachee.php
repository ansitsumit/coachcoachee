<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class home_coachee extends CI_Controller {


public function __construct()
{

parent::__construct();
$this->load->model('model_data');
$this->model_data->session_expire_redirect("2");	
$this->load->model('AddNewCoach_model');
$this->load->model('sidebar/side_model');
$image['im']=$current_lang=$this->language_model->get_coachee_current_language();
$image['title'] = 'Home';
$this->load->view('header',$image);

$this->load->model('language_model');
$current_lang=$this->language_model-> get_coachee_current_language();


if($current_lang[0]->name!=""){
$this->lang=$current_lang[0]->name;
}else{
    $this->lang="default";
    
}


// Your own constructor code
}
public function index($a="",$b="")
{
$data['side']=$this->side_model->coachee($this->lang);
$data['lang']=$this->language_model->get_languages($this->lang);

$data['total_activitiest']=$this->AddNewCoach_model->total_Activities("select count(*) as total  from activities where IDCoachee='".$_SESSION['coach']."'");

$data['total_activitiestcomplete']=$this->AddNewCoach_model->total_Activities("select count(*) as total,IDAtividade  from activities where IDCoachee='".$_SESSION['coach']."' and DataEntrega <='".date("Y-m-d")."' and DataEntrega!='0000-00-00'");

$list_activitiestcomplete=$this->AddNewCoach_model->total_Activities("select IDAtividade  from activities where IDCoachee='".$_SESSION['coach']."' and DataEntrega <='".date("Y-m-d")."' and DataEntrega!='0000-00-00'");

$this->model_data->save_activity_score($list_activitiestcomplete);


$data['total_goalcomplete']=$this->AddNewCoach_model->total_Activities("select IDgoal  from  goals where IDCoachee='".$_SESSION['coach']."'");
$data["list_Activities"]=$this->model_data->get_total_list("select *  from activities where IDCoachee='".$_SESSION['coach']."'");

$data["list_goal"]=$this->model_data->get_total_list("select * from goals where IDCoachee='".$_SESSION['coach']."'");


$data["list_session"]=$this->model_data->get_total_list("select * from sessions where IDCoachee='".$_SESSION['coach']."'");

$data['complete_goals']=$this->model_data->get_total_complete_goal($data['total_goalcomplete'],$data["list_Activities"]);



$data['total_score']=$this->AddNewCoach_model->total_Activities("select sum(points) as total   from  log_gamification where idcoachee='".$_SESSION['coach']."'");


$data['total_goals']=$this->AddNewCoach_model->total_goals("select count(*) as total  from goals where IDCoachee='".$_SESSION['coach']."'");
$data['total_session']=$this->AddNewCoach_model->total_session("select count(*) as total  from sessions where IDCoachee='".$_SESSION['coach']."'");
$data['total_compelete_session']=$this->AddNewCoach_model->total_session("select count(*) as total  from sessions where IDCoachee='".$_SESSION['coach']."' and complete='Yes'");


$this->load->view("Dashboard1",$data);



$this->load->view('footer');


}


function Activity($id){
$data["goalw"]=$this->model_data->get_total_list("select * from goals where IDCoachee='".$_SESSION['coach']."'");


$data['side']=$this->side_model->coachee($this->lang);
$data['lang']=$this->language_model->get_languages($this->lang);

$data['goal_data'] = $this->model_data->get_Activities_data($id);
$data['cancel'] = "index.php/home_coachee/";
$data['button']="no";
$data['view']="view";
$this->load->view('Activity',$data);
      
$this->load->view('footer');
    
}
function Goal($id){
$data['side']=$this->side_model->coachee($this->lang);
$data['lang']=$this->language_model->get_languages($this->lang);

$data['goal_data'] = $this->model_data->get_goal_data($id);
$data['cancel'] = "index.php/home_coachee/";
$data['button']="no";
$this->load->view('goal',$data);
      
$this->load->view('footer');
    
}

function session($id){
$data['side']=$this->side_model->coachee($this->lang);
$data['lang']=$this->language_model->get_languages($this->lang);

$data['goal_data'] = $this->model_data->get_session_data($id);

$data['cancel'] = "index.php/home_coachee/";
$data['button']="no";
$this->load->view('session',$data);
      
$this->load->view('footer');
    
}

  public function download_file($file) {
//print_r($_GET);
      header("Content-type: application/octet-stream");
        header("Content-Disposition: filename=\"" . $path_parts["basename"] . "\"");
    }
}
