<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class log_book extends CI_Controller {
var $lang="";

public function __construct()
{

parent::__construct();
$this->load->model('model_data');
$this->model_data->session_expire_redirect("1");	
$this->load->model('query_model');

$this->load->model('coachee_model');
$this->load->model('AddNewCoach_model');
$this->load->model('test/test_model');
$this->load->model('language_model');
$this->load->model('log_model');
$this->load->library('pagination');

$current_lang=$this->language_model->get_current_language();
if($current_lang[0]->name!=""){
$this->lang=$current_lang[0]->name;
}else{
    $this->lang="default";
    
}

}
public function index($a="",$b="")
{

$data['lang']=$this->language_model->get_languages($this->lang);
$image['title'] = "Log Book";
$image['im']=$current_lang=$this->language_model->get_current_language();
$this->load->view('header',$image);
$this->load->model('sidebar/side_model');
$data['side']=$this->side_model->coach($this->lang);



if(isset($_POST['add_question'])){
$this->log_model->add_question();
}

if(isset($_POST['button_up'])){

$this->query_model->update_question_order($_POST['pre_id'],$_POST['pre_order'],$_POST['current_id'],$_POST['current_order']);
}
if(isset($_POST['delete'])){
$this->query_model->delete_logbook_question($_POST['current_id']);
}
if($a<1){
$query=$this->query_model->get_total_logbook_question();
}else{
$query="select * from QUESTIONSLOGBOOK where ACTIVE='0' and IDCOACH='".
$_SESSION['coach']."' order by ORDERQUESTION asc  limit ".addslashes($a).",10";
}
$query1="select * from QUESTIONSLOGBOOK where ACTIVE='0'";

$data['total']=$this->AddNewCoach_model->total_Activities($query1);

$data['total_question']=$this->AddNewCoach_model->total_Activities($query);

$config['base_url'] =base_url().'index.php/log_book/index/';
$config['total_rows'] = count($data['total']);
$config['per_page'] = 10;

$this->pagination->initialize($config);

$data['page']=$this->pagination->create_links();

$this->load->view("logbook",$data);
$this->load->view('footer',$image);

}



}
