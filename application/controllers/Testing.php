<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Testing extends CI_Controller {


public function __construct()
{

parent::__construct();
$this->load->model('coachee_model');
$this->load->model('model_data');
 $this->model_data->session_expire_redirect("2");	
$this->load->model('query_model');
$this->load->model('model_toolbox');
$image['title'] ='ToolBox';	
$this->load->model('testing_model');
$this->load->model('language_model');
$current_lang=$this->language_model-> get_coachee_current_language();


if($current_lang[0]->name!=""){
$this->lang=$current_lang[0]->name;
}else{
    $this->lang="default";
    
}

}
public function index($a="",$b="")
{
 // echo $a;
  //exit; 
 $data['lang']=$this->language_model->get_languages($this->lang);

if(isset($_POST['answer']))
{
$iDAlternativa=$this->model_data->get_array_value($_POST['iDAlternativa']);
$IDQuest=$this->model_data->get_array_value($_POST['IDQuest']);
$query_list=$this->testing_model->save_answer($iDAlternativa,$IDQuest,$a);

//print_r($query_list);
for($t=0;$t<count($query_list);$t++){
    $this->db->query($query_list[$t]);
    
}
header("Location:".$this->config->base_url()."index.php/Testing?msg=Your Answer has been Saved");
}

$image['im']=$current_lang=$this->language_model->get_coachee_current_language();

$this->load->view('header',$image);

$this->load->model('sidebar/side_model');
$data['side']=$this->side_model->coachee($this->lang);
if($a=="" ){

////////////////////////////////////////////////////////////////////////////
////////////////////Get All test By coach
///////////////////////////////////////////////////////////////////////////////
$coachID = get_coach_id($_SESSION['coach']);
$answer_query =$this->query_model->get_attempte_test($_SESSION['coach']); // attempt test list.

$data['test_answer']=$this->model_data->get_total_list($answer_query);
$get_test_coachee_list = $this->model_toolbox->get_list_coachee_list($_SESSION['coach'],$coachID);
$data['test_list'] = $get_test_coachee_list; 
/*$data['test_list']=$this->model_data->get_total_list("select tests.* from  tests 
inner join enabledtool on enabledtool.IDtool=tests.IDAvaliacao 
inner join coachee on coachee.IDCoachee=enabledtool.IDCoachee
where coachee.IDCoachee='".$_SESSION['coach']."' and Enabled='1'");
//wheel query*/
//$data['wheel_answer']=$this->model_data->get_total_list($answer_query);
//$data['wheel_list']=$this->model_data->get_total_list("select wheel.* from  wheel 
//inner join enabledtool on enabledtool.IDtool=wheel.IDwheel 
//inner join coachee on coachee.IDCoachee=enabledtool.IDCoachee  
//where coachee.IDCoachee='".$_SESSION['coach']."' and Enabled='1'");

//khetab-code#################################--------------------
$get_wheel_coachee_list = $this->model_toolbox->get_wheel_coachee_list($_SESSION['coach'],$coachID);

$data['get_wheel_coachee_list']= $get_wheel_coachee_list; 

$get_video_coachee_list = $this->model_toolbox->get_video_coachee_list($_SESSION['coach'],$coachID);

$data['get_video_coachee_list']= $get_video_coachee_list; 

#########################################
//print_r($data['test_list']);
$this->load->view("Testing_view",$data);
//$this->load->view("wheels_coachee",$data);

//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////

}
else if($a!="" && $b=="")
{

$query=$this->query_model->get_test_query($a);
$data['test_info']=$this->model_data->get_total_list($query);
$this->load->view("question_ans",$data);

}
else if($a!="" && $b!="")
{

$query=$this->query_model->get_test_query($a);
$data['url']="index.php/Testing/";

$data['view']=$b;
$data['test_info']=$this->model_data->get_total_list($query);
$answer_query=$this->query_model->get_test_answer($a,$_SESSION['coach']);
$data['test_answer']=$this->model_data->get_total_list($answer_query);


$this->load->view("question_ans",$data);

}
$this->load->view('footer');

}

public function coacheeans()
{
	$image['im']=$current_lang=$this->language_model->get_coachee_current_language();
	$data['lang']=$this->language_model->get_languages($this->lang);
	if($this->uri->segment(3) == 'add')
	{
	    
		
	    $whellitensid_arr  = $this->input->post('IDwheelitens');
		$point_arr = $this->input->post('point');
	    for($i=0;$i<count($whellitensid_arr);$i++)
		{
		    $data['IDCoach']	  = $this->input->post('IDCoach'); 
			$data['IDCoachee']	= $_SESSION['coach'];
			$data['Value']	    = $point_arr[$i];  
			$data['IDwheel']  	  = $this->input->post('whellID');
			$data['IDwhellitens'] = $whellitensid_arr[$i]; 
			$rss = $this->model_toolbox->add_whellans($data,'wheelanswers');
			 	
		}
		 redirect('Testing');
		 exit;
	   //print_r($this->input->post('point'));
	 	  
	}
	//$coachee_id =$this->uri->segment(3);
	$action =$this->uri->segment(4);
	$data['action']=$action;
	$data['question']=$this->model_toolbox->get_question($this->uri->segment(3)); 
	$data['question_list']=$this->model_toolbox->get_question_list($this->uri->segment(3));
	$this->load->view('header',$image);
    $this->load->model('sidebar/side_model');
	$this->load->model('sidebar/side_model');
    $data['side']=$this->side_model->coachee($this->lang);
	$this->load->view("add_wheel_coachee",$data);
	$this->load->view('footer');
}

 function add_video_coachee($id="")
 {
	$image['im']=$current_lang=$this->language_model->get_coachee_current_language();
	$data['lang']=$this->language_model->get_languages($this->lang);
	$this->load->view('header',$image);
    $this->load->model('sidebar/side_model');
	$data['side']=$this->side_model->coachee($this->lang);
	$data['id']=$id;
	if($id!=""){
	$data['edit_video']=$this->model_toolbox->edit_video($id);

	}
	$this->load->view("add_video_coachee",$data);
	$this->load->view('footer');
	
	
 }


 function addwhellanswer()
 {
	echo 'kkkkkkkkkkkk';
	exit;

	//$url= base_url().'index.php/Testing/'; 
	redirect('index.php/Testing');
	//header("Location:".$this->config->base_url()."index.php/Testing?msg=Your Answer has been Saved");


	
 }



}
