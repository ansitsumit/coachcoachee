<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Documents extends CI_Controller {
var $lang="";
function __construct()
{
parent::__construct();
$this->load->model('model_data');
//$this->model_data->session_expire_redirect("1");	
$this->load->model('language_model');
$current_lang=$this->language_model->get_current_language();
if($current_lang[0]->name!=""){
    
$this->lang=$current_lang[0]->name;
}else{
    $this->lang="default";
     }
}


public function index($delete="0")
{
    $image['title'] = 'Documents';
    $data['lang']=$this->language_model->get_languages($this->lang);

$this->load->model('Documents_model');

if(isset($_POST['document']))
{
$this->Documents_model->Documents();
$data['msg']=$this->model_data->submit_msg();
//print_r($data['msg']);
//exit;

}
if($delete>0)
{
    //echo $delete;
    //exit;
$this->Documents_model->delete_Documents($delete);

}

$data['list_documents']=$this->Documents_model->list_Documents();

$this->load->model('sidebar/side_model');
$data['side']=$this->side_model->coach($this->lang);
$image['im']=$current_lang=$this->language_model->get_current_language();
$this->load->view('header',$image);
$this->load->view('Documents',$data);
$this->load->view('footer');

}



}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */