<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Documents1 extends CI_Controller {
	function __construct()    { 
	parent::__construct(); 
		$this->load->model('model_data');
		$this->model_data->session_expire_redirect("2");
		$this->load->model('AddNewCoach_model');
		$this->load->model('language_model');
		$current_lang=$this->language_model-> get_coachee_current_language();
		if($current_lang[0]->name!=""){
		$this->lang=$current_lang[0]->name;
		}else{   
		$this->lang="default";   
		}	}	
		public function index($delete="0") {
			$data['lang']=$this->language_model->get_languages($this->lang);
			$this->load->model('Documents_model');
			$this->load->model('Documents_model');	
			$info=$this->AddNewCoach_model->get_data_coach($_SESSION['coach']);	
			$data['list_documents']=$this->Documents_model->list_coach_Documents($info[0]->IDCoach);
			$this->load->model('sidebar/side_model');	
			$data['side']=$this->side_model->coachee($this->lang);
			$image['title']='Document';
			$image['im']=$current_lang=$this->language_model->get_coachee_current_language();
			$this->load->view('header',$image);
			$this->load->view('Documents1',$data);	
			$this->load->view('footer');	
			}	
			}
			/* End of file welcome.php *//* Location: ./application/controllers/welcome.php */