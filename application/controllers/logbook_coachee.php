<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class logbook_coachee extends CI_Controller {


public function __construct()
{

parent::__construct();
$this->load->model('model_data');
$this->model_data->session_expire_redirect("2");	
$this->load->model('AddNewCoach_model');
$this->load->model('sidebar/side_model');
$this->load->model('query_model');
$this->load->model('log_model');
$image['title']='Log Book';
$image['im']=$current_lang=$this->language_model->get_coachee_current_language();
$this->load->view('header',$image);

$this->load->model('language_model');
$current_lang=$this->language_model-> get_coachee_current_language();


if($current_lang[0]->name!=""){
$this->lang=$current_lang[0]->name;
}else{
    $this->lang="default";
    
}

}
public function index($a="",$b="")
{
    
$data['side']=$this->side_model->coachee($this->lang);
$data['lang']=$this->language_model->get_languages($this->lang);
$url=$this->config->base_url()."index.php/logbook_coachee/viewfilltoday/";
$data['result']=$this->log_model->get_question_list_info($_SESSION['coach'],$url);
$query="delete from logbook_tmp where logbook_tmp.IDCOACHEE='".$_SESSION['coach']."'";
$this->db->query($query);
$this->load->view("logbook_coachee_view",$data);
$this->load->view('footer');

}

public function filltoday($question_id="",$b="")
{
$data['side']=$this->side_model->coachee($this->lang);
$data['lang']=$this->language_model->get_languages($this->lang);

if(isset($_POST['saved_question'])){


$data_entered->IDCOACH=$_POST['IDCOACH'];
$data_entered->IDCOACHEE=$_SESSION['coach'];
$data_entered->IDQUESTIONS=$_POST['IDQUESTIONS'];
$data_entered->question_date=$_POST['question_date'];
$data_entered->ANSWER=addslashes($_POST['ANSWER']);
$data_entered->DATE=date("Y-m-d");
$data_entered->time=date("H:i:s");
if(strlen(trim($_POST['ANSWER']))<1 || $_POST['IDQUESTIONS']<1){
$data_entered->status="no";
$this->db->insert("logbook_tmp",$data_entered);

}else{
$data_entered->status="yes";
$this->db->insert("logbook_tmp",$data_entered);

}
if($_POST['saved']=="final_submit"){
$query="insert into logbook (`IDCOACH`, `IDCOACHEE`, `IDQUESTIONS`, `ANSWER`, `DATE`, `time`,`question_date`,`status`)select `IDCOACH`, `IDCOACHEE`, `IDQUESTIONS`, `ANSWER`, `DATE`, `time`,`question_date`,`status` from logbook_tmp where logbook_tmp.IDCOACHEE='".$_SESSION['coach']."'";
$this->db->query($query);
 $this->db->_error_message();;
$query="delete from logbook_tmp where logbook_tmp.IDCOACHEE='".$_SESSION['coach']."'";
$this->db->query($query);
$this->db->_error_message();
header("Location:".base_url()."index.php/logbook_coachee");
}
}
$query="select IDQUESTIONS 	 from logbook where IDCOACHEE='".$_SESSION['coach']."'";
$attemp_question=$this->query_model->self_query($query);

if($question_id>0 && $question_id!="new"){

$query="select QUESTIONSLOGBOOK.* from QUESTIONSLOGBOOK
left join coach on coach.IDCOACH=QUESTIONSLOGBOOK.IDCOACH
left join coachee on coachee.IDCOACH=coach.IDCOACH
where coachee.IDCoachee='".$_SESSION['coach']."' 
 and QUESTIONSLOGBOOK.Active='0'
and QUESTIONSLOGBOOK.ORDERQUESTION >='".$question_id."' 
and IDQUESTIONSLOGBOOK NOT
IN (select IDQUESTIONS 	 from logbook where IDCOACHEE='".$_SESSION['coach']."')
order by
QUESTIONSLOGBOOK.ORDERQUESTION asc limit 2 ";
$data['question_list']=$this->query_model->self_query($query);
}
else if($question_id=="new"){
$query="select QUESTIONSLOGBOOK.* from QUESTIONSLOGBOOK
left join coach on coach.IDCOACH=QUESTIONSLOGBOOK.IDCOACH
left join coachee on coachee.IDCOACH=coach.IDCOACH
where coachee.IDCoachee='".$_SESSION['coach']."'
 and QUESTIONSLOGBOOK.Active='0' and IDQUESTIONSLOGBOOK NOT
IN (select IDQUESTIONS 	 from logbook where IDCOACHEE='".$_SESSION['coach']."') 
order by QUESTIONSLOGBOOK.ORDERQUESTION asc
 limit 2";
$data['question_list']=$this->query_model->self_query($query);

}
else{

}
$data['check_attemp']=$this->query_model->self_query("select count(*) as 
total_answer from logbook where IDCoachee='".$_SESSION['coach']."'
 and date='".date("Y-m-d")."'");

$this->load->view("filltoday",$data);

$this->load->view('footer');

}

function viewfilltoday($a){
if($a==""){
redirect($this->config->base_url()."index.php/logbook_coachee/");
}else{
$data['side']=$this->side_model->coachee($this->lang);
$data['lang']=$this->language_model->get_languages($this->lang);

$query="SELECT * FROM `QUESTIONSLOGBOOK` 
inner join logbook on logbook.IDQUESTIONS=QUESTIONSLOGBOOK.IDQUESTIONSLOGBOOK
where  logbook.date='".$a."'
and logbook.IDCOACHEE='".$_SESSION['coach']."'
order by QUESTIONSLOGBOOK.ORDERQUESTION asc
";
$data['attemp_question']=$this->query_model->self_query($query);
//echo "<pre>";
//print_r($data['attemp_question']);
$this->load->view("viewfilltoday",$data);
$this->load->view('footer');
}
}
}
