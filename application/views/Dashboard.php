<?php include 'sidemenu.php'; $_SESSION['demo'] = $color[0]->Color; ?>
    <!-- End: sidemenu -->
   
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper"
	<!-- Start: Topbar -->
	<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					<a href="javascript:void(0);">Dashboard</a>
				</li>
			</ol>
		</div>
	</header>
	<!-- End: Topbar -->

    <!-- Begin: Content -->
    <div id="content" class="animated fadeIn">
        <div class="row">
			<form class="form-horizontal" role="form">
                <a href="<?php echo $this->config->base_url();?>index.php/home/index/Coachees" style="color:black"><div class="col-sm-6 col-xl-3">
					<div class="panel panel-tile text-center br-a br-grey">
						<div class="panel-body btn-dark">
							<h1 class="fs30 mt5 mbn"><?php echo $lang[15]?></h1>
							<h1 class="text-system"><?php  echo $list_cochee[0]->total; ?></h1>
						</div>
						<div class="panel-footer br-t p12">
							<span class="fs11">
							<i class="glyphicons glyphicons-user_add"></i>
							<b><?php echo $lang[15]?></b>
							</span>
						</div>
					</div>
				</div></a>
				
				<a href="<?php echo $this->config->base_url();?>index.php/BuyCredits" style="color:black"><div class="col-sm-6 col-xl-3">
					<div class="panel panel-tile text-center br-a br-grey">
						<div class="panel-body btn-dark">
							<h1 class="fs30 mt5 mbn"> <?php echo $lang[16]?> </h1>
							<h1 class="text-system"> <?php echo ($credit[0]->Credits-$list_cochee[0]->total);?> </h1>
						</div>
						<div class="panel-footer br-t p12">
							<span class="fs11">
							<i class="glyphicons glyphicons-usd"></i>
							<b><?php echo $lang[16]?></b>
							</span>
						</div>
					</div>
				</div></a>
				
				<a href="<?php echo $this->config->base_url();?>index.php/Documents" style="color:black"><div class="col-sm-6 col-xl-3">
					<div class="panel panel-tile text-center br-a br-grey">
						<div class="panel-body btn-dark">
							<h1 class="fs30 mt5 mbn"> <?php echo $lang[17]?> </h1>
							<h1 class="text-system"> <?php echo $documents[0]->total?> </h1>
						</div>
						<div class="panel-footer br-t p12">
							<span class="fs11">
							<i class="glyphicons glyphicons glyphicons-book_open"></i>
							<b><?php echo $lang[17]?></b>
							</span>
						</div>
					</div>
				</div></a>
				
				<a href="<?php echo $this->config->base_url();?>index.php/home/index/Tests" style="color:black"><div class="col-sm-6 col-xl-3">
					<div class="panel panel-tile text-center br-a br-grey">
						<div class="panel-body btn-dark">
							<h1 class="fs30 mt5 mbn"> <?php echo $lang[18]?> </h1>
							<h1 class="text-system"> <?php echo $test[0]->total?> </h1>
						</div>
						<div class="panel-footer br-t p12">
							<span class="fs11">
							<i class="glyphicons glyphicons-notes_2"></i>
							<b><?php echo $lang[18]?></b>
							</span>
						</div>
					</div>
				</div></a>
				
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span><?=$lang[420]?>
							</span>
                        </div>
                        <div class="panel-body pn">
                            <div class="table-responsive" style="height:600px; overflow:auto;">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th style="width:15%;"><?=$lang[34]?></th>
											<th style="width:25%;"><?=$lang[15]?> </th>
											<th style="width:20%;"><?=$lang[421]?></th>
											<th style="width:5%;"><?=$lang[102]?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php
										$id=array();
										if($done_activity[0]->IDAtividade>0){
										for($r=0;$r<count($done_activity);$r++){
										if(!in_array($done_activity[$r]->IDAtividade,$id)){
										if($done_activity[$r]->DataEntrega=="0000-00-00"){
											//echo "8";	////////////////////////////////////////////////////
											////// check end date  equal to null date
											///////////////////////////////
										if($done_activity[$r]->dis_acti_id>0){
										$date_info=$done_activity[$r]->date_info;
											$status="Message on Activity";
											//echo "7";
											}else{
										 $status="Delayed Activity";
										$date_info=$done_activity[$r]->Deadline;	
										//echo "6";
											}
										}
										else if($done_activity[$r]->DataEntrega<=date("Y-m-d")){
											////////////////////////////////////////////////////
											////// check end date small from current
											///////////////////////////////
											//echo "5";
										$status="Activity Done";
										$date_info=$done_activity[$r]->DataEntrega;
											//echo "4";
										
										}
										else if($done_activity[$r]->DataEntrega>date("Y-m-d")){
											//echo "3";
											////////////////////////////////////////////////////
											////// check end date greater from current
											///////////////////////////////
										
											if($done_activity[$r]->dis_acti_id>0){
											//echo "2";
											$date_info=$done_activity[$r]->date_info;
												$status="Message on Activity";
											}else{
											//echo "1";
											 $status="Delayed Activity";
											$date_info=$done_activity[$r]->Deadline;	
									 
											}
										
										}
											
										?>
										<tr>
											<td><?php echo $date_info;?></td>
											<td><?php echo $done_activity[$r]->Name?></td>
											<td><?php echo $status;?></td>
											<td><a href="<?=$this->config->base_url()?>index.php/AddNewCoach/Activity/<?php echo $done_activity[$r]->IDCoachee?>/<?php echo $done_activity[$r]->IDAtividade?>"class="btn btn-success btn-xs"> <i class="fa fa-eye"> </i> <?=$lang[102]?></a>
											</td>

											</tr>
											<?php  } $id[]=$done_activity[$r]->IDAtividade;} }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- end col-md-12 -->
					
			</form>      
        </div>
	</div>
            <!-- End: Content -->  
</section>
    <!-- End: Content-Wrapper -->
	
<?php
if($_GET['tip']) {
?>
<html>
<head>
<meta charset="utf-8">
<title>coach</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/skin/default_skin/css/main.css">
</head>
<body>

<div id="boxes">
  <div style="top: 199.5px; left: 551.5px; display: none;" id="dialog" class="window"> Tips
    <div id="lorem">
	<hr/>
      <p>
	<?php 
	for($r=0;$r<count($tips);$r++){
	?>
	<p> <?php echo $tips[$r]->Tip?></p>
	  
	  <?php }?>
	  </p>
    </div>
    <div id="popupfoot"> <a href="<?=$this->config->base_url();?>index.php/home/index/Dashboard">Close</a> </div>
  </div>
  <div style="width: 100%; font-size: 32pt; color:white; height: 602px; display: none; opacity: 0.8;" id="mask"></div>
</div><!--
<script src="<?php echo base_url();?>media/assets/js/jquery_1_11_1.js"></script> --->

</body>
</html>
<?php }elseif($_GET['tip1']){

}else {
redirect($this->config->base_url()."index.php/home/index/Dashboard?tip1=4");
}


