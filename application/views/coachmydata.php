<?php include 'sidemenu.php'; ?>
    <!-- End: sidemenu -->
   
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper"
	<!-- Start: Topbar -->
	<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					<a href="#">My Data</a>
				</li>
			</ol>
		</div>
	</header>
	<!-- End: Topbar -->

    <!-- Begin: Content -->
    <div id="content" class="animated fadeIn">
        <div class="row">
		<h3 class=" smaller lighter green" style="color:green" align="center"><?php 
        print($update);?></h3>
			<form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                
                <div class="col-md-12"> 
					<div class="col-md-4">
						<div class="fileupload fileupload-new admin-form" data-provides="fileupload">
							<div class="fileupload-preview thumbnail mb20">
								<img width="150" height="69" src="<?= $this->config->base_url(); ?>coach_images/<?php echo $coach_info[0]->Photo ?>" alt="holder">
							</div>
							<div class="row">
								
								<div class="col-xs-5">
									<span class="button btn-system btn-file btn-block">
										<span class="fileupload-new"><?=$lang[57]?></span>
									<span class="fileupload-exists">Change</span>
									<input  type="file" name="Photo" >
									<input  type="hidden" name="photo1" value="<?php echo $coach_info[0]->Photo ?>" >
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				
                <div class="col-md-6">
					<div class="panel panel-primary panel-border top mt20 mb35">
                        <div class="panel-body bg-light dark">
                            <div class="admin-form">
                               
                                <div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[22]?> :</label>
								<div class="col-lg-8">
								<input class="form-control" type="text" name="Name" value="<?php echo $coach_info[0]->Name ?>" required="">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[20]?>1 :</label>
								<div class="col-lg-8">
								<input  class="form-control" type="email" name="Email1" value="<?php echo $coach_info[0]->Email1?>" required="">
								</div>
								</div>
								
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[20]?>2 :</label>
								<div class="col-lg-8">
								<input  class="form-control" type="email" name="Email2" value="<?php echo $coach_info[0]->Email2?>">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label"><?=$lang[61]?> :</label>
								<div class="col-lg-8">
								<input  class="form-control" type="password" name="Password" value="<?php echo $coach_info[0]->Password ?>" required="">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[62]?> </label>
								<div class="col-lg-8">
								<label class="field select">
									<select name="CivilState">
										<option> </option>
										<?php
										for($r=0;$r<count($civil_staes);$r++){
										$check="";  	  
											if($coach_info[0]->CivilState==$civil_staes[$r]->id){
											$check="selected";
											}
										?>
										<option value="<?=$civil_staes[$r]->id?>" <?=$check?>><?=$civil_staes[$r]->name?></option>
										<?php }?>
									</select>
									<i class="arrow double"></i>
								</label>
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[63]?>  :</label>
								<div class="col-lg-8">
								<label class="field select">
									<select name="Education">
										<option></option>
										<?php
										for($r=0;$r<count($edu);$r++){
										$check="";    	  
										if($coach_info[0]->Education==$edu[$r]->ID){
											$check="selected";
										}
										?>
										<option value="<?=$edu[$r]->ID?>" <?=$check?>><?=$edu[$r]->Name?></option>
										<?php }?>
									</select>
									<i class="arrow double"></i>
								</label>
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[64]?> :</label>
								<div class="col-lg-8">
								<label class="field select">
									<select name="HierachicalLevel">
										<option></option>
										<?php
										for($r=0;$r<count($hierachi);$r++){
										$check="";    	  
											if($coach_info[0]->HierachicalLevel==$hierachi[$r]->ID){
												$check="selected";
											}
										?>
										<option value="<?=$hierachi[$r]->ID?>" <?=$check?>><?=$hierachi[$r]->Name?></option>
										<?php }?>
									</select>
									<i class="arrow double"></i>
								</label>
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard">Date Home License :</label>
								<div class="col-lg-8">
								<input class="form-control datepicker" type="text" name="DataInicioLicenca" placeholder="YYYY-MM-DD" value="<?php echo $coach_info[0]->DataInicioLicenca ?>" readonly>
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard">End date License :</label>
								<div class="col-lg-8">
								<input class="form-control datepicker" type="text" name="DataFimLicenca"  value="<?php echo $coach_info[0]->DataFimLicenca ?>" readonly>
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[16]?>:</label>
								<div class="col-lg-8">
								<input class="form-control" type="text" name="Credits" 
								value="<?=($coach_info[0]->Credits-$total_coach[0]->total)?>"
								onkeyup="this.value=this.value.replace(/[^0-9\.]/g,'');" readonly>
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[66]?> :</label>
								<div class="col-lg-8">
								<input class="form-control" type="text" name="Observations"  value="<?php echo $coach_info[0]->Observations ?>">
								</div>
								</div>
								
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[70]?></label>
								<div class="col-lg-8">
								<textarea class="gui-textarea" name="Address"> <?php echo $coach_info[0]->Address ?> </textarea>
								</div>
								</div>
								
								
								
                                
                            </div>
                        </div>
                    </div>
                </div> <!--close col-sm-6-->
				
				<div class="col-md-6">
					<div class="panel panel-primary panel-border top mt20 mb35">
                        <div class="panel-body bg-light dark">
                            <div class="admin-form">
							
                                <div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[71]?>:</label>
								<div class="col-lg-8">
								<input  class="form-control" type="text" name="Neighborhood" 
								value="<?php echo $coach_info[0]->Neighborhood ?>">
								</div>
								</div>
								
                                <div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[72]?> :</label>
								<div class="col-lg-8">
								<input class="form-control" type="text" name="City" value="<?php echo $coach_info[0]->City ?>">
								</div>
								</div>
							
                                <div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[73]?> :</label>
								<div class="col-lg-8">
								<label class="field select">
									<select name="Country">
										<option></option>

										<?php
										for($r=0;$r<count($country);$r++){
										$check="";     
											if($coach_info[0]->Country==$country[$r]->CountryID){
												$check="selected";
											}
										?>
										<option value="<?=$country[$r]->CountryID?>" <?=$check?>><?=$country[$r]->CountryName?></option>
										<?php }?>
									</select>
									<i class="arrow double"></i>
								</label>
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[74]?> :</label>
								<div class="col-lg-8">
								<input class="form-control" type="text" name="ZipCode" value="<?php echo $coach_info[0]->ZipCode ?>" onkeyup="this.value=this.value.replace(/[^0-9\.]/g,'');">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[75]?> :</label>
								<div class="col-lg-8">
								<input class="form-control" type="text" name="PhoneResidential" value="<?php echo $coach_info[0]->PhoneResidential ?>" onkeyup="this.value=this.value.replace(/[^0-9\.]/g,'');">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[76]?> :</label>
								<div class="col-lg-8">
								<input  class="form-control" type="text" name="PhoneCommercial"
								value="<?php echo $coach_info[0]->PhoneCommercial ?>" onkeyup="this.value=this.value.replace(/[^0-9\.]/g,'');">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[77]?> :</label>
								<div class="col-lg-8">
								<input class="form-control" type="text" name="Mobile" value="<?php echo $coach_info[0]->Mobile ?>" onkeyup="this.value=this.value.replace(/[^0-9\.]/g,'');">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[78]?> :</label>
								<div class="col-lg-8">
								<input  class="form-control" type="text" name="Document" value="<?php echo $coach_info[0]->Document ?>">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[79]?>  :</label>
								<div class="col-lg-8">
								<input class="form-control" type="text" name="Profession" value="<?php echo $coach_info[0]->Profession ?>">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[80]?> :</label>
								<div class="col-lg-8">
								<input  class="form-control" type="text" name="Company" value="<?php echo $coach_info[0]->Company ?>">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[65]?> :</label>
								<div class="col-lg-8">
								<input class="form-control" type="text" name="Birth" class="datepicker2" value="<?php echo $coach_info[0]->Birth ?>">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[69]?>  :</label>
								<div class="col-lg-8">
								<input class="form-control" readonly value="<?php echo $score[0]->point ?>">
								</div>
								</div>
								
								<br/> <br/> <br/> <br/>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="col-md-12" align="center">
					<button class="btn active btn-success" name="submit_coach" type="submit"> 
					<i class="fa fa-refresh"></i>  <?=$lang[424]?> </button>
					<button class="btn active btn-system " type="submit"> 
					<i class="fa fa-refresh"></i>  <?=$lang[108]?> </button>
				</div>	
					
			</form>      
        </div>
	</div>
            <!-- End: Content -->  
</section>
    <!-- End: Content-Wrapper -->


