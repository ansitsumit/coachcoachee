<?php include 'sidemenu.php'; ?>
    <!-- End: sidemenu -->
   
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper"
	<!-- Start: Topbar -->
	<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					<a href="#"> <?php echo $lang[15]?> </a>
				</li>
			</ol>
		</div>
	</header>
	<!-- End: Topbar -->

    <!-- Begin: Content -->
    <div id="content" class="animated fadeIn">
        <div class="row">
			<form class="form-horizontal" role="form">

				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span><?php echo $lang[15]?>
							</span>
							<a href="<?=$this->config->base_url();?>index.php/coachee"><span>
							<button style="padding:9px; margin-right:-7px;" class="btn btn-success btn-sm light fw600 ml10 pull-right" type="button">
							<i class="fa fa-plus"></i>
							<?php echo $lang[28]?>
							</button> </span></a>
                        </div>
                        <div class="panel-body pn">
                            <div class="table-responsive" style="height:460px; overflow:auto;">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th style="width:30%;"><?php echo $lang[22];?></th>
											<th style="width:20%;"><?php echo $lang[20];?></th>
											<th style="width:20%;"><?php echo $lang[21];?></th>
											<th style="width:20%;"><?php echo $lang[23];?></th>
											<!--<th style="width:20rem;"><?php echo $lang[24];?></th>--->
											<th style="width:10%;"><?php echo $lang[433];?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       
                                            <?php
												if($active_coachee>0){

												for($r=0;$r<count($active_coachee);$r++){
												if((($active_session_done_total[$r]->total*100)/$active_session_total[$r]->total)=="100" && (($active_total_done[$r]->total*100)/$active_total[$r]->total)=="100" ){
												$df[]=$active_coachee[$r];
												}else{
												?>	
											<tr>
												<td><?php echo $active_coachee[$r]->Name?></td>
												<td><?php echo $active_coachee[$r]->Email1?></td>
												<td><?php echo $active_coachee[$r]->Mobile?></td>
												<td> 
												<div class="clearfix">
													<?php $as=((($active_total_done[$r]->total*100)/$active_total[$r]->total));?>
													<span class="pull-right"><?=round($as,2)?>%</span>
												</div>	
												<div class="progress mt12 mbn">
													<div class="progress-bar progress-bar-system progress-bar-striped active" style="width:<?=($active_total_done[$r]->total*100)/$active_total[$r]->total?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="45" role="progressbar">
													</div>
												</div>
												</td>	
												<td>
													<a class="btn btn-success btn-xs" href="<?php echo $this->config->base_url();?>index.php/AddNewCoach/index/<?=$active_coachee[$r]->IDCoachee?>">
														<i class="fa fa-folder-open"></i> <?php echo $lang[433]?>
													</a>
												</td>
											</tr>		
											<?php }}}?>	
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- end col-md-12 -->
				
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span><?php echo $lang[26]?>/<?php echo $lang[27]?>
							</span>
							
                        </div>
                        <div class="panel-body pn">
                            <div class="table-responsive" style="height:360px; overflow:auto;">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th style="width:30%;"><?=$lang[22]?></th>
											<th style="width:20%;"><?=$lang[20]?></th>
											<th style="width:20%;"><?=$lang[21]?></th>
											<th style="width:20%;"><?=$lang[23]?></th>
											<!---<th style="width:20%;"><?=$lang[24]?></th>--->
											<th style="width:10%;"><?=$lang[433]?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php
											if($inactive_coachee>0){

											for($r=0;$r<count($inactive_coachee);$r++){?>	
										<tr>
											<td style="width:30%;"><?php echo $inactive_coachee[$r]->Name?></td>
											<td style="width:20%;"><?php echo $inactive_coachee[$r]->Email1?></td>
											<td style="width:20%;"><?php echo $inactive_coachee[$r]->Mobile?></td>
                                     
                                            <td>
												<div class="clearfix">
												<span class="pull-right"><?=($inactive_session_done_total[$r]->total*100)/$inactive_session_total[$r]->total?>%</span>
												</div>
												<div class="progress mt12 mbn">
													<div class="progress-bar progress-bar-system progress-bar-striped active" style="width:<?=($inactive_session_done_total[$r]->total*100)/$inactive_session_total[$r]->total?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="45" role="progressbar">
													<span class="sr-only">45% Complete</span>
													</div>
												</div>
											</td>
                                            <td>
												<a class="btn btn-success btn-xs" href="<?php echo $this->config->base_url();?>index.php/AddNewCoach/index/<?=$inactive_coachee[$r]->IDCoachee?>">
													<i class="fa fa-folder-open"></i> <?php echo $lang[433]?>
												</a>
											</td>
                                        </tr>
										<?php }}

										for($r=0;$r<count($df);$r++){?>	
										<tr>
											<td style="width:30%;"><?php echo $df[$r]->Name?></td>
											<td style="width:20%;"><?php echo $df[$r]->Email1?></td>
											<td style="width:20%;"><?php echo $df[$r]->Mobile?></td>
                                            <td>
												<div class="clearfix">
													<span class="pull-right">100%</span>
												</div>
												<div class="progress mt12 mbn">
													<div class="progress-bar progress-bar-system progress-bar-striped active" style="width: 100%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="45" role="progressbar">
													<span class="sr-only">45% Complete</span>
													</div>
												</div>
											</td>
											
                                            <td>
												<a class="btn btn-success btn-xs" href="<?php echo $this->config->base_url();?>index.php/AddNewCoach/index/<?=$df[$r]->IDCoachee?>">
													<i class="fa fa-folder-open"></i> <?php echo $lang[433]?>
												</a>
											</td>
                                        </tr>
										<?php }?>
										
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- end col-md-12 -->
					
			</form>      
        </div>
	</div>
            <!-- End: Content -->  
</section>
    <!-- End: Content-Wrapper -->
<script>
$(".new_add").click(function(){
var total="<?=($credit[0]->Credits-$total_coach[0]->total)?>";
if(total<1 ){
alert("Please Buy Credits First");
return false;

}else{
return true;
}
})
</script>
