	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/validate/validate.css">
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/validate/jquery.validate.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/validate/additional-methods.js"></script>
	
	
<?php include 'sidemenu.php'; ?>
    <!-- End: sidemenu -->
   
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper"
	<!-- Start: Topbar -->
		<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					<a href="#"><?php echo $pageheader?> </a>
				</li>
			</ol>
		</div>
	</header>
	<!-- End: Topbar -->
	<?php
	if(isset($wheelinfo)){
		$values_wheel=array(
		array($lang[112],$wheelinfo[0]->Creativity),
		array($lang[113],$wheelinfo[0]->Happiness),
		array($lang[114],$wheelinfo[0]->Spirituality),
		array($lang[115],$wheelinfo[0]->Health),
		array($lang[116],$wheelinfo[0]->Intellectual),
		array($lang[117],$wheelinfo[0]->Emotional),
		array($lang[118],$wheelinfo[0]->Purpose),
		array($lang[119],$wheelinfo[0]->Financial),
		array($lang[120],$wheelinfo[0]->Social),
		array($lang[121],$wheelinfo[0]->Family),
		array($lang[122],$wheelinfo[0]->Loving),
		array($lang[123],$wheelinfo[0]->Social_life)
		);
	}
	?>
    <!-- Begin: Content -->
    <div id="content" class="animated fadeIn">
        <div class="row">	
			<form id="frmcoachee" class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                <div class="col-md-12"> 
					<h3 style="color:green;" align="center" ><?php echo isset($msg)?$msg:'';?></h3>
				</div>
				
                <div class="col-md-12"> 
					<div class="col-md-4">
						<div class="fileupload fileupload-new admin-form" data-provides="fileupload">
							<div class="fileupload-preview thumbnail mb20">
								<?php if(isset($get_data_coachee[0]->Photo)){?>
								<img src="<?=$this->config->base_url();?>coach_images/<?=isset($get_data_coachee[0]->Photo)?$get_data_coachee[0]->Photo:'';?>" alt="holder">
								<?php }?>
							</div>
							<div class="row">
								
								<div class="col-xs-5">
									<span class="button btn-system btn-file btn-block">
										<span class="fileupload-new"><?=$lang[57]?></span>
									<span class="fileupload-exists"><?=$lang[57]?></span>
									<input id="form-field-6" type="file" name="photo"  >
									<input id="form-field-6" type="hidden" name="photo1" value="<?=isset($get_data_coachee[0]->Photo)?$get_data_coachee[0]->Photo:'';?>" >
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				
                <div class="col-md-6">
					<div class="panel panel-primary panel-border top mt20 mb35">
                        <div class="panel-body bg-light dark">
                            <div class="admin-form">
                               
                                <div class="form-group">
								<label class="col-lg-4 control-label"><?=$lang[58]?> :</label>
								<div class="col-lg-8">
								<input class="form-control" type="text" name="Name" value="<?=isset($get_data_coachee[0]->Name)?$get_data_coachee[0]->Name:'';?>" required="required">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label"> <?=$lang[59]?> :</label>
								<div class="col-lg-8">
								<input class="form-control" id="Email1" type="email"  value="<?=isset($get_data_coachee[0]->Email1)?$get_data_coachee[0]->Email1:'';?>" name="Email1" required="required">
								</div>
								</div>
								
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[60]?>  :</label>
								<div class="col-lg-8">
								<input id="email2" class="form-control" type="email" value="<?=isset($get_data_coachee[0]->Email2)?$get_data_coachee[0]->Email2:'';?>" name="Email2">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[61]?> :</label>
								<div class="col-lg-8">
								<input id="inputStandard" id="Password" class="form-control" type="password" value="<?=isset($get_data_coachee[0]->Password)?$get_data_coachee[0]->Password:'';?>" name="Password" required="required">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[62]?> :</label>
								<div class="col-lg-8">
								<label class="field select">
									<select id="form-field-6" name="CivilState">
										<option> </option>
										<?php
										for($r=0;$r<count($civil_staes);$r++){
										$check="";
										if(isset($get_data_coachee[0]->CivilState)){
										if($get_data_coachee[0]->CivilState==$civil_staes[$r]->id){
										$check="selected";
										}
										}
										?><option value="<?=$civil_staes[$r]->id?>" <?=$check?>><?=$civil_staes[$r]->name?></option>
										<?php }?>

										</select>
									<i class="arrow double"></i>
								</label>
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[63]?> :</label>
								<div class="col-lg-8">
								<label class="field select">
									<select id="form-field-6" type="text" name="Education">
										<option></option>
										<?php
										for($r=0;$r<count($edu);$r++){
										$check="";
										if(isset($get_data_coachee[0]->Education)){
										if($get_data_coachee[0]->Education==$edu[$r]->ID){
										$check="selected";
										}	
										}
										
										?><option value="<?=$edu[$r]->ID?>" <?=$check?>><?=$edu[$r]->Name?></option>
										<?php }?>


										</select>
									<i class="arrow double"></i>
								</label>
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[64]?> :</label>
								<div class="col-lg-8">
								<label class="field select">
									<select id="form-field-6" type="text" name="HierachicalLevel">
										<option></option><?php
										for($r=0;$r<count($hierachi);$r++){
										$check="";
										if(isset($get_data_coachee[0]->HierachicalLevel)){
										if($get_data_coachee[0]->HierachicalLevel==$hierachi[$r]->ID){
										$check="selected";
										}}
										?><option value="<?=$hierachi[$r]->ID?>" <?=$check?>><?=$hierachi[$r]->Name?></option>
										<?php }?>

										</select>
									<i class="arrow double"></i>
								</label>
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="dob"><?=$lang[65]?> :</label>
								<div class="col-lg-8">
								<input id="dob" class="form-control" type="text" value="<?=isset($get_data_coachee[0]->Birth)?$get_data_coachee[0]->Birth:'';?>" name="Birth">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[66]?> :</label>
								<div class="col-lg-8">
								<input id="inputStandard" class="form-control" type="text" value="<?=isset($get_data_coachee[0]->Observations)?$get_data_coachee[0]->Observations:'';?>" name="Observations">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[67]?>:</label>
								<div class="col-lg-8">
								<input id="inputStandard" class="form-control" type="text" value="<?=isset($get_data_coachee[0]->ObjetivosDoCoaching)?$get_data_coachee[0]->ObjetivosDoCoaching:'';?>" name="ObjetivosDoCoaching">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="DataFimCoaching"> <?=$lang[68]?>:</label>
								<div class="col-lg-8">
								<input id="datendCoaching" class="form-control datecal" type="text" name="DataFimCoaching" value="<?=isset($get_data_coachee[0]->DataFimCoaching)?$get_data_coachee[0]->DataFimCoaching:'';?>">
								</div>
								</div>
								
								<?php 
								if(isset($get_data_coachee[0]->IDCoachee) && ($get_data_coachee[0]->IDCoachee < 1)){
								$style="display:none";
								}
								?> 
								
								<div class="form-group" style="<?=isset($style)?$style:'';?>">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[69]?> :</label>
								<div class="col-lg-8">
								<input id="inputStandard" class="form-control" type="text" value="<?=isset($get_data_coachee[0]->score)?$get_data_coachee[0]->score:'';?>" name="score">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[48]?> :</label>
								<div class="col-lg-8">
								<textarea class="gui-textarea" name="Notes"> <?=isset($get_data_coachee[0]->Notes)?$get_data_coachee[0]->Notes:'';?> </textarea>
								</div>
								</div>
								
                            </div>
                        </div>
                    </div>
                </div> <!--close col-sm-6-->
				
				<div class="col-md-6">
					<div class="panel panel-primary panel-border top mt20 mb35">
                        <div class="panel-body bg-light dark">
                            <div class="admin-form">
							
                                <div class="form-group">
								<label class="col-lg-4 control-label"> <?=$lang[70]?> :</label>
								<div class="col-lg-8">
								<input  class="form-control" value="<?=isset($get_data_coachee[0]->Address)?$get_data_coachee[0]->Address:''?>" name="Address">
								</div>
								</div>
								
                                <div class="form-group">
								<label class="col-lg-4 control-label"> <?=$lang[71]?> :</label>
								<div class="col-lg-8">
								<input class="form-control" type="text" value="<?=isset($get_data_coachee[0]->Neighborhood)?$get_data_coachee[0]->Neighborhood:'';?>" name="Neighborhood">
								</div>
								</div>
								
                                <div class="form-group">
								<label class="col-lg-4 control-label"> <?=$lang[72]?> :</label>
								<div class="col-lg-8">
								<input id="inputStandard" class="form-control" type="text"  value="<?=isset($get_data_coachee[0]->City)?$get_data_coachee[0]->City:'';?>" name="City">
								</div>
								</div>
							
                                <div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[73]?> :</label>
								<div class="col-lg-8">
								<label class="field select">
									<select id="form-field-6" type="text" name="Country">
										<option></option>
										<?php
										for($r=0;$r<count($country);$r++){
										$check="";
										if(isset($get_data_coachee[0]->Country) && $get_data_coachee[0]->Country==$country[$r]->CountryID){
										$check="selected";
										}
										?><option value="<?=$country[$r]->CountryID?>" <?=$check?>><?=$country[$r]->CountryName?></option>
										<?php }?>
									</select>
									<i class="arrow double"></i>
								</label>
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[74]?> :</label>
								<div class="col-lg-8">
								<input id="inputStandard" class="form-control" type="text" value="<?=isset($get_data_coachee[0]->ZipCode)?$get_data_coachee[0]->ZipCode:'';?>" name="ZipCode" onkeyup="this.value=this.value.replace(/[^0-9\.]/g,'');">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label"> <?=$lang[75]?> :</label>
								<div class="col-lg-8">
								<input class="form-control" type="text" value="<?=isset($get_data_coachee[0]->PhoneResidential)?$get_data_coachee[0]->PhoneResidential:'';?>" name="PhoneResidential" onkeyup="this.value=this.value.replace(/[^0-9\.]/g,'');">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label"> <?=$lang[76]?> :</label>
								<div class="col-lg-8">
								<input class="form-control" type="text" value="<?=isset($get_data_coachee[0]->PhoneCommercial)?$get_data_coachee[0]->PhoneCommercial:'';?>" name="PhoneCommercial" onkeyup="this.value=this.value.replace(/[^0-9\.]/g,'');">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[77]?> :</label>
								<div class="col-lg-8">
								<input id="inputStandard" class="form-control" type="text" value="<?=isset($get_data_coachee[0]->Mobile)?$get_data_coachee[0]->Mobile:'';?>" name="Mobile" onkeyup="this.value=this.value.replace(/[^0-9\.]/g,'');">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[78]?>  :</label>
								<div class="col-lg-8">
								<input id="inputStandard" class="form-control" type="text" value="<?=isset($get_data_coachee[0]->Document)?$get_data_coachee[0]->Document:'';?>" name="Document">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label"> <?=$lang[79]?> :</label>
								<div class="col-lg-8">
								<input  class="form-control" type="text" value="<?=isset($get_data_coachee[0]->Profession)?$get_data_coachee[0]->Profession:'';?>" name="Profession">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[80]?>  :</label>
								<div class="col-lg-8">
								<input id="inputStandard" class="form-control" type="text" value="<?=isset($get_data_coachee[0]->Company)?$get_data_coachee[0]->Company:'';?>" name="Company">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="startcoachingdate"> <?=$lang[81]?> :</label>
								<div class="col-lg-8">
								<input id="startcoachingdate" class="form-control datecal" type="text" name="DataInicioCoaching" value="<?=isset($get_data_coachee[0]->DataInicioCoaching)?$get_data_coachee[0]->DataInicioCoaching:'';?>">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[82]?>   :</label>
								<div class="col-lg-8">
								<label class="field select">
									<select id="form-field-6" type="text" name="Status">
										<option value="1" <?php  if(isset($get_data_coachee[0]->Status) && $get_data_coachee[0]->Status=="1"){echo "selected";}?> > Active </option>
										<option value="0" <?php if(isset($get_data_coachee[0]->Status) && $get_data_coachee[0]->Status=="0"){echo "selected";}?>> Inactive </option>
									</select>
									<i class="arrow double"></i>
								</label>
								</div>
								</div>
								
								<?php if(isset($get_data_coachee[0]->IDCoachee) && ($get_data_coachee[0]->IDCoachee>0)){ ?>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[83]?> :</label>
								<div class="col-lg-8">
								<input id="inputStandard" class="form-control" type="text"  value="<?=isset($Sessions[0]->total)?$Sessions[0]->total:'';?>" readonly>
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[84]?> :</label>
								<div class="col-lg-8">
								<input id="inputStandard" class="form-control" type="text" name="SessoesFeitas" value="<?=isset($total_compelete_session[0]->total)?$total_compelete_session[0]->total:'';?>"  readonly>
								</div>
								</div>
								
								<?php }?>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="col-md-12" align="center">
					<?php
					for($r=0;$r<count($button);$r++){
					echo $button[$r][0]."&nbsp;";
					}

					?>
				</div>	
					
			</form>      
			<?php /*
				$values_wheel=array(
				array($lang[112],$wheelinfo[0]->Creativity),
				array($lang[113],$wheelinfo[0]->Happiness),
				array($lang[114],$wheelinfo[0]->Spirituality),
				array($lang[115],$wheelinfo[0]->Health),
				array($lang[116],$wheelinfo[0]->Intellectual),
				array($lang[117],$wheelinfo[0]->Emotional),
				array($lang[118],$wheelinfo[0]->Purpose),
				array($lang[119],$wheelinfo[0]->Financial),
				array($lang[120],$wheelinfo[0]->Social),
				array($lang[121],$wheelinfo[0]->Family),
				array($lang[122],$wheelinfo[0]->Loving),
				array($lang[123],$wheelinfo[0]->Social_life)
				);*/
			?>
        </div>
	</div>
            <!-- End: Content -->  
</section>
    <!-- End: Content-Wrapper -->
<script>
$(".new_add").click(function(){
var total="<?=($credit[0]->Credits-$total_coach[0]->total)?>";
if(total<1 ){
alert("Please Buy Credits First");
return false;

}else{
return true;
}
})
</script>
<script>
	$(function(){
		$("#dob").datepicker({
			dateFormat: "yy-mm-dd" //,minDate: '0' //<----here
		});
		$("#datendCoaching").datepicker({
			dateFormat: "yy-mm-dd" //,minDate: '0' //<----here
		});		
		$("#startcoachingdate").datepicker({
			dateFormat: "yy-mm-dd" //,minDate: '0' //<----here
		});		
	});
</script>
	<script>
		$(document).ready(function(){

				
			
			$('#frmcoachee').validate({
				rules: {
					Email1:{customemail: true},
					Email2:{customemail: true},
					Password: {
						alphanumeric: true,
						minlength: 6,
						maxlength: 8
					}
				},
				messages: {
					Email1:'Please Enter complete Email ID',
					Email2:'Please Enter complete Email ID'
				}
				
			})
		});
	</script>
