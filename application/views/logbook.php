<?php include 'sidemenu.php'; ?>
    <!-- End: sidemenu -->
   
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper"
	<!-- Start: Topbar -->
	<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					<a href="#"> LogBook </a>
				</li>
			</ol>
		</div>
	</header>
	<!-- End: Topbar -->
	
    <!-- Begin: Content -->
    <div id="content" class="animated fadeIn">
        <div class="row">
			
			<div class="p25 br-t">
			<?php if(isset($_GET['msg'])){?>
			<div class="col-md-12">
			<div class="alert alert-system dark alert-dismissable">
			<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
			<i class="fa fa-check pr10"></i>
			<strong> <?php print($msg.$_GET['msg']);?> </strong>
			</div>
			</div>
			<?php }?>
<!---========== Add Documents ==========================------------------------------------------->
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> LogBook
							</span>
                        </div>
                        <div class="panel-body pn">
							<div class="col-md-12"> &nbsp; </div>
							<div class="col-md-6">
								<div class="admin-form">
									<form class="form-horizontal" action="" method="post">
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> Question :</label>
									<div class="col-lg-8">
									<textarea  class="form-control" name="Question" required></textarea>
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> &nbsp; </label>
									<div class="col-lg-8">
									<button type="submit" class="btn btn-system btn-block" name="add_question">
									<i class="fa fa-save"></i>
									Add this Question </button>
									</div>
									</div>
									</form>
								</div>	
							</div>
							
							<div class="col-md-12">
							<h3> LogBook-saved question <hr/></h3>
							</div>
							<?php
							for($r=0;$r<count($total_question);$r++){
							if($total_question[$r]->IDQUESTIONSLOGBOOK >0){
							?>
							<div class="col-md-10">	
								<div class="admin-form">
									<div class="form-group">
									<div class="col-lg-8">
									<?php echo stripslashes($total_question[$r]->QUESTION);?>
									</div>
									</div>
								</div>	
							</div>
							<div class="col-md-2">
									
								<form action="" method="post">
								<input type="hidden" name="pre_id"
								 value="<?=isset($total_question[($r-1)]->IDQUESTIONSLOGBOOK)?$total_question[($r-1)]->IDQUESTIONSLOGBOOK:'';?>">
								<input type="hidden" name="pre_order"
								 value="<?=isset($total_question[($r-1)]->ORDERQUESTION)?$total_question[($r-1)]->ORDERQUESTION:''?>">

								 <input type="hidden" name="current_id"
								 value="<?=isset($total_question[($r)]->IDQUESTIONSLOGBOOK)?$total_question[($r)]->IDQUESTIONSLOGBOOK:''?>">

								 <input type="hidden" name="current_order"
								 value="<?=isset($total_question[($r)]->ORDERQUESTION)?$total_question[($r)]->ORDERQUESTION:''?>">
							
								<button class="btn btn-system btn-xs" type="submit"  name="button_up">
								<i class="fa fa-hand-o-up"></i>
								Up
								</button>
				
								<button class="btn btn-danger btn-xs" type="submit" onclick="return confirm('Are you sure want to delete');" name="delete">
								<i class="fa fa-trash-o"></i>
								Del
								</button>
								</form>			
							</div>
							<div class="col-md-12"> <hr/>	 </div>
							<?php }} ?>
							<!---## paging start here #########--------------------->
							<div class="col-md-12"> <?php echo $page;?>	 </div>
                        </div>
                    </div>
                </div><!-- end col-md-12 -->
			
<!---==========  Documents List ==========================------------------------------------------->	
		    
        </div>
	</div>
            <!-- End: Content -->  
</section>
    <!-- End: Content-Wrapper -->

