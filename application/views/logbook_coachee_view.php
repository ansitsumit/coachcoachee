<?php include ('sidemenu.php');?>
    <!-- End: sidemenu -->
   
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper"
	<!-- Start: Topbar -->
	<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					<a href="javascript:void(0);">LogBook</a>
				</li>
			</ol>
		</div>
	</header>
	<!-- End: Topbar -->

    <!-- Begin: Content -->
    <div id="content" class="animated fadeIn">
        <div class="row">
			<form class="form-horizontal" role="form">
               
				<div class="col-md-12"> <a href="<?php echo $this->config->base_url()?>index.php/logbook_coachee/filltoday/new" class="btn btn-primary"> 
				<?php echo $lang[128];?></a> <br/> <br/> </div>
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span><?php echo $lang[127];?> 
							</span>
                        </div>
                        <div class="panel-body pn">
                            <div class="table-responsive" style="height:800px; overflow:auto;">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th style="width:30%;"><?php echo $lang[126];?></th>
											<th style="width:30%;"><?php echo $lang[87];?></th>
											<th style="width:30%;"><?php echo $lang[129];?></th>
											<th style="width:10%;"> <?php echo $lang[102];?> </th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php echo $result;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- end col-md-12 -->
				
				
					
			</form>      
        </div>
	</div>
            <!-- End: Content -->  
</section>
    <!-- End: Content-Wrapper -->




