<?php include 'sidemenu.php'; ?>
    <!-- End: sidemenu -->
   
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper"
	<!-- Start: Topbar -->
	<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					<a href="#"> <?=$lang[106]?> </a>
				</li>
			</ol>
		</div>
	</header>
	<!-- End: Topbar -->
	<?php 
if($id!=""){
$show="in";
}
?>
    <!-- Begin: Content -->
    <div id="content" class="animated fadeIn">
        <div class="row">
			<form class="form-horizontal" action="" method="post">
			<div class="p25 br-t">
			<?php if($_GET['msg']){?>
			<div class="col-md-12">
			<div class="alert alert-system dark alert-dismissable">
			<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
			<i class="fa fa-check pr10"></i>
			<strong> <?php print($msg.$_GET['msg']);?> </strong>
			</div>
			</div>
			<?php }?>
<!---========== Add Documents ==========================------------------------------------------->
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> <?=$lang[106]?>
							</span>
                        </div>
                        <div class="panel-body pn">
							<div class="col-md-12"> &nbsp; </div>
							<div class="col-md-6">
								<div class="admin-form">
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[100]?> :</label>
									<div class="col-lg-8">
									<input id="inputStandard" class="form-control" name="term" type="text">
									</div>
									</div>
								</div>	
							</div>
							
							<div class="col-md-6">
								<div class="admin-form">
									<div class="form-group">
								
									<div class="col-lg-8">
									<button class="btn active btn-success btn-midium" type="submit" name="search">
									<i class="fa fa-check-circle-o"></i> <?=$lang[107]?> </button>
									
									<button class="btn active btn-system btn-midium" type="submit">
									<i class="fa fa-refresh"></i>
									Reset
									</button>
									</div>
									</div>
								</div>	
							</div>
                        </div>
                    </div>
                </div><!-- end col-md-12 -->
			</form>	
<!---==========  Documents List ==========================------------------------------------------->	
			<form action="" class="form-horizontal" method="post">
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> <?=$lang[106]?>
							</span>
							<a data-toggle="collapse" class="accordion-toggle collapsed" data-parent="#accordion1" href="#accord1"> <span>
							<button style="padding:9px; margin-right:-7px;" class="btn btn-success btn-sm light fw600 ml10 pull-right" type="button">
							<i class="fa fa-plus"></i>
							<?=$lang[104]?>
							</button> </span> </a>
                        </div>
						<div id="accordion1" class="accordion">
						<div id="accord1" class="accordion-body <?=$show?> collapse">
							<div class="panel-body">
							<div class="col-md-6">
								<div class="admin-form">
									<div class="form-group">
									<label class="col-lg-4 control-label"> <?=$lang[34]?>  :</label>
									<div class="col-lg-8">
									<input class="form-control datepicker" type="text"name="Date"  placeholder="YYYY-MM-DD"  value="<?php echo $knowledge_info[0]->Date;?>" required="required" readonly>
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[101]?>:</label>
									<div class="col-lg-8">
									<input class="form-control" type="text" name="Subject"  
									value="<?php echo $knowledge_info[0]->Subject;?>" required>
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[33]?> :</label>
									<div class="col-lg-8">
									<textarea class="gui-textarea" placeholder="Default Text" name="Description">
									<?php echo $knowledge_info[0]->Description;?>
									</textarea>
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> &nbsp; </label>
									<div class="col-lg-4">
										<button class="btn active btn-success btn-block" name="knowledge" type="submit">
										<i class="fa fa-save"></i>
										<?=$lang[105]?>
										</button>
									</div>
									<div class="col-lg-4">
										<a href="<?=$this->config->base_url();?>index.php/knowledge" style="text-decoration:none;">
										<button type="button" class="btn btn-warning btn-block"> <i class="fa fa-warning"></i> 
										Cancel </button> </a>
									</div>
									</div>
									
								</div>		
							</div>
							</div>
						</div>
					</div>
                        <div class="panel-body pn">
                            <div class="table-responsive" style="height:720px; overflow:auto;">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th style="width:30%;"><?=$lang[34]?></th>
											<th style="width:30%;"><?=$lang[101]?></th>
											<th style="width:30%;"><?=$lang[33]?></th>
											<th style="width:10%;"><?=$lang[102]?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
										
										<?php
										if($list_knowledge>0){

										for($r=0;$r<count($list_knowledge);$r++){?>	<tr>
										<td><?php echo $list_knowledge[$r]->Date?></td>
										<td><?php echo $list_knowledge[$r]->Subject?></td>
										<td><?php echo $list_knowledge[$r]->Description?></td>
										<td><a class="btn btn-info btn-xs" href="<?=$this->config->base_url();?>index.php/knowledge/index/<?php echo $list_knowledge[$r]->IDknowledge?>">
										<i class="fa fa-edit"></i> Edit<a/></td>

										</tr>
										<?php }}else{
										?>
										<tr><td colspan="5"><p style="color:red;" align="center"><?=$lang[103]?></p></td></tr>
										<?php 
										}?>
									
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- end col-md-12 -->
			</form>      
        </div>
	</div>
            <!-- End: Content -->  
</section>
    <!-- End: Content-Wrapper -->

