<?php include ('sidemenu.php'); ?>
    <!-- End: sidemenu -->
   
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper"
	<!-- Start: Topbar -->
	<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					<?php if (empty($test_info)) {?>
					<a href="#"> Add Test </a>
					<?php }else{?>
					<a href="#"> Edit Test </a>
					<?php }?>
				</li>
			</ol>
		</div>
	</header>
	<!-- End: Topbar -->

    <!-- Begin: Content -->
    <div id="content" class="animated fadeIn">
        <div class="row">
			<form class="form-horizontal" method="post" action="">

<!---========== Add test ==========================------------------------------------------->
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> <?php if (empty($test_info)) { echo "Add Test";}else{echo "Edit Test";}?>
							</span>
                        </div>
                        <div class="panel-body pn">
							<div class="col-md-12"> &nbsp; </div>
							<div class="col-md-6">
								<div class="admin-form">
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[85]?> :</label>
									<div class="col-lg-8">
									<input  class="form-control" type="text" name="NomeAvaliacao" value="<?=$test_info[0]->NomeAvaliacao ?>" required>
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[33]?> :</label>
									<div class="col-lg-8">
									<input  class="form-control" type="text" name="Description" value="<?=$test_info[0]->Descfricao ?>">
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[45]?> :</label>
									<div class="col-lg-8">
									<input  class="form-control" type="text" name="Objectives" value="<?=$test_info[0]->Objectives ?>">
									</div>
									</div>
								</div>	
							</div>
<!--############ BELOW START ADD MORE ###########################################################----------------------->						 <a href="#collapseTwo" data-parent="#accordion2" data-toggle="collapse" 
						class="accordion-toggle collapsed"> </a>	
						<?php if($test_quest[0]->IDQuest >0){ $style="in"; } ?>
						<div id="accordion2" class="accordion" >
						<div class="accordion-body in collapse" id="collapseTwo">
						<div class="accordion-inner">
						<div class="col-md-12"> <hr/> </div>
							<?php if($test_quest[0]->IDQuest<0){ ?>
							<div class="col-md-6">
								<div class="admin-form">
								
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[87]?>: </label>
									<div class="col-lg-8">
									<textarea class="gui-textarea" name="question[]" id="form-field-8"> 
									<?=trim($test_quest[$r]->Question)?> </textarea>
					<input id="form-field-6" type="hidden" name="IDQuest[]" value="<?=$test_quest[$r]->IDQuest?>"  >
					<input id="form-field-6" type="hidden" name="iDAlternativa[]" value="<?=$test_quest[$r]->iDAlternativa?>">
									</div>
									</div>
								
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[89]?> :</label>
									<div class="col-lg-8">
									<label class="field select">
										<select class="t_a"  name="t_a[]"  required>
											<option> </option>
											<option value="Radio" <?php if($test_quest[$r]->t_a=="Radio"){ echo "selected"; }?> >Radio Group</option>
											<option value="Checkbox" <?php if($test_quest[$r]->t_a=="Checkbox"){ echo "selected"; }?>>Check Box</option>
											<option value="Text" <?php if($test_quest[$r]->t_a=="Text"){ echo "selected"; }?>>Text Box</option>
										</select>
										<i class="arrow double"></i>
									</label>
									</div>
									<div class="option_set"></div>
									</div>
									
									
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="admin-form">
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[88]?> :</label>
									<div class="col-lg-4">
									<input  class="form-control" type="text" name="point[]" value="<?=$test_quest[$r]->Points?>"  onkeyup="this.value=this.value.replace(/[^0-9\.]/g,'');" required> 
									</div>
									<!---<div class="col-lg-4">
									<button class="btn btn-primary btn-sm"> <i class="fa fa-power-off"></i> Remove </button>
									</div>
									</div>---->
								</div>
							</div>
							<?php }else{ for($r=0;$r<count($test_quest);$r++){ ?>
							<div class="col-md-6">
								<div class="admin-form">
								
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[87]?> : </label>
									<div class="col-lg-8">
									<textarea class="gui-textarea" name="question[]" id="form-field-8"> 
									<?=trim($test_quest[$r]->Question)?> </textarea>
					<input id="form-field-6" type="hidden" name="IDQuest[]" value="<?=$test_quest[$r]->IDQuest?>"  >
					<input id="form-field-6" type="hidden" name="iDAlternativa[]" value="<?=$test_quest[$r]->iDAlternativa?>">
									</div>
									</div>
								
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[89]?>:</label>
									<div class="col-lg-8">
									<label class="field select">
										<select class="t_a"  name="t_a[]"  required>
											<option> </option>
											<option value="Radio" <?php if($test_quest[$r]->t_a=="Radio"){ echo "selected"; }?> >Radio Group</option>
											<option value="Checkbox" <?php if($test_quest[$r]->t_a=="Checkbox"){ echo "selected"; }?>>Check Box</option>
											<option value="Text" <?php if($test_quest[$r]->t_a=="Text"){ echo "selected"; }?>>Text Box</option>
										</select>
										<i class="arrow double"></i>
									</label>
									</div>
									<div class="option_set">
									
									<?php if($test_quest[$r]->t_a=="Radio" || $test_quest[$r]->t_a=="Checkbox"){
									$data=explode("@",$test_quest[$r]->option_list);  ?>
									<div class="col-md-12"> <br/>
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> 1 .</label>
									<div class="col-lg-8">
									<input  class="form-control" type="text" name="options[]" value="<?=$data[0]?>" >
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> 2 .</label>
									<div class="col-lg-8">
									<input  class="form-control" type="text" name="options[]" value="<?=$data[1]?>" >
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> 3 .</label>
									<div class="col-lg-8">
									<input  class="form-control" type="text" name="options[]" value="<?=$data[2]?>" >
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> 4 .</label>
									<div class="col-lg-8">
									<input  class="form-control" type="text" name="options[]" value="<?=$data[3]?>" >
									</div>
									</div>
									</div>
									<?php }else{ ?>
									<input id="form-field-6" name="options[]" style="width:120px;" value="0" type="hidden">
									<input id="form-field-6" name="options[]" style="width:120px;" value="0" type="hidden">
									<input id="form-field-6" name="options[]" style="width:120px;" value="0" type="hidden">
									<input id="form-field-6" name="options[]" style="width:120px;" value="0" type="hidden">
									<?php }?>
									
									</div>
									</div>
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="admin-form">
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[88]?> :</label>
									<div class="col-lg-4">
									<input  class="form-control" type="text" name="point[]" value="<?=$test_quest[$r]->Points?>"  onkeyup="this.value=this.value.replace(/[^0-9\.]/g,'');" required> 
									</div>
									</div>
								</div>
							</div>
							<div class="col-md-12"> <hr/> </div>
							<?php }}?>
							<div class="addnewquestion">
	
	
							</div>	
							
							<div class="col-md-6">
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"> &nbsp; </label>
								<div class="col-lg-8">
								<button class="btn active btn-success" type="submit" name="test">
								<i class="fa fa-save"></i>  <?php echo $lang[56]?> </button>
								
								<a href="<?php echo base_url();?>index.php/toolbox/" class="btn active btn-warning">
								<i class="fa fa-warning"></i>
								<?php echo $lang[422]?>
								</a>
								
								<button class="btn active btn-success new_question" type="button"> 
								<i class="fa fa-plus"></i>  <?php echo $lang[423]?> </button>
								
								</div>	
								</div>
							</div>
						</div><!-- end col-md-12 -->	
					</div>
				</div>
			</form>      
        </div>
	</div>
            <!-- End: Content -->  
</section>
    <!-- End: Content-Wrapper -->

