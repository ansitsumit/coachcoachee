<?php include 'sidemenu.php'; ?>
    <!-- End: sidemenu -->
   
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper"
	<!-- Start: Topbar -->
	<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					<a href="#"> My Activities </a>
				</li>
			</ol>
		</div>
	</header>
	<!-- End: Topbar -->

    <!-- Begin: Content -->
    <div id="content" class="animated fadeIn">
        <div class="row">
			<form class="form-horizontal" action="" method="post" enctype="multipart/form-data">

<!---========== Add Documents ==========================------------------------------------------->
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> My Activities
							</span>
                        </div>
                        <div class="panel-body pn" style="min-height:880px;">
							<div class="col-md-12"> &nbsp; </div>
							<div class="col-md-6">
								<div class="admin-form">
									<div class="form-group">
									<label class="col-lg-2"> <b> <?=$lang[24]?>: </b> </label>
									<div class="col-lg-10">
									<label> <?=$activity[0]->Activity?> </label>
									</div>
									</div> 
									
									<div class="form-group">
									<label class="col-lg-2"> <b> <?=$lang[36]?>: </b> </label>
									<div class="col-lg-10">
									<label> <?=$activity[0]->StartDate?> </label>
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-2"> <b> <?=$lang[33]?>: </b> </label>
									<div class="col-lg-10">
									<label> <?=$activity[0]->Description?></label>
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-2"> <b> Dificuly: </b> </label>
									<div class="col-lg-10">
									<label> <?=$activity[0]->GrauDificuldade?></label>
									</div>
									</div>
									
								</div>	
							</div>
							
							<div class="col-md-6">
								<div class="admin-form">
									<div class="form-group">
									<label class="col-lg-2"> <b> &nbsp; </label>
									<div class="col-lg-10">
									<label> &nbsp; </label>
									</div>
									</div>
								</div>	
								
								<div class="admin-form">
									<div class="form-group"> 
									<label class="col-lg-4 control-label"> <b> DataEntrega : </b></label>
									<div class="col-lg-8">
									<input class="form-control datepicker22" type="text" name="DataEntrega" value="<?=$activity[0]->DataEntrega?>">
									</div>
									</div>
								</div>	
								
								
								<div class="admin-form">
									<div class="form-group">
									<label class="col-lg-4" style='text-align:right;'> <b> Deadline : </b></label>
									<div class="col-lg-8">
									<label> <?=$activity[0]->Deadline?> </label>
									</div>
									</div>
								</div>	
								
								<div class="admin-form">
									<div class="form-group">
									<label class="col-lg-4" style='text-align:right;'> <b> Score : </b></label>
									<div class="col-lg-8">
									<label> <?=$view?> </label>
									</div>
									</div>
								</div>	
								
							</div>
							<input type="hidden" name="IDCoachee" value="<?=$activity[0]->IDCoachee?>">
							<input type="hidden"name="IDCoach" value="<?=$activity[0]->IDCoach?>">
							<input type="hidden" name="IDAtividade" value="<?=$activity[0]->IDAtividade?>">
							<input type="hidden" name="Response" value="<?=$_SESSION['coach']?>">
							<div class="col-md-12"> <hr/> </div> <!-- end col-md-12 hr -->
							
							
							<div class="col-md-12" align="center">
								<button type="Submit" class="btn active btn-success save_answer" name="save_activites"  >
									<i class="fa fa-save"></i> Submit
								</button>
							</div>
		<!--###### Conversatiuon #################################------------>	
		
	<form action="" method="post">	
	<input class="form-field-6" type="hidden" value="<?=$activity[0]->IDAtividade ?>" name="IDAtividade" >
	<input class="form-field-6" type="hidden" value="<?=$activity[0]->IDCoach?>" name="IDCoach" >
	<input class="form-field-6" type="hidden" value="<?=$activity[0]->IDCoachee ?>" name="IDCoachee" >
	<input class="form-field-6" type="hidden" value="coachee@<?=$_SESSION['coach'] ?>" name="Response" >
					<div class="col-md-10">		<br/>									
					<div class="row-fluid"><div class="span8" style="margin-left:120px;">	<link href="<?=$this->config->base_url()?>assets/style_chat.css" rel="stylesheet" type="text/css"/>
                         
						 <div class="portlet">
						<div class="panel-headingcolor">
							<div class="caption">
								<i class="fa fa-comments"></i> Conversations
							</div>
							
						</div>
						<div class="portlet-body" id="chats">
							<div >
							<input type="hidden" name="email" value="<?=$email[0]->Email1?>">
							<div  style="height: 200px; overflow: scroll;" data-always-visible="1" data-rail-visible1="1">
								<ul class="chats">
								<?php
								for($dis=0;$dis<count($discussion);$dis++){
								$response=explode("@",$discussion[$dis]->Response);
								if($response[0]=="coachee"){
								
								$name=$discussion[$dis]->coach_Name;
								$align="out";
								 $image=$discussion[$dis]->coachee_Photo;
								
								}
								else{
								$name=$discussion[$dis]->coachee_Name;
								$align="in";
								 $image=$discussion[$dis]->coach_Photo;
								}
								?>
									<li class="<?=$align?>">
										<img class="avatar img-responsive" alt="" src="<?=$this->config->base_url()?>coach_images/<?=$image?>">
										<div class="message">
											<span class="arrow">
											</span>
											<span  class="name"><?=stripslashes($name)?></span>
											<span class="datetime">
												 at <?=date("Y-m-d h:i A",strtotime($discussion[$dis]->Date." ".$discussion[$dis]->Time))?>
											</span>
											<span class="body">
										<?=stripslashes($discussion[$dis]->Discussion)?>	</span>
										</div>
									</li>
									<?php }?>
									</ul>
							</div>
							</div>
							<div class="chat-form">
								<div class="input-cont">
									<input class="form-control" name="msg" placeholder="Type a message here..." type="text" style="height:35px;" vrequired>
								</div>
								<div class="btn-cont" style="width:80px;" >
									
									<input type="submit" style="height:35px;" name="save_discuuss" class="btn btn-system btn-small"  value="Send" >
								</div>
							</div>
						</div>
					</div>	
									</form>	
							
                        </div>
                    </div>
                    </div>
                </div><!-- end col-md-12 -->
<!---==========  Documents List ==========================------------------------------------------->			
				

			</form>      
        </div>
	</div>
            <!-- End: Content -->  
</section>
    <!-- End: Content-Wrapper -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<script>

$(function() {
$( ".datepicker21" ).datepicker({ dateFormat: 'yy-mm-dd' });
});$(function() {
$( ".datepicker22" ).datepicker({ dateFormat: 'yy-mm-dd' });
});


</script>