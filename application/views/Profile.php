	<!--jqueryvalidation plugin-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/validate/validate.css">
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/validate/jquery.validate.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/validate/additional-methods.js"></script>
	<script>
		$(document).ready(function(){
			form =  $('#frmfirstprofile');				
			$('#frmfirstprofile').validate({
				submitHandler: function(form){
					var size = $('[type="checkbox"]:checked').length;
					console.log(size);
					if (size == 1) {
						console.log('invalid');
						alert('Please read the instructions and answer the questionnaire. (Or close this page if you do not want to fill');
					}else{
						console.log('valid');
						form.submit();
						
					}
				},
				rules: {
					email:{customemail: true},
					telefone:{
						required:true,
						phoneUS: true
						}
				},
				messages: {
					email:'Please Enter complete Email ID'
				}
				
			})
		});
	</script>
<script>
//	$(function(){
//		form =  $('#frmfirstprofile');
//$(".selector").validate({
//  submitHandler: function(form) {
//    // do other things for a valid form
//    form.submit();
//  }
//});
//		
//		$('#frmfirstprofile').submit(function(e){
//			frmvalid = form.valid();
//			if(!(frmvalid == true) && !(valid == true)) {
//				e.preventDefault();
//			}
//			
//					var size = $('[type="checkbox"]:checked').length;
//					if (size == 1) {
//						valid = false;
//						alert('Please read the instructions and answer the questionnaire. (Or close this page if you do not want to fill');
//					}else{
//						valid = true;
//					}
//					console.log(size);
//					console.log(valid);
//					console.log('frmvalid'+frmvalid);
//					if((frmvalid == true) && (valid == true)) {
//						console.log('submit now');
//							form.submit();
//					}else{
//						form.submit(function(e){
//							e.preventDefault();
//						});
//					}
//					
//		});
//	
//	});
</script>	
	
<?php include ('sidemenu.php');?>
    <!-- End: sidemenu -->
   
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper"
	<!-- Start: Topbar -->
	<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					<a href="javascript:void(0);">Profiler</a>
				</li>
			</ol>
		</div>
	</header>
	<!-- End: Topbar -->

    <!-- Begin: Content -->
    <div id="content" class="animated fadeIn">
        <div class="row">
			<div class="col-md-12">
			
				<div align="center">
				<center>
				<table border="0" cellpadding="2" cellspacing="0" width="950">
				<tr>
				<td width="100%"><font size="1" face="Arial">&nbsp;</font></td>
				</tr>
				<tr>
				<td width="100%">
				<script type="text/javascript">
					
				function countchk() {
					var size = $('[type="checkbox"]:checked').length;
					if (size <= 1) {
						return false;
						alert('Please read the instructions and answer the questionnaire. (Or close this page if you do not want to fill');
					}
					console.log(size);
					
				}
				function vcb(){
				box = document.getElementsByTagName('input');
				if (box.length  == 0) {
					alert('Please read the instructions and answer the questionnaire. (Or close this page if you do not want to fill');
					return false;
				}
				for(x = 0; x < box.length; x++) {
				if(box[x].checked) {r
				return true;
				} }
				alert("Por favor, leia as instru��es e responda ao question�rio. (Ou feche esta p�gina se n�o desejar preencher)");
				
				}
				</script>
				<p class="MsoTitle" align="center"><span style="mso-bidi-font-size: 7.5pt"><font face="Arial" size="4" color="#0000FF"><b>Questionnary - Part 1 of 2</b></font></span></p>
				<br><p class="MsoNormal" align="center" style="text-align:center"><b><span style="font-size: 9.0pt; mso-bidi-font-size: 7.5pt; font-family: Verdana"><font color="#0000FF"><br>INSTRUCTIONS:</font></span></b></p>
				<blockquote>
				<p class="MsoNormal" align="left"><span style="font-size: 9.0pt; mso-bidi-font-size: 7.5pt; font-family: Verdana">
				<font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;<b>Read with attention:</b><br><b>1)</b> Check the items that you think that part of your temperament.<br><b>2)</b> If you do not know the meaning of a word, search for it in a dictionary.<br><b>3)</b> Do not overthink, check by your intuition.<br><b>4)</b> Remember, do not check what you would like to be, but rather, what you really are.<br><b>5)</b> Analyze both your strengths as your negatives and be as honest as possible.</font></p>
				</blockquote>

				<p>&nbsp;</p>
				<form id="frmfirstprofile" class="form-horizontal" method="POST" action="<?=$this->config->base_url()?>index.php/profiler/index/2"  name="FrontPage_Form1">
				<div align="center">
				<table width="414" border="0" cellspacing="0" cellpadding="0">
				<tbody>
				<tr>
				<td width="79" align="right"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Name:</font></td>
				<td width="331"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" maxlength="200" size="42" obrigatorio="1" class="form-control" name="nome" descricao="Nome" required></font></td>
				</tr>
				<tr>
				<td width="79" align="right"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Email:</font></td>
				<td width="331"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;<input type="email" maxlength="200" size="42" id="email" name="email" class="form-control" descricao="E-mail" obrigatorio="1" required></font></td>
				</tr>
				<tr>
				<td width="79" align="right"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Phone:&nbsp;</font></td>
				<td width="500"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" maxlength="200" size="27" id="telefone" name="telefone" class="form-control" obrigatorio="1" descricao="E-mail"></font><br><font size="2" face="Verdana">1(xxx)-xxx-xxxx</font></td>
				</tr>
				<tr>
				<td width="79" align="right"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;CPF:&nbsp;</font></td>
				<td width="331"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" maxlength="200" size="42" name="cpf" obrigatorio="1" class="form-control" descricao="E-mail"></font></td>
				</tr>
				<tr>
				<td width="79" align="right"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Gender:</font></td>
				<td width="331"><select class="form-control" name="sexo" style="margin-top:18px;">
				<option value="masculino">Male</option>
				<option value="feminino">Female</option>
				</select></td>
				</tr>
				</tbody>
				</table>
				<br> <br> <br> <br><blockquote>
				<p class="MsoNormal" align="left"><span style="font-size: 9.0pt; mso-bidi-font-size: 7.5pt; font-family: Verdana">
				<font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;<b>Read with attention:</b><br><b>1)</b> Check the items that you think that part of your temperament.<br><b>2)</b> If you do not know the meaning of a word, search for it in a dictionary.<br><b>3)</b> Do not overthink, check by your intuition.<br><b>4)</b> Remember, do not check what you would like to be, but rather, what you really are.<br><b>5)</b> Analyze both your strengths as your negatives and be as honest as possible.</font></p>
				</blockquote>

				</center>
				</div>

				<table i class="table  ">

				<tbody><tr>

				<td> <input type="checkbox" style="opacity:2" name="contagiante1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Contagious<br>
				</font><input type="checkbox" style="opacity:2" name="audacioso1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Audacious 
				</font><font face="Verdana" size="1">(bold)</font><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;<br>
				</font><input type="checkbox" style="opacity:2" name="indeciso1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;undecided<br>
				</font><input type="checkbox" style="opacity:2" name="equilibrado1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Balanced<br>
				</font><input type="checkbox" style="opacity:2" name="inseguro1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Insecure<br>
				</font><input type="checkbox" style="opacity:2" name="lider1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Leader<br>
				</font><input type="checkbox" style="opacity:2" name="ingenuo1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Naive<br>
				</font><input type="checkbox" style="opacity:2" name="leal1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Loyal<br>
				</font><input type="checkbox" style="opacity:2" name="dedicado1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Dedicated<br>
				</font><input type="checkbox" style="opacity:2" name="otimista1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Optimistic<br>
				</font><input type="checkbox" style="opacity:2" name="eficiente1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Efficient<br>
				</font><input type="checkbox" style="opacity:2" name="habilidoso1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Skillful<br>
				</font><input type="checkbox" style="opacity:2" name="corajoso1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Brave</font><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;<br>
				</font><input type="checkbox" style="opacity:2" name="comunicativo1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Communicative<br>
				</font><input type="checkbox" style="opacity:2" name="decidido1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Decided<br>
				</font><input type="checkbox" style="opacity:2" name="medroso1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Frightful<br>
				</font><input type="checkbox" style="opacity:2" name="ativo1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Active<br>
				</font><input type="checkbox" style="opacity:2" name="intolerante1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Intolerant<br>
				</font><input type="checkbox" style="opacity:2" name="pretensioso1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Pretentious<br>
				</font><input type="checkbox" style="opacity:2" name="persistente1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Persistent<br>
				</font><input type="checkbox" style="opacity:2" name="barulhento1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Noisy<br>
				</font><font size="2" face="Verdana"></font><input type="checkbox" style="opacity:2" name="animado1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Lively<br>
				</font><input type="checkbox" style="opacity:2" name="pratico1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Practical<br>
				</font><input type="checkbox" style="opacity:2" name="minucioso1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Thorough<br>
				</font><input type="checkbox" style="opacity:2" name="discreto1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Discrete<br>
				</font><input type="checkbox" style="opacity:2" name="desconfiado1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Suspicious<br>
				</font><input type="checkbox" style="opacity:2" name="autodisciplinado1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Self-Disciplined<br>
				</font><input type="checkbox" style="opacity:2" name="entusiasta1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Enthusiast<br>
				</font><input type="checkbox" style="opacity:2" name="C821" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Resolute
				</font><font face="Verdana" size="1">(decided)</font></p>
				</td>

				<td>
				<input type="checkbox" style="opacity:2" name="sensivel1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Sensitive<br>
				</font><input type="checkbox" style="opacity:2" name="critico1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Critical<br>
				</font><input type="checkbox" style="opacity:2" name="exigente1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Demanding<br>
				</font><input type="checkbox" style="opacity:2" name="influenciador1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Influencer<br>
				</font><input type="checkbox" style="opacity:2" name="empolgante1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Breathtaking<br>
				</font><input type="checkbox" style="opacity:2" name="desmotivado1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Unmotivated<br>
				</font><input type="checkbox" style="opacity:2" name="insensivel1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Insensitive<br>
				</font><input type="checkbox" style="opacity:2" name="antisocial1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Antisocial<br>
				</font><input type="checkbox" style="opacity:2" name="estimulante1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Stimulant<br>
				</font><input type="checkbox" style="opacity:2" name="inflexivel1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Inflexible<br>
				</font><input type="checkbox" style="opacity:2" name="vingativo1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Vindictive<br>
				</font><input type="checkbox" style="opacity:2" name="extrovertido1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Extroverted<br>
				</font><input type="checkbox" style="opacity:2" name="orgulhoso1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Proud<br>
				</font><input type="checkbox" style="opacity:2" name="teorico1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Theoretical<br>
				</font><input type="checkbox" style="opacity:2" name="exagerado1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Exaggerated<br>
				</font><input type="checkbox" style="opacity:2" name="firme1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Firm<br>
				</font><input type="checkbox" style="opacity:2" name="modesto1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Modest<br>
				</font><input type="checkbox" style="opacity:2" name="alegre1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Cheerful<br>
				</font><input type="checkbox" style="opacity:2" name="racional1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Rational<br>
				</font><input type="checkbox" style="opacity:2" name="rotineiro1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Warkaday<br>
				</font><input type="checkbox" style="opacity:2" name="calculista1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Calculative<br>
				</font><font size="2" face="Verdana"></font><input type="checkbox" style="opacity:2" name="bemquisto1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Well-Cyst<br>
				</font><input type="checkbox" style="opacity:2" name="destacado1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Highlighted<br>
				</font><input type="checkbox" style="opacity:2" name="pessimista1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Pessimist<br>
				</font><input type="checkbox" style="opacity:2" name="metodico1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Methodical<br>
				</font><input type="checkbox" style="opacity:2" name="calmo1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Calm<br>
				</font><input type="checkbox" style="opacity:2" name="tranquilo1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Quiet<br>
				</font><input type="checkbox" style="opacity:2" name="espalhafatoso1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Fussy<br>
				</font><input type="checkbox" style="opacity:2" name="vaidoso1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Vain
				</font>

				</td>
				<td>
				<input type="checkbox" style="opacity:2" name="bemhumorado1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Good-tempered<br>
				</font><input type="checkbox" style="opacity:2" name="simpatico1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Nice<br>
				</font><input type="checkbox" style="opacity:2" name="impaciente1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Impatient<br>
				</font><input type="checkbox" style="opacity:2" name="bomcompanheiro1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Good Fellow<br>
				</font><input type="checkbox" style="opacity:2" name="arrogante1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Arrogant<br>
				</font><input type="checkbox" style="opacity:2" name="egocentrico1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Egocentric<br>
				</font><input type="checkbox" style="opacity:2" name="conservador1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Conservative<br>
				</font><input type="checkbox" style="opacity:2" name="desorganizado1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Disorganized<br>
				</font><input type="checkbox" style="opacity:2" name="frio1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Cold<br>
				</font><input type="checkbox" style="opacity:2" name="cumpridor1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Punctilious<br>
				</font><input type="checkbox" style="opacity:2" name="popular1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Popular<br>
				</font><input type="checkbox" style="opacity:2" name="paciente1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Patient<br>
				</font><input type="checkbox" style="opacity:2" name="sentimental1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Sentimental<br>
				</font><input type="checkbox" style="opacity:2" name="depressivo1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Depressing<br>
				</font><input type="checkbox" style="opacity:2" name="reservado1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Reserved<br>
				</font><input type="checkbox" style="opacity:2" name="energico1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Energetic<br>
				</font><input type="checkbox" style="opacity:2" name="perfeccionista1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Perfctionist<br>
				</font><input type="checkbox" style="opacity:2" name="sincero1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Sincere<br>
				</font><input type="checkbox" style="opacity:2" name="idealista1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Idealistic<br>
				</font><input type="checkbox" style="opacity:2" name="indisciplinado1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Undisciplined<br>
				</font><input type="checkbox" style="opacity:2" name="autosuficiente1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Self-Sufficient<br>
				</font><input type="checkbox" style="opacity:2" name="exuberante1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Lush<br>
				</font><input type="checkbox" style="opacity:2" name="introvertido1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Introvert<br>
				</font><input type="checkbox" style="opacity:2" name="independente1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Independent<br>
				</font><input type="checkbox" style="opacity:2" name="egoista1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Selfish<br>
				</font><input type="checkbox" style="opacity:2" name="temeroso1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Fearful<br>
				</font><input type="checkbox" style="opacity:2" name="procrastinador1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Procrastinator<br>
				</font><input type="checkbox" style="opacity:2" name="sarcastico1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Sarcastic<br>
				</font><input type="checkbox" style="opacity:2" name="compreensivo1" value="ON"><font size="2" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;Comprehensive</font>

				</td>

				</tr>
				</tbody>
				</table>
				<input type="hidden" name="horainicio" value="<?php echo date('H:i:s'); ?>">
				<p align="center"><input type="submit"  class="btn btn-system" value="    Go to Final Part    " name="B1"></p></form>


				</td> </tr>

				</td>
				</tr>
				<tr>
				<td width="100%"><font size="1" face="Arial">&nbsp;</font></td>
				</tr>
				</table>
				</center>
				</div>
			</div><!-- end col-md-12 -->   
        </div>
	</div>
            <!-- End: Content -->  
</section>
    <!-- End: Content-Wrapper -->




