<?php include 'sidemenu.php'; ?>
    <!-- End: sidemenu -->
   
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper"
	<!-- Start: Topbar -->
	<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					<a href="#"> <?=$lang[124]?> </a>
				</li>
			</ol>
		</div>
	</header>
	<!-- End: Topbar -->

    <!-- Begin: Content -->
    <div id="content" class="animated fadeIn">
        <div class="row">
			<form class="form-horizontal" role="form">

<!---==========  Report List ==========================------------------------------------------->			
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> <?=$lang[124]?>
							</span>
                        </div>
                        <div class="panel-body pn">
                            <div class="table-responsive" style="height:720px; overflow:auto;">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th style="width:30%;"><?=$lang[34]?></th>
											<th style="width:30%;"><?=$lang[205]?></th>
											<th style="width:30%;"><?=$lang[426]?></th>
											<th style="width:10%;"><?=$lang[427]?> </th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php 
										for($r=0;$r<count($total_compelete_session);$r++){
										?>
										<tr>
											<td> <?=$total_compelete_session[$r]->Date?> </td>
											<td> <?=$total_compelete_session[$r]->Value?></td>
											<td> <?=$total_compelete_session[$r]->a2?> </td>
											<td> <?=$total_compelete_session[$r]->a1?> </td>
										</tr>
										<?php } ?>
									
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- end col-md-12 -->

			</form>      
        </div>
	</div>
            <!-- End: Content -->  
</section>
    <!-- End: Content-Wrapper -->

