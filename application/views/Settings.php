<?php include 'sidemenu.php'; $_SESSION['demo'] = $color[0]->Color; ?>
    <!-- End: sidemenu -->
   
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper"
	<!-- Start: Topbar -->
	<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					<a href="#"> Settings </a>
				</li>
			</ol>
		</div>
	</header>
	<!-- End: Topbar -->

    <!-- Begin: Content -->
    <div id="content" class="animated fadeIn">
        <div class="row">
			<form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
			<?php if($_GET['msg']){?>
			<div class="col-md-12">
			<div class="alert alert-system dark alert-dismissable">
			<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
			<i class="fa fa-check pr10"></i>
			<strong> <?=@$_GET['msg']?> </strong>
			</div>
			</div>
			<?php }?>
<!---========== Add Documents ==========================------------------------------------------->
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> Settings
							</span>
                        </div>
                        <div class="panel-body pn">
							<div class="col-md-12"> &nbsp; </div>
							<div class="col-md-6">
								<div class="admin-form">
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[95]?> : </label>
									<div class="col-lg-8">
									<label class="field select">
										<select name="Language" required>
											<option> </option>
											<?php 
											for($r=0;$r<count($list_languages);$r++){
											$check="";
											if($info[0]->Language==$list_languages[$r]->id){
											$check="selected";
											}
											?>
											<option value="<?php echo $list_languages[$r]->id?>" <?=$check?>> <?php echo $list_languages[$r]->name?></option>
											<?php }?>
										</select>
										<i class="arrow double"></i>
									</label>
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[97]?> : </label>
									<div class="col-lg-8">
									<div class="section">
									<label class="field prepend-icon append-button file">
									<span class="button btn-primary">Choose File</span>
									<input id="file1" class="gui-file" type="file" name="Logo" onchange="document.getElementById('uploader1').value = this.value;">
									<input  class="gui-input" type="text" placeholder="Please Select a Logo">
									<input id="form-field-6" type="hidden" name="photo1" value="<?=$info[0]->Logo?>">
									<input id="form-field-6" type="hidden" name="IDConfig" value="<?=$info[0]->IDConfig?>">
									<label class="field-icon">
									<i class="fa fa-upload"></i>
									</label>
									</label>
									</div>
									</div>  
									</div>
									
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[96]?> :</label>
									<div class="col-lg-8">
									<input  class="form-control" type="text" value="<?=$info[0]->ContaPaypal?>" name="ContaPaypal">
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[431]?> : </label>
									<div class="col-lg-8">
									<textarea class="gui-textarea" name="code"> <?=$info[0]->code?></textarea>
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[432]?> </label>
									<div class="col-lg-8">
									<label class="field select">
										<select name="Enable">
											<option> </option>
											<option value="1" <? if($select[0]->Enable=="1"){echo "selected";}?>> Enable </option>
											<option value="0" <? if($select[0]->Enable=="0"){echo "selected";}?>> Disable </option>
										</select>
										<i class="arrow double"></i>
									</label>
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="cp3"><?=$lang[306]?> :</label>
									<div class="col-lg-8">
									<div class="input-group colorpicker-component demo demo-auto cursor colorpicker-element">
									<span class="input-group-addon">
									<i style="background-color: rgb(164, 80, 80);"></i>
									</span>
									<input class="form-control" type="text" name="Color" value="<?=$color[0]->Color?>">
									</div>
									</div>
									</div>
								</div>	
							</div>
							
							<div class="col-md-6">
								<div class="admin-form">
									<div class="col-md-6">
										<div class="fileupload fileupload-new admin-form" data-provides="fileupload">
											<div class="fileupload-preview thumbnail mb20">
												<img width="150" height="69" src="<?=$this->config->base_url();?>coach_images/<?=$info[0]->Logo?>" alt="Logo">
											</div>
										</div>
									</div>
								</div>	
							</div>
							<div class="col-md-12"> &nbsp; </div>
							<div class="col-md-6">
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"> &nbsp; </label>
								<div class="col-lg-4">
								<button class="btn active btn-success btn-block" type="submit" name="settings">
								<i class="fa fa-save"></i>  <?=$lang[56]?> </button>
								</div>
								<div class="col-lg-4">
								<button class="btn active btn-system btn-block" type="reset"> 
								<i class="fa fa-refresh"></i>  <?=$lang[108]?> </button>
								</div>
								</div>
							</div>	
                        </div>
                    </div>
                </div><!-- end col-md-12 -->		
			</form>      
        </div>
	</div>
            <!-- End: Content -->  
</section>
    <!-- End: Content-Wrapper -->

