<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class side_model extends CI_Model {
 function __construct() {
        parent::__construct();
    $this->load->model("language_model");
 }

  
 function coach($lang)
  {
     $rt=$this->language_model->get_languages($lang);

     $data=array(
		array($rt[0],"index.php/home/index/Dashboard"),
		array($rt[1],"index.php/home/index/Coachees"),
		array($rt[2],"index.php/home/index/Tests"),
		array($rt[125],"index.php/log_book"),
		array($rt[3],"index.php/Documents"),
		array("schedule","index.php/home/index/schedule"),
		array($rt[4],"index.php/home/index/coachmydata"),
		array($rt[5],"index.php/home/index/Settings"),
		array($rt[6],"index.php/BuyCredits"),
		array($rt[7],"index.php/knowledge"),
		array($rt[124],"index.php/report"),
		array($rt[8],"index.php/home/index/help"),);
	return $data;
  }


 function coachee($lang)
  {
   $rt=$this->language_model->get_languages($lang);

   
	     $data=array(
		array($rt[0],"index.php/home_coachee/"),
	array($rt[125],"index.php/logbook_coachee"),
	array($rt[9],"index.php/profiler"),
     	array($rt[109],"index.php/wheelif"),
		array($rt[10],"index.php/MyActivities"),
		array($rt[11],"index.php/Testing"),
		array($rt[3],"index.php/Documents1/"),
		array($rt[4],"index.php/coacheemydata"),
		array($rt[14],"index.php/Payments"));
	return $data;
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */