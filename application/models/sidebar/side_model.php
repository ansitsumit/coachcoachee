<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class side_model extends CI_Model {
 function __construct() {
        parent::__construct();
    $this->load->model("language_model");
 }

  
 function coach($lang)
  {
     $rt=$this->language_model->get_languages($lang);

     $data=array(
		array($rt[0],"index.php/home/index/Dashboard","glyphicons glyphicons-home"),
		array($rt[1],"index.php/home/index/Coachees","glyphicons glyphicons-user_add"),
		array($rt[206],"index.php/toolbox/","glyphicons glyphicons-settings"),
		//array($rt[2],"index.php/home/index/Tests"),
		//array($rt[125],"index.php/log_book"),
		array($rt[3],"index.php/Documents","glyphicons glyphicons-book_open"),
		array($rt[425],"index.php/home/index/schedule","glyphicons glyphicons-alarm"),
		array($rt[4],"index.php/home/index/coachmydata","glyphicons glyphicons-user"),
		array($rt[5],"index.php/home/index/Settings","glyphicons glyphicons-cogwheels"),
		array($rt[6],"index.php/BuyCredits","glyphicons glyphicons-usd"),
		array($rt[7],"index.php/knowledge","glyphicons glyphicons-sun"),
		array($rt[124],"index.php/report","glyphicons glyphicons-notes_2"),
		array($rt[8],"index.php/home/index/help","glyphicons glyphicons-snowflake"),);
	return $data;
  }


 function coachee($lang)
  {
   $rt=$this->language_model->get_languages($lang);
   
	    $data=array(
		array($rt[0],"index.php/home_coachee/","glyphicons glyphicons-home"),
		array($rt[125],"index.php/logbook_coachee","glyphicons glyphicons-notes_2"),
		array($rt[206],"index.php/Testing","glyphicons glyphicons-settings"),
		array($rt[9],"index.php/profiler","glyphicons glyphicons-user"),
     	//array($rt[109],"index.php/wheelif"),
		array($rt[10],"index.php/MyActivities","glyphicons glyphicons-sun"),
		//array($rt[11],"index.php/Testing"),
		array($rt[3],"index.php/Documents1/","glyphicons glyphicons-book_open"),
		array($rt[4],"index.php/coacheemydata","glyphicons glyphicons-user"),
		array($rt[14],"index.php/Payments","glyphicons glyphicons-usd"));
		return $data;
		
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */