<?php
class model_data extends CI_Model {

function __construct() {
parent::__construct();
}

function get_goal_data($id){

$this->db->select("*");
$this->db->from("goals");
$this->db->where("IDgoal",$id) ;
$query=$this->db->get();
foreach($query->result() as $row){
$data[]=$row;

}

return $data ;
}

function session_expire_redirect($a){
if(isset($_SESSION['RoleID'])){
if($a!=$_SESSION['RoleID']){
session_destroy();
redirect($this->config->base_url());
exit();
}
}
if(empty($_SESSION['coach'])){

redirect($this->config->base_url());
exit();
}
}
function update_msg(){

return "Your Data has been Updated";
}

function email_allready_msg(){

return "Email Id already exists";
}
function register_msg(){

return "You have registered Successfully";
}


function submit_msg(){

return "Your Data has been Submited";
}
function delete_msg(){

return "Your Data has been delete";
}

function get_civilstates() {

$this->db->select("*");
$this->db->from("cilvilsates");
$query=$this->db->get();
foreach($query->result() as $row){
$data[]=$row;

}

return $data;
}    

function get_education() {

$this->db->select("*");
$this->db->from("education");
$query=$this->db->get();
foreach($query->result() as $row){
$data[]=$row;

}

return $data;
} 

function get_hierachical() {

$this->db->select("*");
$this->db->from("hierachicallevel");
$query=$this->db->get();
foreach($query->result() as $row){
$data[]=$row;

}

return $data;
}  

function get_country() {

$this->db->select("*");
$this->db->from("countryinfo");
$query=$this->db->get();
foreach($query->result() as $row){
$data[]=$row;

}

return $data;
}

function coach_update($id)
{
if(strlen($_FILES['Photo']['name'])>3){
// echo $_FILES['photo']['name'];
$images=date("Ym-dHis").$_FILES['Photo']['name'];;
move_uploaded_file($_FILES['Photo']['tmp_name'],"coach_images/".$images);
} else{
$images=$_POST['photo1'];
}  
$this->Photo = $images; 
$this->Name = $_POST['Name']; 
$this->Email1 = $_POST['Email1']; 
$this->Email2 = $_POST['Email2']; 
$this->Password = $_POST['Password']; 
$this->PhoneResidential = $_POST['PhoneResidential']; 
$this->PhoneCommercial = $_POST['PhoneCommercial']; 
$this->Mobile = $_POST['Mobile']; 
$this->Document = $_POST['Document']; 
$this->CivilState = $_POST['CivilState']; 
$this->Education = $_POST['Education']; 
$this->HierachicalLevel = $_POST['HierachicalLevel']; 
$this->Profession = $_POST['Profession']; 
$this->Company = $_POST['Company']; 
$this->Birth = $_POST['Birth']; 
$this->Address = $_POST['Address']; 
$this->Neighborhood = $_POST['Neighborhood']; 
$this->City = $_POST['City']; 
$this->Country = $_POST['Country']; 
$this->ZipCode = $_POST['ZipCode']; 
$this->Observations = $_POST['Observations']; 
//$this->Activity = $_POST['Activity']; 
$this->DataInicioLicenca = $_POST['DataInicioLicenca']; 
$this->Credits = $_POST['Credits']; 
$this->DataFimLicenca = $_POST['DataFimLicenca']; 
$this->db->where('IDCoach',$id);
$this->db->update('coach',$this);


}


function get_coach_info($id){

$this->db->select("*");
$this->db->from("coach");
$this->db->where("IDCoach",$id);
$query=$this->db->get();
foreach($query->result() as $row){
$data[]=$row;

}
return  $data;
}
function get_session_data($id){

$this->db->select("*");
$this->db->from("sessions");
$this->db->where("IDSessoes",$id);
$query=$this->db->get();
foreach($query->result() as $row){
$data[]=$row;

}

return $data;
}

function get_Activities_data($id){

$this->db->select("*");
$this->db->from("activities");
$this->db->where("IDAtividade",$id);
$query=$this->db->get();
foreach($query->result() as $row){
$data[]=$row;

}

return $data;
}

function get_total_score($id,$field){

$query=$this->db->query("select sum(points) as point from log_gamification where ".$field."='".$id."'");
foreach($query->result() as $row){
$data[]=$row;

}

return $data;

}

function get_total_list($query){

$query=$this->db->query($query);
return $query->result_array();
//foreach($query->result() as $row){
//$data[]=$row;
//
//}

//return $data;

}

function get_update($query){

$query=$this->db->query($query);
}
function get_array_value($value){
$list;
foreach($value as $id)
{

$list[]=$id;
}
return $list;
}


function get_total_activiy_count($list){
for($r=0;$r<count($list);$r++){
$query=$this->db->query("select count(*) as total from activities where IDCoachee='".$list[$r]->IDCoachee."'");
foreach($query->result() as $row){
$data[]=$row;

}}


return $data;

}

function get_total_done_activiy_count($list){
for($r=0;$r<count($list);$r++){
$query=$this->db->query("select count(*) as total from activities where IDCoachee='".$list[$r]->IDCoachee."' and DataEntrega <='".date("Y-m-d")."' and DataEntrega!='0000-00-00'");
foreach($query->result() as $row){
$data[]=$row;

}}


return $data;

}


function get_total_session_count($list){
for($r=0;$r<count($list);$r++){
$query=$this->db->query("select count(*) as total  from sessions where IDCoachee='".$list[$r]->IDCoachee."'");
foreach($query->result() as $row){
$data[]=$row;

}}


return $data;

}
function get_total_done_session_count($list){
for($r=0;$r<count($list);$r++){
$query=$this->db->query("select count(*) as total  from sessions where IDCoachee='".$list[$r]->IDCoachee."' and complete='Yes' ");
foreach($query->result() as $row){
$data[]=$row;

}}


return $data;

}

function get_total_complete_goal($goal,$activity){
$rt;
for($r=0;$r<count($goal);$r++){
$query=$this->db->query("select IDAtividade from activities where goal='".$goal[$r]->IDgoal."'");
$query1= $this->db->query("select IDAtividade from activities where goal='".$goal[$r]->IDgoal."' and DataEntrega<='".date("Y-m-d")."' and DataEntrega!='0000-00-00'");

$rt[]=array($query->num_rows(),$query1->num_rows(),$goal[$r]->IDgoal);     

}

$total=0;
for($r=0;$r<count($rt);$r++){


if($rt[$r][2]>0 && $rt[$r][0]>0 && $rt[$r][1]>0){ 

if($rt[$r][0]==$rt[$r][1]){
$total++;   
 //$rt[$r][2];

$idlog = $this->db->query("select idlog  from log_gamification where goal='".$rt[$r][2]."'")->row()->idlog;;
if($idlog==""){
    
$value->idcoachee = $_SESSION['coach']; 
$value->points = "350"; 
$value->goal=$rt[$r][2]; 
$value->date = date("Y-m-d"); 
$value->hour = date("H:i:s"); 
$this->db->insert('log_gamification',$value); 

} 
}

}

}
return $total;

}

function save_activity_score($list){

for($r=0;$r<count($list);$r++){
$idlog = $this->db->query("select idlog  from log_gamification where activity='".$list[$r]->IDAtividade."'")->row()->idlog;;
if($idlog==""){
    
$value->idcoachee = $_SESSION['coach']; 
$value->points = "200"; 
$value->activity=$list[$r]->IDAtividade; 
$value->date = date("Y-m-d"); 
$value->hour = date("H:i:s"); 
$this->db->insert('log_gamification',$value); 

} 
}
}


function save_tmp_credit(){

$value->coach = $_SESSION['coach']; 
$value->credit =$_POST['amount_1']; 
$value->date =date("Y-m-d H:i:s"); 
$value->amount =$_POST['amount_2']; 
$this->db->insert('tmp_credit',$value); 
return $this->db->insert_id();
}
}




