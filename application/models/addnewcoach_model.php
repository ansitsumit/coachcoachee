<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class AddNewCoach_model extends CI_Model {


function __construct()
{
// Call the Model constructor
parent::__construct();
}



function Goals($id)
{
$this->IDCoach  = $_SESSION['coach'];
$this->IDCoachee  = $id;
$this->Description = $_POST['Description']; 
$this->Evidence = $_POST['Evidence']; 
$this->motivators = $_POST['motivators']; 
$this->saboteurs = $_POST['saboteurs']; 
$this->values = $_POST['values']; 
$this->DataEntrega = $_POST['DataEntrega']; 
$this->strategies = $_POST['strategies']; 
$this->resources = $_POST['resources']; 
$this->db->insert('goals', $this);


}
function Goals_update($id)
{
$this->Description = $_POST['Description']; 
$this->Evidence = $_POST['Evidence']; 
$this->motivators = $_POST['motivators']; 
$this->saboteurs = $_POST['saboteurs']; 
$this->values = $_POST['values']; 
$this->DataEntrega = $_POST['DataEntrega']; 
$this->strategies = $_POST['strategies']; 
$this->resources = $_POST['resources']; 
$this->db->where('IDgoal',$id);
$this->db->update('goals',$this);


}

function list_Goals($id){


$role1="";
$this->db->select('*');
$this->db->from('goals');
$this->db->where('IDCoachee',$id);
$query = $this->db->get();
//echo $this->db->last_query();
if ($query -> num_rows() > 0)
{
foreach($query->result() as $role)
{
$role1[]=$role;

}
}else
{
$role1=0;
}
return $role1;

}

function Sessions($id)
{
$this->IDCoach  = $_SESSION['coach'];
$this->IDCoachee  = $id;
$this->Notes = $_POST['Notes']; 
$this->complete = $_POST['complete']; 
$this->Date = $_POST['Date']; 
$this->Time = $_POST['Time']; 
$this->db->insert('sessions', $this);
}

function Sessions_update($id)
{
$this->Notes = $_POST['Notes']; 
$this->Date = date("Y-m-d",strtotime($_POST['Date'])); 
$this->complete = $_POST['complete']; 
$this->Time = $_POST['Time']; 
$this->db->where("IDSessoes",$id);
$this->db->update('sessions', $this);

}

function list_Sessions($id)
{
$role1="";
$this->db->select('*');
$this->db->from('sessions');
$this->db->where('IDCoachee',$id);
$query = $this->db->get();
//echo $this->db->last_query();
if ($query -> num_rows() > 0)
{
foreach($query->result() as $role)
{
$role1[]=$role;

}
}else
{
$role1=0;
}
return $role1;

}

function Activities($id)
{
$this->IDCoach  =$_SESSION['coach'];
$this->IDCoachee  = $id;
$this->StartDate = date("Y-m-d",strtotime($_POST['StartDate'])); 
$this->Description = $_POST['Description']; 
$this->goal = $_POST['goal']; 
$this->Deadline = date("Y-m-d",strtotime($_POST['Deadline'])); 
$this->Activity = $_POST['Activity']; 
$this->GrauDificuldade = $_POST['GrauDificuldade']; 
$this->Objectives = $_POST['Objectives']; 
$this->Pontuation = $_POST['Pontuation']; 
$this->Date = date("Y-m-d"); 
$this->Time = date("H:i:s"); 

$this->db->insert('activities', $this);

$a->idcoach=$_SESSION['coach'];
$a->date=date("Y-m-d");
$a->points="5";


$a->hour=date("H:i:s");
$this->db->insert("log_gamification",$a);

}

function Activities_update($id)
{
$this->StartDate = date("Y-m-d",strtotime($_POST['StartDate'])); 
$this->Description = $_POST['Description']; 
$this->goal = $_POST['goal']; 

$this->Deadline = date("Y-m-d",strtotime($_POST['Deadline'])); 
$this->Activity = $_POST['Activity']; 
$this->GrauDificuldade = $_POST['GrauDificuldade']; 
$this->Objectives = $_POST['Objectives']; 
$this->Pontuation = $_POST['Pontuation']; 
$this->db->where('IDAtividade', $id);
$this->db->update('activities', $this);


}

function list_Activities($id)
{
$role1="";
$this->db->select('*');
$this->db->from('activities');
$this->db->where('IDCoachee',$id);
$query = $this->db->get();
//echo $this->db->last_query();
if ($query -> num_rows() > 0)
{
foreach($query->result() as $role)
{
$role1[]=$role;

}
}
return $role1;

}

function total_Activities($id)
{
$role1="";
$query=$this->db->query($id);
if ($query -> num_rows() > 0)
{
foreach($query->result() as $role)		{
$role1[]=$role;

}}
return $role1;

}
function total_session($id)
{
$role1="";
$query=$this->db->query($id);
if ($query -> num_rows() > 0)
{
foreach($query->result() as $role)		{
$role1[]=$role;

}}
return $role1;

}



function total_goals($id)
{
$role1="";
$query=$this->db->query($id);
if ($query -> num_rows() > 0)
{
foreach($query->result() as $role)		{
$role1[]=$role;

}}
return $role1;

}
function get_data_coach($id)
{
$role1="";
$this->db->select('*');
$this->db->from('coachee');
$this->db->where('IDCoachee',$id);
//$this->db->join('cilvilsates', 'cilvilsates.id = coachee.IDCoachee');
$query = $this->db->get();
$query = $this->db->query("select cilvilsates.name,coachee.* from coachee 
left join cilvilsates  on  cilvilsates.id= coachee.CivilState
where coachee.IDCoachee='".$id."'");


if ($query -> num_rows() > 0)
{
foreach($query->result() as $role)
{
$role1[]=$role;

}
}else
{
$role1=0;
}
return $role1;

}



}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */