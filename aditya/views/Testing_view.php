<?php include ('sidemenu.php');?>
    <!-- End: sidemenu -->
   
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper"
	<!-- Start: Topbar -->
	<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					<a href="dashboard.html">ToolBox</a>
				</li>
			</ol>
		</div>
	</header>
	<!-- End: Topbar -->

    <!-- Begin: Content -->
    <div id="content" class="animated fadeIn">
        <div class="row">
			<form class="form-horizontal" role="form">
				<?php if($_GET['msg']){?>
				<div class="col-md-12">
				<div class="alert alert-system dark alert-dismissable">
				<button class="close" type="button" data-dismiss="alert" aria-hidden="true">x</button>
				<i class="fa fa-check pr10"></i>
				<strong> <?=$_GET['msg']?> </strong>
				</div>
				</div>
				<?php }?>
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span><?=$lang[201]?>
							</span>
                        </div>
                        <div class="panel-body pn">
                            <div class="table-responsive" style="height:600px; overflow:auto;">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th style="width:30%;"><?=$lang[44]?></th>
											<th style="width:30%;"><?=$lang[33]?></th>
											<th style="width:30%;"><?=$lang[45]?></th>
											<th style="width:10%;"><?=$lang[25]?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php
										$id;
										for($r=0;$r<count($test_answer);$r++){
											$id[]=$test_answer[$r]->IDAvaliacao;
										}
										for($r=0;$r<count($test_list);$r++){
										?>
										<td> <?=$test_list[$r]->NomeAvaliacao?> </td>
										<td> <?=$test_list[$r]->Descfricao?></td>
										<td> <?=$test_list[$r]->Objectives?> </td>
										<td>
                                        
                                          <?
                                          if(in_array($test_list[$r]->IDAvaliacao, $id)){
                                          ?>
                                            <a class="btn btn-success btn-xs" href="<?php echo $this->config->base_url()?>index.php/Testing/index/<?=$test_list[$r]->IDAvaliacao?>/View"> <i class="fa fa-eye"></i> <?=$lang[102]?>  </a> 
                                          <?php }?>
                                         
                                          <?
                                          if(!in_array($test_list[$r]->IDAvaliacao, $id)){
                                          ?>
                                         <a class="btn btn-system btn-xs" href="<?php echo $this->config->base_url()?>index.php/Testing/index/<?=$test_list[$r]->IDAvaliacao?>"> <i class="fa fa-eye"></i>  <?=$lang[25]?>  </a> 
										 
										 
                                          <?php }?>
                                        </td>  </tr>
										<?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- end col-md-12 -->
<!-------------------------------------------whell------------------------------------------------------------------->				
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span><?=$lang[202]?> 
							</span>
                        </div>
                        <div class="panel-body pn">
                            <div class="table-responsive" style="height:300px; overflow:auto;">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th style="width:30%;"><?=$lang[22]?></th>
											<th style="width:60%;"><?=$lang[33]?></th>
											<th style="width:10%;"><?=$lang[25]?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php
										for($r=0;$r<count($get_wheel_coachee_list);$r++){

										if($get_wheel_coachee_list[$r]->IDwheel>0){
										?>	<tr>
										<td><?=$get_wheel_coachee_list[$r]->Name?></td>
										<td><?=$get_wheel_coachee_list[$r]->Description?></td>

										<?php $status = get_status($get_wheel_coachee_list[$r]->IDwheel,$get_wheel_coachee_list[$r]->IDCoach,$_SESSION['coach']) ;?>
										<?php  if($status == 'action') { ?>
										<td><a class="btn btn-system btn-xs" href="<?=$this->config->base_url();?>index.php/Testing/coacheeans/<?=$get_wheel_coachee_list[$r]->IDwheel?>"> <i class="fa fa-eye"></i> <?=$lang[25]?>  </a></td>
										<?php }?>
										<?php  if($status == 'view') { ?>
										<td><a class="btn btn-success btn-xs" href="<?=$this->config->base_url();?>index.php/Testing/coacheeans/<?=$get_wheel_coachee_list[$r]->IDwheel?>/view"> <i class="fa fa-eye"></i> <?=$lang[102]?>  </a></td>
										<?php }?>
										</tr>
										<?php }}?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- end col-md-12 -->
<!-------------------------------------------video------------------------------------------------------------------->				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span><?=$lang[300]?>
							</span>
                        </div>
                        <div class="panel-body pn">
                            <div class="table-responsive" style="height:300px; overflow:auto;">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th style="width:30%;"><?=$lang[302]?></th>
											<th style="width:30%;"><?=$lang[33]?></th>
											<th style="width:30%;"><?=$lang[303]?></th>
											<th style="width:10%;"><?=$lang[25]?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php
										for($r=0;$r<count($get_video_coachee_list);$r++){

										if($get_video_coachee_list[$r]->IDvideo>0){
										?>	<tr>
										<td><?=$get_video_coachee_list[$r]->Title?></td>
										<td><?=$get_video_coachee_list[$r]->Description?></td>
										<td><?=$get_video_coachee_list[$r]->Purpose?></td>
										<td><a class="btn btn-system btn-xs" href="<?=$this->config->base_url();?>index.php/Testing/add_video_coachee/<?=$get_video_coachee_list[$r]->IDvideo?>"> <i class="fa fa-eye"></i> <?=$lang[25]?>  </a></td>
										</tr>
										
										<?php }}?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- end col-md-12 -->
				
					
			</form>      
        </div>
	</div>
            <!-- End: Content -->  
</section>
    <!-- End: Content-Wrapper -->




