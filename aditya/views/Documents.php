<?php include 'sidemenu.php'; ?>
    <!-- End: sidemenu -->
   
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper"
	<!-- Start: Topbar -->
	<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					<a href="#"> Documents </a>
				</li>
			</ol>
		</div>
	</header>
	<!-- End: Topbar -->

    <!-- Begin: Content -->
    <div id="content" class="animated fadeIn">
        <div class="row">
			<form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
				<h3 class=" smaller lighter" style="margin-left:40%; color:green;"><?
				print($msg);
				?></h3>
<!---========== Add Documents ==========================------------------------------------------->
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> <?=$lang[92]?>
							</span>
                        </div>
                        <div class="panel-body pn">
							<div class="col-md-12"> &nbsp; </div>
							<div class="col-md-6">
								<div class="admin-form">
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[93]?> :</label>
									<div class="col-lg-8">
									<input id="inputStandard" class="form-control" type="text"  name="FileName" required>
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[33]?> :</label>
									<div class="col-lg-8">
									<input id="inputStandard" class="form-control" type="text" name="Description">
									</div>
									</div>
				
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> &nbsp; </label>
									<div class="col-lg-8">
									<div class="section">
									<label class="field prepend-icon append-button file">
									<span class="button btn-primary">Choose File</span>
									<input class="gui-file" name="upload" type="file" required>
									<input  class="gui-input" type="text" placeholder="Please Select a Logo">
									<label class="field-icon">
									<i class="fa fa-upload"></i>
									</label>
									</label>
									</div>
									</div>  
									</div>
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard">
									&nbsp;
									</label>
									<div class="col-lg-8">
									<button class="btn active btn-success" type="submit" name="document">
									<i class="fa fa-upload"></i> <?=$lang[94]?> </button>
									</div>
									</div>
								</div>	
							</div>
							
							<div class="col-md-6">
								<div class="admin-form">
									&nbsp;
								</div>	
							</div>
                        </div>
                    </div>
                </div><!-- end col-md-12 -->
<!---==========  Documents List ==========================------------------------------------------->			
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> <?=$lang[3]?>
							</span>
                        </div>
                        <div class="panel-body pn">
                            <div class="table-responsive" style="height:600px; overflow:auto;">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th style="width:30%;"><?=$lang[90]?></th>
											<th style="width:60%;"><?=$lang[3]?></th>
											<th style="width:10%;"><?=$lang[91]?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
										
										<?php
 
										for($r=0;$r<count($list_documents);$r++){

										if($list_documents>0){
										?>	<tr>
										<td><?php echo $list_documents[$r]->FileName?></td>
										<td><?php echo $list_documents[$r]->Description?></td>
										
										<td>
											<a onclick="return confirm('Are you sure want to delete');" class="btn btn-danger btn-xs" href="<?php echo $this->config->base_url();?>index.php/Documents/index/<?=$list_documents[$r]->IDDocs?>">
											<i class="fa fa-trash-o"></i> <?=$lang[91]?>
											</a>
										</td>
												
										</tr>
										<?php }}?>
									
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- end col-md-12 -->

			</form>      
        </div>
	</div>
            <!-- End: Content -->  
</section>
    <!-- End: Content-Wrapper -->

