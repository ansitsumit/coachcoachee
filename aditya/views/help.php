<?php include 'sidemenu.php'; $_SESSION['demo'] = $color[0]->Color; ?>
    <!-- End: sidemenu -->
   
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper">
	<!-- Start: Topbar -->
	<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					<a href="#"> Help </a>
				</li>
			</ol>
		</div>
	</header>
	<!-- End: Topbar -->

    <!-- Begin: Content -->
    <div id="content" class="animated fadeIn">
        <div class="row">
			<form class="form-horizontal" role="form" method="post" action="" enctype="multipart/form-data">

<!---========== Add Documents ==========================------------------------------------------->
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> Help
							</span>
                        </div>
                        <div class="panel-body pn">
							<div class="col-md-12"> &nbsp; </div>
							<div class="row">
							<div class="col-md-6"> 
								<div class="admin-form">
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[101]?> :</label>
									<div class="col-lg-8">
									<input  class="form-control" type="text" type="text">
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[428]?>: </label>
									<div class="col-lg-8">
									<textarea class="gui-textarea" name="code"> </textarea>
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> &nbsp; </label>
									<div class="col-lg-8">
									<button class="btn active btn-success" type="submit" >
									<i class="fa fa-save"></i>  <?=$lang[56]?> </button>
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> &nbsp; </label>
									<div class="col-lg-8">
									 &nbsp; 
									</div>
									</div> 
									
									
									
								</div>	
							</div> 
							<div class="col-md-6"> 
							<div class="control-group">
									<iframe width="100%" height="300px" src="https://www.youtube.com/embed/" frameborder="0" allowfullscreen></iframe>
									</div>	
							</div>
							
							</div>
							<div class="row">
							<div class="col-sm-6">
							<br />
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> &nbsp; </label>
									<div class="col-lg-8">
									<button class="btn active btn-system" type="reset"> 
									<i class="fa fa-download"></i>  <?=$lang[430]?> </button>
									</div>
									</div>
							</div>
							
							<div class="col-md-6">
								<div class="admin-form">
									
									
									<div class="form-group help">
									<p> 
									Support : +55 (31) 2526-5652</br>
									Support@softwarecoaching.com.br
									
									</p>
									</div>
								</div>	
							</div>
							</div>
							<div class="col-md-12"> &nbsp; </div>
					
							<div class="col-md-12"> 
							<br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> 
							<br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/>
							</div>
                        </div>
                    </div>
                </div><!-- end col-md-12 -->		
			</form>      
        </div>
	</div>
            <!-- End: Content -->  
</section>
    <!-- End: Content-Wrapper -->

