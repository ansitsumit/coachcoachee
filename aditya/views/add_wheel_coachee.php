<?php include ('sidemenu.php'); $point = ''; ?>
    <!-- End: sidemenu -->
   
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper"
	<!-- Start: Topbar -->
	<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					<a href="#"> Wheels </a>
				</li>
			</ol>
		</div>
	</header>
	<!-- End: Topbar -->

    <!-- Begin: Content -->
    <div id="content" class="animated fadeIn">
        <div class="row">
			<form class="form-horizontal" action="" method="post" enctype="multipart/form-data">

<!---========== Add Documents ==========================------------------------------------------->
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> Wheels 
							</span>
                        </div>
                        <div class="panel-body pn" style="min-height:880px;">
							<div class="col-md-12"> &nbsp; </div>
							<div class="col-md-6">
								<div class="admin-form">
									<div class="form-group">
									<label class="col-lg-2"> <b> <?=$lang[58]?> </b> </label>
									<div class="col-lg-10">
									<label> <?php echo $question[0]->Name;?> </label>
									</div>
									</div>
								</div>	
							</div>
							
							<div class="col-md-6">
								<div class="admin-form">
									<div class="form-group">
									<label class="col-lg-2"> <b> <?=$lang[33]?> </b> </label>
									<div class="col-lg-10">
									<label> <?php echo $question[0]->Description;?> </label>
									<input type="hidden" name="whellID" id="whellID" value="<?php echo $question[0]->IDwheel?>" />
									<input type="hidden" name="IDCoach" id="IDCoach" value="<?php echo $question[0]->IDCoach?>" />
									</div>
									</div>
								</div>	
							</div>
							
							<div class="col-md-12"> <hr/> </div> <!-- end col-md-12 hr -->
							<?php //echo '<pre>'; print_r($question_list);?>
							<?php if(count($question_list)> 0){?>
							<?php for($i=0;$i<count($question_list);$i++){?>
							<div class="col-md-4">
								<div class="admin-form">
									<div class="form-group">
									<label class="col-lg-2"> <b> <?=$lang[58]?> </b> </label>
									<div class="col-lg-10">
									<label> <?php echo $question_list[$i]->Name;?> </label>
									</div>
									</div>
								</div>	
							</div>
							
							<div class="col-md-4">
								<div class="admin-form">
									<div class="form-group">
									<label class="col-lg-3"> <b> <?=$lang[33]?> </b> </label>
									<div class="col-lg-9">
									<label> <?php echo $question_list[$i]->Description;?> </label>
									</div>
									</div>
								</div>	
							</div>
							
							<div class="col-md-4">
								<div class="admin-form">
								<?php if($action == 'view'){ ?>
									<div class="form-group">
									<label class="col-lg-3">
									<?php $point = get_point($question[0]->IDwheel,$question[0]->IDCoach,$_SESSION['coach'],$question_list[$i]->IDwheelitens);?>
									</label>
									<div class="col-lg-9">
									<label> 
									<?php echo 'Points ('.$point.')'; ?>
									<input type="hidden" name="IDwheelitens[]" id="IDwheelitens" value="<?php echo $question_list[$i]->IDwheelitens?>" /> 
									</label>
									</div>
									</div>
									<?php } ?>
									<?php if($action != 'view'){ ?>
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[205]?>:</label>
									<div class="col-lg-8">
									<label class="field select">
										<select id="form-field-2"  name="point[]" id="point">
											<option value="0"> Select Number</option>
											<?php for($num=0; $num<=10; $num++){ ?>
											<option value="<?php echo $num?>"> <?php echo $num?></option>
											<?php }?>

											</select>
										<i class="arrow double"></i>
									</label>
									<input type="hidden" name="IDwheelitens[]" id="IDwheelitens" value="<?php echo $question_list[$i]->IDwheelitens?>" />
									</div>
									</div>
									<?php }?>
								</div>	
							</div>
							
							
							<?php }}?>
							
							
							<div class="col-md-12" align="center">
								<?php if($action != 'view'){ ?>
								<button type="Submit" class="btn active btn-success save_answer" name="wheel" id="btnSubmit" onclick="return confirm('Are you sure want to Save');">
									<i class="fa fa-save"></i> <?=$lang[56]?>
								</button>
								<?php }?>
								<a href="<?=$this->config->base_url();?>index.php/Testing/">
								<button class="btn active btn-warning" type="button">
									<i class="fa fa-warning"></i> <?=$lang[422]?>
								</button></a>
							</div>
							
                        </div>
                    </div>
                </div><!-- end col-md-12 -->
<!---==========  Documents List ==========================------------------------------------------->			
				

			</form>      
        </div>
	</div>
            <!-- End: Content -->  
</section>
    <!-- End: Content-Wrapper -->
<script>
$(".new_add").click(function(){
var total="<?=($credit[0]->Credits-$total_coach[0]->total)?>";
if(total<1 ){
alert("Please Buy Credits First");
return false;

}else{
return true;
}
})
</script>