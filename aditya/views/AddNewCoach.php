<?php include 'sidemenu.php'; ?>
    <!-- End: sidemenu -->
   
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper">
	<!-- Start: Topbar -->
	<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					<a href="#"> Coachee Records </a>
				</li>
			</ol>
		</div>
	</header>
	<!-- End: Topbar -->

    <!-- Begin: Content -->
    <div id="content" class="animated fadeIn">
        <div class="row">
			<form class="form-horizontal" action="" method="POST">
				<?php if(isset($_GET['msg'])){?>
				<div class="col-md-12">
				<div class="alert alert-system dark alert-dismissable">
				<button class="close" type="button" data-dismiss="alert" aria-hidden="true">x</button>
				<i class="fa fa-check pr10"></i>
				<strong> <?=$_GET['msg']?> </strong>
				</div>
				</div>
				<?php }?>
				<div class="col-md-4"> <br/>
					<div class="fileupload fileupload-new admin-form" data-provides="fileupload">
						<div class="fileupload-preview thumbnail mb20">
							<img src="<?=$this->config->base_url();?>coach_images/<?=$get_data_coachee[0]->Photo?>"  alt="holder">
						</div>
						<div class="row">
							<div class="col-xs-5">
								<span class="button btn-system">
									<a href="<?php echo $this->config->base_url(); ?>index.php/coachee/index/<?=$get_data_coachee[0]->IDCoachee ?>"> <span class="fileupload-new" style="color:#626262;"><b><?=$lang[29]?> </b> </span></a>
								</span>
							</div>
						</div>
					</div>
				</div>
				
				
                <div class="col-md-4">
					<div class="panel panel-primary panel-border top mt20 mb35">
                        <div class="panel-body bg-light dark">
                            <div class="admin-form">
                               
                                <div class="section row mb12">
                                    <div class="col-md-12">
                                        <label for="account-name" class="field prepend-icon">
                                            <input  class="gui-input" value="<?= $get_data_coachee[0]->Name ?>" type="text" placeholder="Name" readonly>
                                            <label for="account-name" class="field-icon"><i class="fa fa-user"></i>
                                            </label>
                                        </label>
                                    </div>
                                </div>
								
								<div class="section row mb12">
                                   <div class="col-md-12">
                                        <label class="field prepend-icon">
                                            <input type="text" value="<?= $get_data_coachee[0]->PhoneCommercial ?>" placeholder="Phone" readonly class="gui-input" value="">
                                            <label class="field-icon" class="field-icon"><i class="fa fa-mobile"></i>
                                            </label>
                                        </label>
                                    </div>
                                </div>
								
								<div class="section row mb12">
                                    <div class="col-md-12">
                                        <label class="field prepend-icon">
                                            <input class="gui-input"  type="text" value="<?= $get_data_coachee[0]->Email1 ?>" placeholder="Email" readonly>
                                            <label  class="field-icon"><i class="fa fa-envelope-o"></i>
                                            </label>
                                        </label>
                                    </div>
                                </div>
								
								<div class="section row mb12">
                                    <div class="col-md-12">
                                        <label class="field prepend-icon">
                                            <input type="text" class="gui-input" type="text" value="<?=($get_data_coachee[0]->Birth)?>" placeholder="Birth" readonly>
                                            <label  class="field-icon"><i class="fa fa-calendar"></i>
                                            </label>
                                        </label>
                                    </div>
                                </div>
								
								<div class="section row mb12">
                                    <div class="col-md-12">
                                        <label class="field prepend-icon">
                                            <input class="gui-input" type="text" value="<?=($get_data_coachee[0]->DataFimCoaching)?>" placeholder="Data Fim Coaching" readonly>
                                            <label  class="field-icon"><i class="fa fa-align-justify"></i>
                                            </label>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!--close col-sm-4-->
				
				<div class="col-md-4">
					<div class="panel panel-primary panel-border top mt20 mb35">
                        <div class="panel-body bg-light dark">
                            <div class="admin-form">
							
                                <div class="section row mb12">
                                   <div class="col-md-12">
                                        <label class="field prepend-icon">
                                            <input class="gui-input" type="text" value="<?= $get_data_coachee[0]->Address ?>" placeholder=" Address" readonly>
                                            <label class="field-icon"><i class="fa fa-map-marker"></i>
                                            </label>
                                        </label>
                                    </div>
                                </div>
								
                                <div class="section row mb12">
                                   <div class="col-md-12">
                                        <label class="field prepend-icon">
                                            <input class="gui-input" type="text" value="<?= $get_data_coachee[0]->Mobile ?>" placeholder="Mobile" readonly>
                                            <label class="field-icon"><i class="fa fa-phone"></i>
                                            </label>
                                        </label>
                                    </div>
                                </div>
								
                                <div class="section row mb12">
                                   <div class="col-md-12">
                                        <label class="field prepend-icon">
                                            <input class="gui-input" type="text" value="<?= $get_data_coachee[0]->name ?>" placeholder="Civil State" readonly>
                                            <label class="field-icon"><i class="fa fa-stack-exchange"></i>
                                            </label>
                                        </label>
                                    </div>
                                </div>
								
								<div class="section row mb12">
                                   <div class="col-md-12">
                                        <label class="field prepend-icon">
                                            <input  class="gui-input" type="text" value="<?=($get_data_coachee[0]->DataInicioCoaching)?>" placeholder="DataInicioCoaching" readonly>
                                            <label class="field-icon"><i class="fa fa-instagram"></i>
                                            </label>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!--close col-sm-4-->
				
				<div class="col-md-12"> <!--close col-sm-12--- Goals---->
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> <?php echo $lang[30]?>
							</span>
							
							<a href="<?=$this->config->base_url()?>index.php/AddNewCoach/Goal/<?=$id?>"> <span>
							<button style="padding:9px; margin-right:-7px;" class="btn btn-success btn-sm light fw600 ml10 pull-right" type="button">
							<i class="fa fa-plus"></i>
							<?php echo $lang[40]?>
							</button> </span> </a>
							
                        </div>
                        <div class="panel-body pn full-width">
                            <div class="table-responsive" style="height:160px; overflow:auto; width:100%;">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th style="width:30%;"><?=$lang[38]?> </th>
											<th style="width:60%;"><?=$lang[33]?> </th>
											<th style="width:10%;"><?=$lang[39]?> </th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php
										if ($list_Goals > 0) {
										for ($r = 0; $r < count($list_Goals); $r++) {
										?>	
										<tr>
										<td><?php echo $list_Goals[$r]->IDgoal ?></td>
										<td><?php echo $list_Goals[$r]->Description ?></td>
										<td>
										<a class="btn btn-success btn-xs" href="<?=$this->config->base_url()?>index.php/AddNewCoach/Goal/<?=$id?>/<?php echo $list_Goals[$r]->IDgoal ?>">
										<i class="fa fa-edit"></i>
										<?=$lang[39]?>
										</a>
										</td>		
										</tr>
										<?php }} ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- end col-md-12 -->
				
				
				<div class="col-md-12"> <!--close col-sm-12--- Session---->
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> <?php echo $lang[31]?>
							</span>
							
							<a href="<?=$this->config->base_url()?>index.php/AddNewCoach/session/<?=$id?>"> <span>
							<button style="padding:9px; margin-right:-7px;" class="btn btn-success btn-sm light fw600 ml10 pull-right" type="button">
							<i class="fa fa-plus"></i>
							<?php echo $lang[41]?>
							</button> </span> </a>
							
                        </div>
                        <div class="panel-body pn full-width">
                            <div class="table-responsive" style="height:160px; overflow:auto; width:100%;">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th style="width:30%;"><?=$lang[38]?></th>
											<th style="width:30%;"><?=$lang[34]?></th>
											<th style="width:30%;"><?=$lang[35]?></th>
											<th style="width:10%;"><?=$lang[39]?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php
										if ( $list_Sessions[0]->IDSessoes > 0) {
										for ($r = 0; $r < count($list_Sessions); $r++) {
										?>	
										<tr>
										<td><?php echo $list_Sessions[$r]->IDSessoes ?></td>
										<td><?=($list_Sessions[$r]->Date)?></td>
										<td><?php echo $list_Sessions[$r]->Notes ?></td>
										<td>
										<a class="btn btn-success btn-xs" href="<?php echo $this->config->base_url()?>index.php/AddNewCoach/session/<?=$id?>/<?php echo $list_Sessions[$r]->IDSessoes?>">
										<i class="fa fa-edit"></i>
										<?php echo $lang[39]?>
										</a>
										</td>
										</tr>
										<?php }} ?>		
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- end col-md-12 -->
				
				<div class="col-md-12"> <!--close col-sm-12--- Activity---->
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> <?=$lang[32]?>
							</span>
							
							<a href="<?=$this->config->base_url()?>index.php/AddNewCoach/Activity/<?=$id?>"> <span>
							<button style="padding:9px; margin-right:-7px;" class="btn btn-success btn-sm light fw600 ml10 pull-right" type="button">
							<i class="fa fa-plus"></i>
							<?php echo $lang[42]?>
							</button> </span> </a>
							
                        </div>
                        <div class="panel-body pn full-width">
                            <div class="table-responsive" style="height:260px; overflow:auto; width:100%;">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th style="width:30%;"><?=$lang[38]?> </th>
											<th style="width:30%;"><?=$lang[36]?> </th>
											<th style="width:30%;"><?=$lang[33]?> </th>
											<th style="width:10%;"><?=$lang[39]?> </th>
                                        </tr>
                                    </thead>
                                    <tbody>
											<?php
											if ($list_Activities > 0) {
											for ($r = 0; $r < count($list_Activities); $r++) {
											?>	
											<tr>
											<td><?php echo $list_Activities[$r]->IDAtividade ?></td>
											<td><?=($list_Activities[$r]->StartDate)?></td>
											<td><?php echo $list_Activities[$r]->Description ?></td>
											<td>
											<a class="btn btn-success btn-xs" href="<?=$this->config->base_url()?>index.php/AddNewCoach/Activity/<?=$id?>/<?php echo $list_Activities[$r]->IDAtividade ?>">
											<i class="fa fa-edit"></i>
											<?php echo $lang[39]?>
											</a>
											</td>

											</tr>
											<?php }} ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- end col-md-12 -->
				
				<div class="col-md-12"> <!--close col-sm-12--- Test---->
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> <?=$lang[2]?>
							</span>
                        </div>
                        <div class="panel-body pn full-width">
                            <div class="table-responsive" style="height:260px; overflow:auto; width:100%;">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th style="width:30%;"><?=$lang[44]?></th>
											<th style="width:30%;"><?=$lang[33]?></th>
											<th style="width:30%;"><?=$lang[45]?></th>
											<th style="width:10%;"><?=$lang[102]?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php
										$id_test_answer;
										for($r=0;$r<count($test_answer);$r++){
											$id_test_answer[]=$test_answer[$r]->IDAvaliacao;
										}
								
										for($r=0;$r<count($test_list);$r++){
										if(in_array($test_list[$r]->IDAvaliacao, $id_test_answer)){
										?>
										<tr>
										<td> <?=$test_list[$r]->NomeAvaliacao?> </td>
										<td> <?=$test_list[$r]->Descfricao?></td>
										<td> <?=$test_list[$r]->Objectives?> </td>
										<td>
                                        <a class="btn btn-success btn-xs" href="<?php echo $this->config->base_url()?>index.php/AddNewCoach/testing/<?=$test_list[$r]->IDAvaliacao?>/view/<?=$coachee_id?>">
											<i class="fa fa-eye"></i>
											<?php echo $lang[102]?>
											</a> 
                                        </td>  </tr>
										<?php }} ?>
										
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- end col-md-12 -->
				
				<div class="col-md-12"> <!--close col-sm-12--- Save Logbook---->
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> <?php echo $lang[127];?>
							</span>
                        </div>
                        <div class="panel-body pn full-width">
                            <div class="table-responsive" style="height:160px; overflow:auto; width:100%;">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
											<th style="width:30%;"><?php echo $lang[126];?></th>
											<th style="width:30%;"><?php echo $lang[87];?></th>
											<th style="width:30%;"><?php echo $lang[129];?></th>
											<th style="width:10%;"> <?php echo $lang[102];?> </th>
										</tr>
                                    </thead>
                                    <tbody>
										<?php echo $result;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- end col-md-12 -->
				
				<div class="col-md-12"> <!--close col-sm-12--- Save Logbook---->
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> Notepad
							</span>
                        </div>
						
						<div class="panel-body pn">
							<div class="col-md-6">
								<div class="admin-form">
								   
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard">&nbsp;</label>
									<div class="col-lg-8">
									&nbsp;
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-8 control-label" for="inputStandard">
									<textarea class="gui-textarea" placeholder="Store Description and Purposes" name="notepad_data"> <?php echo $notepad[0]->notepad;?> </textarea>
									</label>
									<div class="col-lg-4">
									&nbsp;
									</div>
									</div>
									
									<div class="form-group">
									<div class="col-lg-12">
									<button class="btn active btn-success" type="submit" name="notepad"> <i class="fa fa-save"></i> 
									<?=$lang[105]?>
									</button>
									</div>
									</div>
								</div>
							</div> <!--close col-sm-6-->
							
						</div>
                    </div>
                </div><!-- end col-md-12 -->
<!--*******************************##################  Tool Start  #################*****************************------->
<script language="javascript">
   function get_whell_test(type)
	{
		var coachee_id = '<?php echo $this->uri->segment(3);?>';
		var url = '<?php echo base_url(); ?>'+'index.php/AddNewCoach/index/'+coachee_id+'/'+type;
		document.location = url; 
	}
 
</script>

<script language="javascript">
$(document).ready(function(){
	  
     $(".status").click(function(e){ 
	 var con = confirm("Are you sure you want to change status?");
	   if(con)
	   { 
			var st_id = $(this).attr('id');
			var arr_st_id = st_id.split("_") ;
			var IDtool = arr_st_id[1] ;
			var Type = $('#tool').val();
			var IDCoach = '<?php echo $_SESSION['coach']?>';
			var IDCoachee ='<?php echo $this->uri->segment(3);?>';
			//alert(IDCoach);
			e.preventDefault();
		    data = { IDtool : IDtool,Type : Type,IDCoach : IDCoach,IDCoachee : IDCoachee};
	       $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>index.php/AddNewCoach/changestatus",
            type: 'POST',
            data: data, 

            success:function(data)
            {
				if(data==1)
				{
					data='<a href="javascript:"><img src="<?php echo base_url(); ?>assets/images/tick.png" alt="Active" title="Active" /></a>';
				}
				else 
				{
					data='<a href="javascript:"><img src="<?php echo base_url(); ?>assets/images/cross.png" alt="Inactive" title="Inactive" /></a>';
				}
               $('#status_'+IDtool).html(data);
            }
        });
	  }
	  else
	  {
		  return false;
	  }
    });

});
</script>	
				
				<div class="col-md-12"> <!--close col-sm-12--- Tool---->
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> <?=$lang[204]?>
							</span>
                        </div>
						<div class="col-md-12">
						<div class="admin-form">
							<div class="form-group"><br/>
								<div class="col-lg-4">
								<label class="field select">
									<select Name="tool" id="tool" onchange="get_whell_test(this.value);">
										<option value="1" <?php if($this->uri->segment(4)== 1){?>selected="selected" <?php }?>> <?=$lang[202]?> </option>
										<option value="2" <?php if($this->uri->segment(4)== 2){?>selected="selected" <?php }?>> <?=$lang[18]?> </option>
										<option value="3" <?php if($this->uri->segment(4)== 3){?>selected="selected" <?php }?>> <?=$lang[305]?> </option>
									</select>
									<i class="arrow double"></i>
								</label>
								</div>
							</div>
						</div>
						</div>
                        <div class="panel-body pn">
							<div class="row-fluid">	
		<div class="span12">
			<div class="control-group" id="changetext">
			<?php if($type == 3){?>
            <?php //echo '<pre>'; print_r($list_video_Arr);?>
			<table id="sample-table-1" class="table table-striped table-bordered table-hover">
			<thead><tr><th style="width:30%;"><?=$lang[302]?></th><th style="width:30%;"><?=$lang[33]?></th><th style="width:30%;"><?=$lang[303]?></th><th style="width:10%;"><?=$lang[25]?></th></tr></thead>
			<tbody>
           <?php if(count($list_video_Arr) > 0) { 
            for($i=0; $i < count($list_video_Arr);$i++) { ?>
			<tr>
           <td><?php echo $list_video_Arr[$i]['Title']?></td>
           <td><?php echo $list_video_Arr[$i]['Description']?></td>
           <td><?php echo $list_video_Arr[$i]['Purpose']?></td>
		     <?php if ($list_video_Arr[$i]['Enabled'] == '1'  ){ ?>
            <td id="status_<?php echo $list_video_Arr[$i]['IDvideo'];?>" class="status"><a href="javascript:">
            <img src="<?php echo base_url(); ?>assets/images/tick.png" alt="Inactive" title="Inactive"/></a></td>
            <?php } elseif($list_video_Arr[$i]['Enabled'] == '0') {?>
            <td id="status_<?php echo $list_video_Arr[$i]['IDvideo'];?>" class="status"><a href="javascript:">
            <img src="<?php echo base_url(); ?>assets/images/cross.png" alt="Inactive" title="Inactive"/></a></td>
             <?php } else { ?>
             <td id="status_<?php echo $list_video_Arr[$i]['IDvideo'];?>" class="status"><a href="javascript:">
             <img src="<?php echo base_url(); ?>assets/images/tick.png" alt="Inactive" title="Inactive"/></a></td>
            <?php } ?>
			</tr>
            <?php } } ?>
     				
			</tbody></table>
            <?php } elseif($type == 2){?>
            <?php //echo '<pre>'; print_r($list_test_Arr);?>
			<table id="sample-table-1" class="table table-striped table-bordered table-hover">
			<thead><tr><th style="width:30%;"><?=$lang[85]?></th><th style="width:30%;"><?=$lang[33]?></th><th style="width:30%;"><?=$lang[45]?></th><th style="width:10%;"><?=$lang[25]?></th></tr></thead>
			<tbody>
           <?php if(count($list_test_Arr) > 0) { 
            for($i=0; $i < count($list_test_Arr);$i++) { ?>
			<tr>
           <td><?php echo $list_test_Arr[$i]['NomeAvaliacao']?></td>
           <td><?php echo $list_test_Arr[$i]['Descfricao']?></td>
           <td><?php echo $list_test_Arr[$i]['Objectives']?></td>
		     <?php if ($list_test_Arr[$i]['Enabled'] == '1'  ){ ?>
            <td id="status_<?php echo $list_test_Arr[$i]['IDAvaliacao'];?>" class="status"><a href="javascript:">
            <img src="<?php echo base_url(); ?>assets/images/tick.png" alt="Inactive" title="Inactive"/></a></td>
            <?php } elseif($list_test_Arr[$i]['Enabled'] == '0') {?>
            <td id="status_<?php echo $list_test_Arr[$i]['IDAvaliacao'];?>" class="status"><a href="javascript:">
            <img src="<?php echo base_url(); ?>assets/images/cross.png" alt="Inactive" title="Inactive"/></a></td>
             <?php } else { ?>
             <td id="status_<?php echo $list_test_Arr[$i]['IDAvaliacao'];?>" class="status"><a href="javascript:">
             <img src="<?php echo base_url(); ?>assets/images/tick.png" alt="Inactive" title="Inactive"/></a></td>
            <?php } ?>
			</tr>
            <?php } } ?>
     				
			</tbody></table>
       	 
             <?php } else {?>
               <table id="sample-table-1" class="table table-striped table-bordered table-hover">
				<thead>
					<tr>
						<th style="width:30%;"><?=$lang[58]?></th>
						<th style="width:60%;" ><?=$lang[33]?></th>
                       	<th style="width:10%;"><?=$lang[25]?></th>
					
					</tr>
				</thead>
				<tbody>
                <?php //echo '<pre>'; print_r($whell_list_Arr); ?>
                 <?php if(count($whell_list_Arr) > 0) {?>
                 <?php for($i=0; $i < count($whell_list_Arr);$i++) {?>
				<tr>
                <td><?php echo $whell_list_Arr[$i]['Name'] ?></td>
                 <td><?php echo $whell_list_Arr[$i]['Description'] ?></td>
                  <?php if($whell_list_Arr[$i]['Enabled'] == '1'  ){ ?>
                  <td id="status_<?php echo $whell_list_Arr[$i]['IDwheel'];?>" class="status"><a href="javascript:void(0);">
                  <img src="<?php echo base_url(); ?>assets/images/tick.png" alt="Active" title="Active"/></a></td>
                 <?php } elseif($whell_list_Arr[$i]['Enabled'] == '0') {?>
                  <td id="status_<?php echo $whell_list_Arr[$i]['IDwheel'];?>" class="status"><a href="javascript:void(0);">
                  <img src="<?php echo base_url(); ?>assets/images/cross.png" alt="Inactive" title="Inactive"/></a></td>
                   <?php } else { ?>
                    <td id="status_<?php echo $whell_list_Arr[$i]['IDwheel'];?>" class="status"><a href="javascript:void(0);">
                    <img src="<?php echo base_url(); ?>assets/images/tick.png" alt="Active" title="Active"/></a></td>
                    <?php }?>
				  </tr>	
                  <?php } }?>
           		
				</tbody>
			</table>
             <?php }?>
			</div>
		</div>
		
	</div>
						
                        </div>
                    </div>
                </div><!-- end col-md-12 -->
					
			</form>      
        </div>
	</div>
            <!-- End: Content -->  
</section>
    <!-- End: Content-Wrapper -->



