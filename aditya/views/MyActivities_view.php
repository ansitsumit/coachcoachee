<?php include ('sidemenu.php');?>
    <!-- End: sidemenu -->
   
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper"
	<!-- Start: Topbar -->
	<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					<a href="dashboard.html">My Activities</a>
				</li>
			</ol>
		</div>
	</header>
	<!-- End: Topbar -->

    <!-- Begin: Content -->
    <div id="content" class="animated fadeIn">
        <div class="row">
			<form class="form-horizontal" role="form">
              
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span><?=$lang[433]?> 
							</span>
                        </div>
                        <div class="panel-body pn">
                            <div class="table-responsive" style="height:400px; overflow:auto;">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th style="width:30%;"><?=$lang[36]?></th>
											<th style="width:30%;"><?=$lang[24]?></th>
											<th style="width:30%;"><?=$lang[33]?></th>
											<th style="width:10%;"><?=$lang[25]?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php
										for($r=0;$r<count($total_activities);$r++){
										if($total_activities[$r]->IDAtividade>0){
									
										?><tr>
											<td><?=date("d/m/Y",strtotime($total_activities[$r]->StartDate))?></td>
												<td><?php echo $total_activities[$r]->Activity; ?></td>
										   <td><?php echo $total_activities[$r]->Description; ?></td>
										   <td><a class="btn btn-system btn-xs" href="<?=$this->config->base_url()?>index.php/MyActivities/index/<?php echo $total_activities[$r]->IDAtividade; ?>/0"> <i class="fa fa-eye"></i> <?=$lang[25]?>  </a></td>
										   
										</tr>
										<?php } } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- end col-md-12 -->
				
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span><?=$lang[445]?> 
							</span>
                        </div>
                        <div class="panel-body pn">
                            <div class="table-responsive" style="height:400px; overflow:auto;">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th style="width:30%;"><?=$lang[36]?></th>
											<th style="width:30%;"><?=$lang[24]?></th>
											<th style="width:30%;"><?=$lang[69]?></th>
											<th style="width:10%;"><?=$lang[25]?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php
										for($r=0;$r<count($done_activities);$r++){
										if($done_activities[$r]->IDAtividade>0){
										if($done_activities[$r]->DataEntrega!="0000-00-00"){
											?><tr>
												<td><?=($done_activities[$r]->StartDate)?></td>
												<td><?php echo $done_activities[$r]->Activity; ?></td>
												<td>200</td>
												<td><a class="btn btn-system btn-xs" href="<?=$this->config->base_url()?>index.php/MyActivities/index/<?php echo $done_activities[$r]->IDAtividade; ?>/200"> <i class="fa fa-eye"></i> <?=$lang[25]?>  </a></td>
												
												 
										 
											</tr>
										<?php }} }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- end col-md-12 -->
				
				
					
			</form>      
        </div>
	</div>
            <!-- End: Content -->  
</section>
    <!-- End: Content-Wrapper -->




