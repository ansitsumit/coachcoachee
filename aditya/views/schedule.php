<?php include 'sidemenu.php'; ?>
    <!-- End: sidemenu -->
   
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper"
	<!-- Start: Topbar -->
	<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					<a href="#"> Schedule </a>
				</li>
			</ol>
		</div>
	</header>
	<!-- End: Topbar -->

    <!-- Begin: Content -->
    <div id="content" class="animated fadeIn">
        <div class="row">
			<form class="form-horizontal" role="form">

<!---========== Condition Calender Schedule ==========================------------------------------------------->
			<?php if($total_row <1){ ?>
<!---==========  Schedule  ==========================------------------------------------------->			
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> Schedule
							</span>
                        </div>
                        <div class="panel-body pn">
                            <div class="table-responsive" style="height:600px; overflow:auto;">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th >Coachee name</th>
											<th >Time</th>
											<th >Date</th>
											<th >complete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
										
										<tr> <td colspan="4" style="text-align: center">No Record(s) found </td> </tr>
									
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- end col-md-12 -->
				<?php } if($caldata[0]->is_google_calander=='0'){ ?>
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> Schedule
							</span>
                        </div>
                        <div class="panel-body pn">
                            <div class="table-responsive" style="height:600px; overflow:auto;">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th >Coachee name</th>
											<th >Time</th>
											<th >Date</th>
											<th >complete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php foreach ($coachData->result() as $row){ ?>
										<tr>
											<td><?echo $row->Name?></td>
											<td><?echo $row->Time?> </td>
											<td><?echo $row->Date?></td>
											<td><?echo $row->complete?></td>
										</tr>
										<?php }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- end col-md-12 -->
				<div class="col-md-12" >
				<?php } else echo $code[0]->code; ?>
				<br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/>
				</div>
			</form>      
        </div>
	</div>
            <!-- End: Content -->  
</section>
    <!-- End: Content-Wrapper -->

