<?php include 'sidemenu.php'; ?>
    <!-- End: sidemenu -->
   
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper">
	<!-- Start: Topbar -->
	<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					<a href="#"> Toolbox </a>
				</li>
			</ol>
		</div>
	</header>
	<!-- End: Topbar -->

    <!-- Begin: Content -->
    <div id="content" class="animated fadeIn">
        <div class="row">
			<form class="form-horizontal" role="form">
				<?php if($_GET['msg']){?>
				<div class="col-md-12">
				<div class="alert alert-system dark alert-dismissable">
				<button class="close" type="button" data-dismiss="alert" aria-hidden="true">x</button>
				<i class="fa fa-check pr10"></i>
				<strong> <?=$_GET['msg']?> </strong>
				</div>
				</div>
				<?php }?>
				<?php if($this->session->flashdata('msg')){?>
				<div class="col-md-12">
				<div class="alert alert-system dark alert-dismissable">
				<button class="close" type="button" data-dismiss="alert" aria-hidden="true">x</button>
				<i class="fa fa-check pr10"></i>
				<strong> <?php echo $this->session->flashdata('msg');?> </strong>
				</div>
				</div>
				<?php } ?>
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
						<div class="row">
						<div class="col-sm-6">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> <?=$lang[201]?>
							</span>
							</div>
							<div class="col-sm-6 tool-button">
							<a href="<?=$this->config->base_url();?>index.php/home/index/add_test"> <span>
							<button style="padding:9px; margin-right:-7px;" class="btn btn-success btn-sm light fw600 ml10 pull-right" type="button">
							<i class="fa fa-plus"></i>
							<?=$lang[86]?>
							</button> </span> </a>
							</div>
							</div>
                        </div>
                        <div class="panel-body pn full-width">
                            <div class="table-responsive" style="height:460px; overflow:auto; width:100%;">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th style="width:30%;"><?=$lang[85]?></th>
											<th style="width:30%;"><?=$lang[33]?></th>
											<th style="width:30%;"><?=$lang[45]?></th>
											<th style="width:10%;"><?=$lang[39]?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php
 
										for($r=0;$r<count($list_test);$r++){

										if($list_test[$r]->IDAvaliacao>0){
										?>	<tr>
										<td><?=$list_test[$r]->NomeAvaliacao?></td>
										<td><?=$list_test[$r]->Descfricao?></td>
										<td><?=$list_test[$r]->Objectives?></td>
										
										<td>
											<a class="btn btn-success btn-xs" href="<?=$this->config->base_url();?>index.php/home/index/add_test/<?=$list_test[$r]->IDAvaliacao?>">
											<i class="fa fa-edit"></i> <?=$lang[39]?>
											</a>
										</td>
												
										</tr>
										<?php }}?>
									
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- end col-md-12 -->
<!---========== Wheels ==========================------------------------------------------->
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> <?=$lang[202]?>
							</span>
							<a href="<?=$this->config->base_url();?>index.php/toolbox/wheel" role="button"> <span>
							<button style="padding:9px; margin-right:-7px;" class="btn btn-success btn-sm fw600 ml10 pull-right green Sessions_id" type="button">
							<i class="fa fa-plus"></i>
							<?=$lang[203]?>
							</button></span>
							</a>
                        </div>
                        <div class="panel-body pn full-width">
                            <div class="table-responsive" style="height:300px; overflow:auto; width:100%;">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th style="width:30%;"><?=$lang[58]?></th>
											<th style="width:60%;"><?=$lang[33]?></th>
											<th style="width:10%;"><?=$lang[39]?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
									
										<?php
 
										for($r=0;$r<count($list_wheel);$r++){

										if($list_wheel[$r]->IDwheel>0){
										?>	<tr>
										<td style="width:15rem;"><?=$list_wheel[$r]->Name?></td>
										<td style="width:45rem;"><?=$list_wheel[$r]->Description?></td>
										
										<td>
											<a class="btn btn-success btn-xs" href="<?php echo base_url();?>index.php/toolbox/edit_wheel/<?php echo $list_wheel[$r]->IDwheel?>">
											<i class="fa fa-edit"></i> <?php echo $lang[39]?>
											</a>
										</td>
												
										</tr>
										<?php }}?>
									
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- end col-md-12 -->
				
<!---========== video ==========================------------------------------------------->
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> <?=$lang[300]?>
							</span>
							<a href="<?=$this->config->base_url();?>index.php/toolbox/videos_and_movies"  role="button"> <span>
							<button style="padding:9px; margin-right:-7px;" class="btn btn-success btn-sm fw600 ml10 pull-right green Sessions_id" type="button">
							<i class="fa fa-plus"></i>
							<?=$lang[304]?>
							</button></span>
							</a>
                        </div>
                        <div class="panel-body pn full-width">
                            <div class="table-responsive" style="height:300px; overflow:auto; width:100%;">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th style="width:30%;"><?=$lang[302]?></th>
											<th style="width:30%;"><?=$lang[301]?></th>
											<th style="width:30%;"><?=$lang[33]?></th>
											<th style="width:10%;"><?=$lang[39]?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
										
										<?php
 
										for($r=0;$r<count($list_video);$r++){

										if($list_video[$r]->IDvideo>0){
										?>	<tr>
										<td><?=$list_video[$r]->Title?></td>
										<td><?=$list_video[$r]->Link?></td>
										<td><?=$list_video[$r]->Description?></td>
										
										<td>
											<a class="btn btn-success btn-xs" href="<?php echo base_url();?>index.php/toolbox/videos_and_movies/<?php echo $list_video[$r]->IDvideo?>">
											<i class="fa fa-edit"></i> <?php echo $lang[39]?>
											</a>
										</td>
												
										</tr>
										<?php }}?>
									
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- end col-md-12 -->
				
<!---========== LogBook ==========================------------------------------------------->
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> <?=$lang[125]?>
							</span>
							<a href="<?=$this->config->base_url()?>index.php/log_book"  role="button"> <span>
							<button style="padding:9px; margin-right:-7px;" class="btn btn-success btn-sm fw600 ml10 pull-right green Sessions_id" type="button">
							<i class="fa fa-plus"></i>
							<?=$lang[130]?>
							</button></span>
							</a>
                        </div>
                    </div>
                </div><!-- end col-md-12 -->				
					
			</form>      
        </div>
	</div>
            <!-- End: Content -->  
</section>
    <!-- End: Content-Wrapper -->

