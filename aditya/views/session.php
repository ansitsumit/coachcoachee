<?php include 'sidemenu.php';?>
    <!-- End: sidemenu -->
   
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper"
	<!-- Start: Topbar -->
	<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					<a href="#"> Add Session </a>
				</li>
			</ol>
		</div>
	</header>
	<!-- End: Topbar -->

    <!-- Begin: Content -->
    <div id="content" class="animated fadeIn">
        <div class="row">
			<form class="form-horizontal" action="" method="post">

<!---========== Add test ==========================------------------------------------------->
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> Add Session
							</span>
                        </div>
                        <div class="panel-body pn">
							<div class="col-md-12"> &nbsp; </div>
							<div class="col-md-6">
								<div class="admin-form">
									<input class="form-field-6" type="hidden" name="IDSessoes" value="<?=$goal_data[0]->IDSessoes?>" required="required">
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[34]?> :</label>
									<div class="col-lg-8">
									<input  class="form-control" type="text" value="<?=$goal_data[0]->Date?>" name="Date" required="required" placeholder="yyyy-mm-dd">
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[49]?>:</label>
									<div class="col-lg-8">
									<input  class="form-control" placeholder="HH:MM" type="text" value="<?=$goal_data[0]->Time?>" name="Time">
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[27]?> : </label>
									<div class="col-lg-8">
									<label class="field select">
										<select name="complete">
											<option value="" <? if($goal_data[0]->complete==""){echo "selected";}?>></option>
											<option value="No" <? if($goal_data[0]->complete=="No"){echo "selected";}?> >No</option>
											<option value="Yes" <? if($goal_data[0]->complete=="Yes"){echo "selected";}?>>Yes</option>
										</select>
										<i class="arrow double"></i>
									</label>
									</div>
									</div>
									
							
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[48]?> : </label>
									<div class="col-lg-8">
									<textarea name="Notes" style="width: 344px; height: 115px;"> 
									<?=$goal_data[0]->Notes?>
									</textarea>
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> &nbsp; </label>
									<? if($button==""){?>
									<div class="col-lg-4">
									<button class="btn active btn-success btn-block" type="submit" name="sessions">
									<i class="fa fa-save"></i>  <?=$lang[56]?> </button>
									</div>
									<div class="col-lg-4">
									<a href="<?=$this->config->base_url()?><?=$cancel?>" class="btn active btn-warning btn-block">
									<i class="fa fa-warning"></i>
									<?php echo $lang[422]?>
									</a>
									</div>
									<?php }else{?>
									<div class="col-lg-4">
									<a href="<?=$this->config->base_url()?><?=$cancel?>" class="btn active btn-warning btn-block">
									<i class="fa fa-warning"></i>
									<?php echo $lang[422]?>
									</a>
									</div>
									<?php }?>
									</div>	
									
								</div>
								
								</div>	
							</div>
							
							
                </div><!-- end col-md-12 -->		
			</form>      
        </div>
	</div>
            <!-- End: Content -->  
</section>
    <!-- End: Content-Wrapper -->

