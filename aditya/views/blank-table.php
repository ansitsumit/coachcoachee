<?php include 'sidemenu.php'; ?>
    <!-- End: sidemenu -->
   
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper"
	<!-- Start: Topbar -->
	<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					<a href="dashboard.html">Manager</a>
				</li>
			</ol>
		</div>
		<div class="topbar-right">    
			<button class="btn btn-success btn-sm light fw600 ml10" type="submit"> <i class="fa fa-plus"></i> Add New</button>
			<button class="btn btn-success btn-sm light fw600 ml10" type="submit"> <i class="fa fa-upload"></i>  Import</button>
			<button class="btn btn-success btn-sm light fw600 ml10" type="submit"> <i class="fa fa-download"></i> Export</button>
		</div>
	   
	</header>
	<!-- End: Topbar -->

    <!-- Begin: Content -->
    <div id="content" class="animated fadeIn">
        <div class="row">
			<form class="form-horizontal" role="form">

                <div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-heading">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span>Responsive Table
							</span>
                        </div>
                        <div class="panel-body pn">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover">
                                    <thead>
                                        <tr class="system">
                                            <th>#</th>
                                            <th>Table heading</th>
                                            <th>Table heading</th>
                                            <th>Table heading</th>
                                            <th>Table heading</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Table cell</td>
                                            <td>Table cell</td>
                                            <td>Table cell</td>
                                            <td>Table cell</td>
                                            <td style="width:250px;">
												<a class="btn btn-success btn-xs purple" href="">
													<i class="fa fa-pencil"></i> Edit
												</a>
												
												<a class="btn btn-success btn-xs" href="">
													<i class="fa fa-eye"></i> View
												</a>
												
												<a class="btn btn-success btn-xs" href="">
													<i class="fa fa-print"></i> Print
												</a>
												
												<a class="btn btn-danger btn-xs" href="">
													<i class="fa fa-trash-o"></i> Delete
												</a>
											</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Table cell</td>
                                            <td>Table cell</td>
                                            <td>Table cell</td>
                                            <td>Table cell</td>
                                            <td style="width:250px;">
												<a class="btn btn-success btn-xs purple" href="">
													<i class="fa fa-pencil"></i> Edit
												</a>
												
												<a class="btn btn-success btn-xs" href="">
													<i class="fa fa-eye"></i> View
												</a>
												
												<a class="btn btn-success btn-xs" href="">
													<i class="fa fa-print"></i> Print
												</a>
												
												<a class="btn btn-danger btn-xs" href="">
													<i class="fa fa-trash-o"></i> Delete
												</a>
											</td>
                                        </tr>
                                       <tr>
                                            <td>1</td>
                                            <td>Table cell</td>
                                            <td>Table cell</td>
                                            <td>Table cell</td>
                                            <td>Table cell</td>
                                            <td style="width:250px;">
												<a class="btn btn-success btn-xs purple" href="">
													<i class="fa fa-pencil"></i> Edit
												</a>
												
												<a class="btn btn-success btn-xs" href="">
													<i class="fa fa-eye"></i> View
												</a>
												
												<a class="btn btn-success btn-xs" href="">
													<i class="fa fa-print"></i> Print
												</a>
												
												<a class="btn btn-danger btn-xs" href="">
													<i class="fa fa-trash-o"></i> Delete
												</a>
											</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Table cell</td>
                                            <td>Table cell</td>
                                            <td>Table cell</td>
                                            <td>Table cell</td>
                                            <td style="width:250px;">
												<a class="btn btn-success btn-xs purple" href="">
													<i class="fa fa-pencil"></i> Edit
												</a>
												
												<a class="btn btn-success btn-xs" href="">
													<i class="fa fa-eye"></i> View
												</a>
												
												<a class="btn btn-success btn-xs" href="">
													<i class="fa fa-print"></i> Print
												</a>
												
												<a class="btn btn-danger btn-xs" href="">
													<i class="fa fa-trash-o"></i> Delete
												</a>
											</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- end col-md-12 -->

			</form>      
        </div>
	</div>
            <!-- End: Content -->  
</section>
    <!-- End: Content-Wrapper -->
<?php include 'footer.php'; ?>

