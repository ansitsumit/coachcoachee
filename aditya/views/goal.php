<?php include 'sidemenu.php';?>
    <!-- End: sidemenu -->
   
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper"
	<!-- Start: Topbar -->
	<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					<a href="#"> Add Goal </a>
				</li>
			</ol>
		</div>
	</header>
	<!-- End: Topbar -->

    <!-- Begin: Content -->
    <div id="content" class="animated fadeIn">
        <div class="row">
			<form class="form-horizontal" action="" method="post">

<!---========== Add test ==========================------------------------------------------->
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> Add Goal
							</span>
                        </div>
                        <div class="panel-body pn">
							<div class="col-md-12"> &nbsp; </div>
							<div class="col-md-6">
								<div class="admin-form">
									<input class="form-field-6" type="hidden"  VALUE="<?=$goal_data[0]->IDgoal?>" name="IDgoal">
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[33]?> : </label>
									<div class="col-lg-8">
									<textarea class="gui-textarea" name="Description" required="required">
									<?=$goal_data[0]->Description?> </textarea>
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[50]?> :</label>
									<div class="col-lg-8">
									<input  class="form-control" type="text" value="<?=$goal_data[0]->Evidence?>" name="Evidence">
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[51]?> :</label>
									<div class="col-lg-8">
									<input  class="form-control" type="text" value="<?=$goal_data[0]->motivators?>" name="motivators">
									</div>
									</div>
							
								</div>
							</div>

							<div class="col-md-6">
								<div class="admin-form">
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[52]?> :</label>
									<div class="col-lg-8">
									<input  class="form-control" type="text" value="<?=$goal_data[0]->saboteurs?>" name="saboteurs">
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[53]?>:</label>
									<div class="col-lg-8">
									<input  class="form-control" type="text" value="<?=$goal_data[0]->values?>"  name="values">
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[54]?> :</label>
									<div class="col-lg-8">
									<input  class="form-control" type="text" value="<?=$goal_data[0]->strategies?>" name="strategies">
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[55]?>:</label>
									<div class="col-lg-8">
									<input  class="form-control" type="text" value="<?=$goal_data[0]->resources?>"  name="resources">
									</div>
									</div>
									
									<div class="form-group" style="display:none;">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[46]?>:</label>
									<div class="col-lg-8">
									<input class="form-field-6 datepicker2" type="text" value="<?=$goal_data[0]->DataEntrega?>"  name="DataEntrega">
									</div>
									</div>
							
								</div>
							</div>
								
							<div class="col-md-12"> &nbsp; </div>
							<div class="col-md-6">
								<div class="form-group">
								<label class="col-lg-4 control-label"> &nbsp; </label>
								<div class="col-lg-4">
								<?php if($button==""){ ?>
								<button class="btn active btn-success btn-block" type="submit" name="goal">
								<i class="fa fa-save"></i>  <?=$lang[56]?> </button>
								</div>
								
								<div class="col-lg-4">
								<a href="<?=$this->config->base_url()?><?=$cancel?>" class="btn active btn-warning btn-block">
								<i class="fa fa-warning"></i> <?php echo $lang[422]?> </a>
								</div>
								<?php }else{?>
								
								<div class="col-lg-8">
								<a href="<?=$this->config->base_url()?><?=$cancel?>" class="btn active btn-warning btn-block">
								<i class="fa fa-warning"></i> <?php echo $lang[422]?> </a>
								</div>
								
								<?php }?>
								</div>	
								
							</div>
						</div>
							
							
                </div><!-- end col-md-12 -->		
			</form>      
        </div>
	</div>
            <!-- End: Content -->  
</section>
    <!-- End: Content-Wrapper -->

