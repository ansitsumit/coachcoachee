<?php include 'sidemenu.php';?>
    <!-- End: sidemenu -->
   
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper"
	<!-- Start: Topbar -->
	<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					<a href="#"> Add Video </a>
				</li>
			</ol>
		</div>
	</header>
	<!-- End: Topbar -->

    <!-- Begin: Content -->
    <div id="content" class="animated fadeIn">
        <div class="row">
			<form class="form-horizontal" action="" method="post">

<!---========== Add test ==========================------------------------------------------->
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> Add Video
							</span>
                        </div>
                        <div class="panel-body pn">
							<div class="col-md-12"> &nbsp; </div>
							<div class="col-md-6">
								<div class="admin-form">
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[302]?> :</label>
									<div class="col-lg-8">
									<input  class="form-control" type="text"  name="Title" value="<?php echo $edit_video[0]->Title ?>" required>
									</div>
									</div>
							
									<div class="form-group">
									<label class="col-lg-4 control-label"> <?=$lang[33]?>: </label>
									<div class="col-lg-8">
									<textarea class="gui-textarea" name="Description"> <?php echo $edit_video[0]->Description ?></textarea>
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-4 control-label"><?=$lang[301]?> :</label>
									<div class="col-lg-8">
									<input  class="form-control" type="text" name="Link" value="<?php echo $edit_video[0]->Link ?>" required>
									</div>
									</div>
							
									<div class="form-group">
									<label class="col-lg-4 control-label"> <?=$lang[303]?>: </label>
									<div class="col-lg-8">
									<textarea class="gui-textarea" name="Purpose"> <?php echo $edit_video[0]->Purpose ?></textarea>
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> &nbsp; </label>
									<div class="col-lg-4">
									<?php if(count($edit_video) > 0){?>
									<button class="btn active btn-success btn-block" type="submit" name="video">
									<i class="fa fa-save"></i>  <?php echo $lang[424]?> </button>
									<?php } else {?>
									<button class="btn active btn-success btn-block" type="submit" name="video">
									<i class="fa fa-save"></i>  <?php echo $lang[56]?> </button>
									<?php } ?> 
									</div>
									<div class="col-lg-4">
									<a href="<?php echo $this->config->base_url();?>index.php/toolbox" style="text-decoration:none;">
									<button class="btn active btn-warning btn-block" type="button">
									<i class="fa fa-warning"></i>
									<?php echo $lang[422]?>
									</button></a>
									</div>
									
									</div>	
									<div class="col-md-12"> 
									<br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> 
									<br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/>
									<br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/>
									</div>
								</div>
								
								</div>	
							</div>
							
							
                </div><!-- end col-md-12 -->		
			</form>      
        </div>
	</div>
            <!-- End: Content -->  
</section>
    <!-- End: Content-Wrapper -->

