<?php include 'sidemenu.php';?>
    <!-- End: sidemenu -->
   
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper">
	<!-- Start: Topbar -->
	<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					<a href="#"> Add Wheel </a>
				</li>
			</ol>
		</div>
	</header>
	<!-- End: Topbar -->

    <!-- Begin: Content -->
    <div id="content" class="animated fadeIn">
        <div class="row">
			<?php if(count($whell) > 0){?>
			<form class="form-horizontal" action="<?php echo $this->config->base_url();?>index.php/toolbox/add_wheel/<?php echo $whell[0]->IDwheel ?>" method="post"> 
			<?php } else {?>
			<form class="form-horizontal" action="<?php echo $this->config->base_url();?>index.php/toolbox/add_wheel" method="post"> 
			<?php } ?> 
			
<!---========== Add test ==========================------------------------------------------->
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> Add Wheel
							</span>
                        </div>
                        <div class="panel-body pn">
							<div class="col-md-12"> &nbsp; </div>
							<div class="col-md-6">
								<div class="admin-form">
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[58]?> :</label>
									<div class="col-lg-8">
									<input  class="form-control" type="text"  name="Name" value="<?php echo $whell[0]->Name ?>" required>
									</div>
									</div>
							
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?php echo $lang[33]?>: </label>
									<div class="col-lg-8">
									<textarea class="gui-textarea" name="Description"> 
									<?php echo $whell[0]->Description ?> </textarea>
									</div>
									</div>
								
								</div>	
							</div>
							
							<div class="col-md-12"> <hr/>  </div>
							<input type="hidden" value="0" id="theValue" />
							<div id="inc">
							<?php if(count($whell_child) > 0) {?>
							<?php for($i=0;$i<count($whell_child);$i++){?>
							<div class="col-md-6">
								<div class="admin-form">
								
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[58]?> :</label>
									<div class="col-lg-8">
									<input  class="form-control" type="text" name="Name1[]" value="<?php echo $whell_child[$i]->Name ?>" required>
									</div>
									</div>
							
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[33]?>: </label>
									<div class="col-lg-8">
									<textarea class="gui-textarea" name="Description1[]"> 
									<?php echo $whell_child[$i]->Description ?>
									</textarea>
									<input id="form-field-6" type="hidden" name="whell_child_id[]" value="<?php echo $whell_child[$i]->IDwheelitens ?>">
									</div>
									</div>
								
								</div>
								
							</div>
							
							<div class="col-md-12"> <hr/> </div>
							<?php } } else{ ?>
							<div class="col-md-6">
								<div class="admin-form">
								
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[58]?> :</label>
									<div class="col-lg-8">
									<input  class="form-control" type="text" name="Name1[]" value="<?php echo $whell_child[$i]->Name ?>" required>
									</div>
									</div>
							
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[33]?>: </label>
									<div class="col-lg-8">
									<textarea class="gui-textarea" name="Description1[]"> 
									<?php echo $whell_child[$i]->Description ?>
									</textarea>
									<input id="form-field-6" type="hidden" name="whell_child_id[]" value="<?php echo $whell_child[$i]->IDwheelitens ?>">
									</div>
									</div>
								
								</div>
								
							</div>
							<?php }  ?>
							<?php if(count($whell) > 0){?>
							<?php }else{?>
							<div class="col-md-12"> <hr/> </div>
							<?php }?>
							</div>	
							
							<div class="col-md-6">
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"> &nbsp; </label>
								<div class="col-lg-8">
								<?php if(count($whell) > 0){?>
								<button class="btn active btn-success" type="submit" name="wheel">
								<i class="fa fa-refresh"></i>  <?php echo $lang[424]?> </button>
								<?php } else {?>
								<button class="btn active btn-success" type="submit" name="wheel">
								<i class="fa fa-save"></i>  <?php echo $lang[56]?> </button>
								<?php } ?> 
								<a href="<?php echo base_url();?>index.php/toolbox/" class="btn active btn-warning">
								<i class="fa fa-warning"></i>
								<?php echo $lang[422]?>
								</a>
								
								<button class="btn active btn-success" id="append" type="button" onclick="addEvent();"> 
								<i class="fa fa-plus"></i>  <?php echo $lang[423]?> </button>
								
								</div>
							
                        </div>
                    </div>
                </div><!-- end col-md-12 -->		
			</form>      
        </div>
	</div>
            <!-- End: Content -->  
</section>
    <!-- End: Content-Wrapper -->

<script>
function addEvent() {
  var ni = document.getElementById('inc');
  var numi = document.getElementById('theValue');
  var num = (document.getElementById("theValue").value -1)+ 2;
  numi.value = num;
  var divIdName = "my"+num+"Div";
  var newdiv = document.createElement('div');
  newdiv.setAttribute("id",divIdName);

  newdiv.innerHTML = '<div class="row" style="margin-left:0px; margin-right:0px;"><div class="col-md-6"><div class="form-group"><label class="col-lg-4 control-label"><?=$lang[58]?> '+num+' :</label><div class="col-lg-8"><input class="form-control " type="text" name="Name1[]" value="" required></div></div><div class="control-group form-group"><label class="col-lg-4 control-label"><?=$lang[33]?> '+num+' :</label><div class="col-lg-8 "><textarea style="width:100%; height:96px;"class="gui-textarea" name="Description1[]" >  </textarea><input id="form-field-6" type="hidden" name="whell_child_id[]" value="insert"></div></div></div><div class="col-md-6"><a href="javascript:;" onclick="removeElement(\''+divIdName+'\')" class=" btn btn-small btn-info pull-right"> <i class="fa fa-power-off"></i> <?=$lang[207]?> </a></div> </div><div class="row"> <div class="col-md-12"> <hr/> </div></div>'; 
  ni.appendChild(newdiv);
}

function removeElement(divNum) {
  var d = document.getElementById('inc');
  var olddiv = document.getElementById(divNum);
  d.removeChild(olddiv);
}
</script>
