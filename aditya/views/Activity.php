<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/validate/validate.css">
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/validate/jquery.validate.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/validate/additional-methods.js"></script>
	
<?php include 'sidemenu.php'; $_SESSION['demo'] = $color[0]->Color; ?>
    <!-- End: sidemenu -->
   
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper">
	<!-- Start: Topbar -->
	<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					<a href="#"> Activity </a>
				</li>
			</ol>
		</div>
	</header>
	<!-- End: Topbar -->

    <!-- Begin: Content -->
    <div id="content" class="animated fadeIn">
        <div class="row">
			<form id="frmactivity" class="form-horizontal" role="form" method="post" action="" enctype="multipart/form-data">
				<h3 class=" smaller lighter green" style="margin-left:40%;"><?=$msg?></h3>
<!---========== Add test ==========================------------------------------------------->
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> Activity
							</span>
                        </div>
                        <div class="panel-body pn">
							<div class="col-md-12"> &nbsp; </div>
							<div class="col-md-6">
								<div class="admin-form">
									<input class="form-field-6" type="hidden" value="<?=$goal_data[0]->IDAtividade ?>" name="IDAtividade" >
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[36]?> :</label>
									<div class="col-lg-8">
									<input  class="form-control datepicker" type="text" value="<?=($goal_data[0]->StartDate)?>" id="StartDate" name="StartDate" required="required">
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[43]?> :</label>
									<div class="col-lg-8">
									<input  class="form-control" type="text" type="text" value="<?=$goal_data[0]->Activity ?>" name="Activity" required>
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[44]?> :</label>
									<div class="col-lg-8">
									<input  class="form-control" type="text" value="<?=$goal_data[0]->Pontuation ?>" name="Pontuation">
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[45]?>:</label>
									<div class="col-lg-8">
									<input  class="form-control" type="text" value="<?=$goal_data[0]->Objectives ?>" name="Objectives">
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-4 control-label"><?=$lang[37]?> :</label>
									<div class="col-lg-8">
									<input  class="form-control datepicker" type="text" value="<?=$goal_data[0]->Deadline?>" id="Deadline" name="Deadline"  required="required">
									</div>
									</div>
								
							
								</div>
							</div>

							<div class="col-md-6">
								<div class="admin-form">
								
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[33]?>: </label>
									<div class="col-lg-8">
									<textarea class="gui-textarea" name="Description"> <?=$goal_data[0]->Description?> </textarea>
									</div>
									</div>
									
									
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[47]?>: </label>
									<div class="col-lg-8">
									<label class="field select">
										<select name="GrauDificuldade" class="form-field-6" >
											<option></option>
											<option <?php if($goal_data[0]->GrauDificuldade=="very low"){ echo "selected";} ?> >very low</option>
											<option <?php if($goal_data[0]->GrauDificuldade=="low"){ echo "selected";} ?>>low</option>
											<option <?php if($goal_data[0]->GrauDificuldade=="normal"){ echo "selected";} ?>>normal</option>
											<option <?php if($goal_data[0]->GrauDificuldade=="high"){ echo "selected";} ?>>high</option>
											<option <?php if($goal_data[0]->GrauDificuldade=="very high"){ echo "selected";} ?>>very high </option>
										</select>
										<i class="arrow double"></i>
									</label>
									</div>
									</div>
									
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[30]?> : </label>
									<div class="col-lg-8">
									<label class="field select">
										<select name="goal" class="form-field-6" required>

											<option></option>
											<?php 
											for($r=0;$r<count($goalw);$r++){
												$check="";
												if($goalw[$r]->IDgoal==$goal_data[0]->goal){
													$check="selected";
													
												}
											?>
											<option value="<?=$goalw[$r]->IDgoal?>" <?=$check?>><?=$goalw[$r]->IDgoal?></option>
											<?php }?>
										</select>
										<i class="arrow double"></i>
									</label>
									</div>
									</div>
							
								</div>
							</div>
							
								
							<div class="col-md-12"> &nbsp; </div>
							
							<div class="col-md-6">
								<div class="admin-form">
									<div class="form-group">
										<label class="col-lg-4 control-label" for="inputStandard">&nbsp </label>
										<div class="col-lg-8">
										<?php if($view!="view"){
										if($goal_data[0]->IDAtividade>0){ $but="Update"; }else{ $but=$lang[56]; //$but="Submit";
										}
										?>
										<button class="btn active btn-success" type="submit" name="activities">
										<i class="fa fa-refresh"></i>  <?=$but?> </button>
								
										<a href="<?=$this->config->base_url()?><?=$cancel?>" >
										<button type="button" class="btn active btn-warning">
										<i class="fa fa-warning"></i> <?=$lang[422]?> </button></a>

										<?php }else{?>
								
										<a href="<?=$this->config->base_url()?><?=$cancel?>">
										<button type="button" class="btn active btn-warning">
										<i class="fa fa-warning"></i> <?=$lang[422]?> </button></a>
								
										<?php }?>
										
										
										</div>
									</div>
								</div>
							</div>
						</form>	
							<!--###### Conversatiuon #################################------------>	
		
	<?php if($view==""){
if($goal_data[0]->IDAtividade>0){?>
<form action="" method="post">	
	<input class="form-field-6" type="hidden" value="<?=$goal_data[0]->IDAtividade ?>" name="IDAtividade" >
	<input class="form-field-6" type="hidden" value="<?=$goal_data[0]->IDCoach?>" name="IDCoach" >
	<input class="form-field-6" type="hidden" value="<?=$goal_data[0]->IDCoachee ?>" name="IDCoachee" >
	<input class="form-field-6" type="hidden" value="coach@<?=$_SESSION['coach'] ?>" name="Response" >
					<div class="col-md-10 col-sm-offset-2">			 								
					<div class="row-fluid">
					<div class="span8 ">
					<link href="<?=$this->config->base_url()?>assets/style_chat.css" 
					rel="stylesheet" type="text/css"/>
                         <div class="portlet">
						<div class="panel-headingcolor">
							<div class="caption">
								<i class="fa fa-comments"></i> Conversations
							</div>
							
						</div>
						<div class="portlet-body" id="chats">
							<div >
							<input type="hidden" name="email" value="<?=$email[0]->Email1?>">
							<div  style="height: 200px; overflow: scroll;" data-always-visible="1" data-rail-visible1="1">
								<ul class="chats">
								<?php
								for($dis=0;$dis<count($discussion);$dis++){
								$response=explode("@",$discussion[$dis]->Response);
								if($response[0]=="coach"){
								
								$name=$discussion[$dis]->coach_Name;
								$align="out";
								 $image=$discussion[$dis]->coach_Photo;
								
								}
								else{
								$name=$discussion[$dis]->coachee_Name;
								$align="in";
								 $image=$discussion[$dis]->coachee_Photo;
								}
								?>
									<li class="<?=$align?>">
										<img class="avatar img-responsive" alt="" src="<?=$this->config->base_url()?>coach_images/<?=$image?>">
										<div class="message">
											<span class="arrow">
											</span>
											<span  class="name"><?=stripslashes($name)?></span>
											<span class="datetime">
												 at <?=date("Y-m-d h:i A",strtotime($discussion[$dis]->Date." ".$discussion[$dis]->Time))?>
											</span>
											<span class="body">
										<?=stripslashes($discussion[$dis]->Discussion)?>	</span>
										</div>
									</li>
									<?php }?>
									</ul>
							</div>
							</div>
							<div class="chat-form">
								<div class="input-cont">
									<input class="form-control" name="msg" placeholder="Type a message here..." type="text" style="height:35px;" required>
								</div>
								<div class="btn-cont" >
									
									<input type="submit" style="height:35px;" name="save_discuuss" class="btn btn-system btn-small" value="Send">
								</div>
							</div>
						</div>
					</div>	
									</form>	
										
										 
</div>
</div> 
</div> 
<?php }}?>
							
							
						</div>
							
						
							
                </div><!-- end col-md-12 -->		
			   
        </div>
	</div>
            <!-- End: Content -->  
</section>
    <!-- End: Content-Wrapper -->

<script>
	$(function(){
		$("#StartDate").datepicker({
			minDate: '0' //<----here
		});		
		$("#Deadline").datepicker({
			onSelect: function(){
			if ($(this).val() <  $("#StartDate").val()) {
				alert("Deadline date can't be less than start date");
				$(this).val('');
				$(this).focus();
			}				
			},
			minDate: '0' //<----here
		});
		
		//$("#Deadline").on('blur',function(){
		//	if ($(this).val() <  $("#StartDate").val()) {
		//		alert("Deadline date can't be less than start date");
		//		$(this).focus();
		//	}				
		//
		//});
		
		
		$('#frmactivity').validate();
		
	});
</script>