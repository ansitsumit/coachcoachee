	<!--jqueryvalidation plugin-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/validate/validate.css">
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/validate/jquery.validate.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/validate/additional-methods.js"></script>
	
	
	<script>
		$(document).ready(function(){
			$('#frmcoacheemydata').validate({
				rules: {
					confpassword:{
						equalTo: "#Password"
					},
					
					
				},
				messages: {
					confpassword: 'Password & confirm password should be same'
				}
				
			})
		});
	</script>

<?php include ('sidemenu.php');?>
    <!-- End: sidemenu -->
   
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper"
	<!-- Start: Topbar -->
	<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					<a href="#">My Data</a>
				</li>
			</ol>
		</div>
	</header>
	<!-- End: Topbar -->

    <!-- Begin: Content -->
    <div id="content" class="animated fadeIn">
        <div class="row">
			<?php //print_r($get_data_coachee)?>
			<form id="frmcoacheemydata" class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                <h3  style="color:green" align="center"> <?=$msg?> </h3>
                <div class="col-md-12"> 
					<div class="col-md-4">
						<div class="fileupload fileupload-new admin-form" data-provides="fileupload">
							<div class="fileupload-preview thumbnail mb20">
								<img id="avatar" src="<?=$this->config->base_url();?>coach_images/<?=$get_data_coachee[0]->Photo?>">
							</div>
							<div class="row">
								
								<div class="col-xs-5">
									<span class="button btn-system btn-file btn-block">
										<span class="fileupload-new"><?=$lang[57]?></span>
									<span class="fileupload-exists"><?=$lang[57]?> </span>
									<input id="form-field-6" type="file" name="photo"  >
									<input id="form-field-6" type="hidden" name="photo1" value="<?=$get_data_coachee[0]->Photo?>" >
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				
                <div class="col-md-6">
					<div class="panel panel-primary panel-border top mt20 mb35">
                        <div class="panel-body bg-light dark">
                            <div class="admin-form">
                               
                                <div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[58]?> :</label>
								<div class="col-lg-8">
								<input class="form-control" type="text" name="Name" value="<?=$get_data_coachee[0]->Name?>" required="required">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[59]?>  :</label>
								<div class="col-lg-8">
								<input  class="form-control" type="email"  value="<?=$get_data_coachee[0]->Email1?>" name="Email1" required="required">
								</div>
								</div>
								
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[60]?> :</label>
								<div class="col-lg-8">
								<input  class="form-control" type="email" name="Email2" value="<?php echo $coach_info[0]->Email2?>">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label"><?=$lang[61]?> :</label>
								<div class="col-lg-8">
								<input id="Password"  class="form-control" type="password" value="<?=$get_data_coachee[0]->Password?>" name="Password">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"> Confirm Password: </label>
								<div class="col-lg-8">
								<input id="confpassword" class="form-control" type="password" value="" name="confpassword" required="required">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[62]?>  :</label>
								<div class="col-lg-8">
								<label class="field select">
									<select id="form-field-6"  name="CivilState">
										<option> </option>
										<?php
										for($r=0;$r<count($civil_staes);$r++){
										$check="";         

										if($get_data_coachee[0]->CivilState==$civil_staes[$r]->id){
										$check="selected";

										}
										?><option value="<?=$civil_staes[$r]->id?>" <?=$check?>><?=$civil_staes[$r]->name?></option>
										<?php }?>

									</select>
									<i class="arrow double"></i>
								</label>
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[63]?>  :</label>
								<div class="col-lg-8">
								<label class="field select">
									<select id="form-field-6" type="text" name="Education">
										<option></option>
										<?php
										for($r=0;$r<count($edu);$r++){
										$check="";         

										if($get_data_coachee[0]->Education==$edu[$r]->ID){
										$check="selected";

										}
										?><option value="<?=$edu[$r]->ID?>" <?=$check?>><?=$edu[$r]->Name?></option>
										<?php }?>
									</select>
									<i class="arrow double"></i>
								</label>
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[64]?> :</label>
								<div class="col-lg-8">
								<label class="field select">
									<select id="form-field-6" type="text" name="HierachicalLevel">
										<option></option>
										<?php
										for($r=0;$r<count($hierachi);$r++){
										$check="";         

										if($get_data_coachee[0]->HierachicalLevel==$hierachi[$r]->ID){
										$check="selected";

										}
										?><option value="<?=$hierachi[$r]->ID?>" <?=$check?>><?=$hierachi[$r]->Name?></option>
										<?php }?>

									</select>
									<i class="arrow double"></i>
								</label>
								</div>
								</div>
								
								
								
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[65]?> :</label>
								<div class="col-lg-8">
								<input class="form-control datepicker2" type="text" value="<?=$get_data_coachee[0]->Birth?>" name="Birth"  readonly>
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[66]?> :</label>
								<div class="col-lg-8">
								<input class="form-control datepicker2" type="text" value="<?=$get_data_coachee[0]->Observations?>" name="Observations">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[67]?> :</label>
								<div class="col-lg-8">
								<input class="form-control" type="text" value="<?=$get_data_coachee[0]->ObjetivosDoCoaching?>" name="ObjetivosDoCoaching">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[81]?> :</label>
								<div class="col-lg-8">
								<input class="form-control" type="text" name="DataInicioCoaching" value="<?=$get_data_coachee[0]->DataInicioCoaching?>" readonly>
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[68]?> :</label>
								<div class="col-lg-8">
								<input class="form-control" type="text" name="DataFimCoaching" value="<?=$get_data_coachee[0]->DataFimCoaching?>" readonly>
								</div>
								</div>
								
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[48]?> </label>
								<div class="col-lg-8">
								<textarea class="gui-textarea" name="Notes" readonly><?=$get_data_coachee[0]->Notes?> </textarea>
								</div>
								</div>
                                
                            </div>
                        </div>
                    </div>
                </div> <!--close col-sm-6-->
				
				<div class="col-md-6">
					<div class="panel panel-primary panel-border top mt20 mb35">
                        <div class="panel-body bg-light dark">
                            <div class="admin-form">
							
                                <div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[70]?>:</label>
								<div class="col-lg-8">
								<input  class="form-control" type="text" value="<?=$get_data_coachee[0]->Address?>" name="Address">
								</div>
								</div>
								
                                <div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[71]?> :</label>
								<div class="col-lg-8">
								<input class="form-control" type="text" value="<?=$get_data_coachee[0]->Neighborhood?>" name="Neighborhood">
								</div>
								</div>
							
                                <div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[72]?> :</label>
								<div class="col-lg-8">
								<input class="form-control" type="text" value="<?=$get_data_coachee[0]->City?>" name="City">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[73]?> :</label>
								<div class="col-lg-8">
								<label class="field select">
									<select id="form-field-6" type="text" name="Country">
										<option></option>

										<?php
										for($r=0;$r<count($country);$r++){
										$check="";         

										if($get_data_coachee[0]->Country==$country[$r]->CountryID){
										$check="selected";

										}
										?><option value="<?=$country[$r]->CountryID?>" <?=$check?>><?=$country[$r]->CountryName?></option>
										<?php }?>

									</select>
									<i class="arrow double"></i>
								</label>
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[74]?> :</label>
								<div class="col-lg-8">
								<input class="form-control" type="text" value="<?=$get_data_coachee[0]->ZipCode?>" name="ZipCode" onkeyup="this.value=this.value.replace(/[^0-9\.]/g,'');">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[75]?> :</label>
								<div class="col-lg-8">
								<input  class="form-control" type="text" value="<?=$get_data_coachee[0]->PhoneResidential?>" name="PhoneResidential" onkeyup="this.value=this.value.replace(/[^0-9\.]/g,'');">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[76]?> :</label>
								<div class="col-lg-8">
								<input class="form-control" type="text" value="<?=$get_data_coachee[0]->PhoneCommercial?>" name="PhoneCommercial" onkeyup="this.value=this.value.replace(/[^0-9\.]/g,'');">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[77]?> :</label>
								<div class="col-lg-8">
								<input  class="form-control" type="text" value="<?=$get_data_coachee[0]->Mobile?>" name="Mobile" onkeyup="this.value=this.value.replace(/[^0-9\.]/g,'');">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[78]?> :</label>
								<div class="col-lg-8">
								<input class="form-control" type="text" value="<?=$get_data_coachee[0]->Document?>" name="Document">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[79]?> :</label>
								<div class="col-lg-8">
								<input  class="form-control" type="text" value="<?=$get_data_coachee[0]->Profession?>" name="Profession">
								</div>
								</div>
								
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"><?=$lang[80]?> :</label>
								<div class="col-lg-8">
								<input class="form-control" type="text" value="<?=$get_data_coachee[0]->Company?>" name="Company">
								</div>
								</div>
								
								<div class="form-group" style="<?=$style?>">
								<label class="col-lg-4 control-label" for="inputStandard"> <?=$lang[69]?> :</label>
								<div class="col-lg-8">
								<input class="form-control" name="score" value="<?=$total_score[0]->total?>" readonly >
								</div>
								</div>
								
								<div class="control-group" style="display:none;">
								<label class="control-label" for="form-field-6"> <?=$lang[82]?> :</label>
								<div class="controls">
								<select id="form-field-6" type="text" name="Status">
								<option value="1" <? if($get_data_coachee[0]->Status=="1"){echo "selected";}?> > Active </option>
								<option value="0" <? if($get_data_coachee[0]->Status=="0"){echo "selected";}?>> Inactive </option>
								</select>
								</div>
								</div>

								<div class="control-group" style="visibility:hidden;">
								<label class="control-label" for="form-field-6"> <?=$lang[83]?> :</label>
								<div class="controls">
								<input id="form-field-6" type="text"  value="<?=count($Sessions)?>" readonly>
								</div>
								</div>	

								<div class="control-group" style="display:none;">
								<label class="control-label" for="form-field-6"><?=$lang[84]?> :</label>
								<div class="controls">
								<input id="form-field-6" type="text"  name="SessoesFeitas" value="<?=$get_data_coachee[0]->SessoesFeitas?>" readonly>
								</div>
								</div>	
								
                            </div>
                        </div>
                    </div>
                </div>
				<div class="col-md-12" align="center">
					<?php
					for($r=0;$r<count($button);$r++){
					echo $button[$r][0]."&nbsp;";
					}

					?>
				</div>	
					
			</form>      
        </div>
	</div>
            <!-- End: Content -->  
</section>
    <!-- End: Content-Wrapper -->


