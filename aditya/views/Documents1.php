<?php include ('sidemenu.php');?>
    <!-- End: sidemenu -->
   
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper"
	<!-- Start: Topbar -->
	<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					<a href="dashboard.html">Documents</a>
				</li>
			</ol>
		</div>
	</header>
	<!-- End: Topbar -->

    <!-- Begin: Content -->
    <div id="content" class="animated fadeIn">
        <div class="row">
			<form class="form-horizontal" role="form">
              
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span><?=$lang[12]?> 
							</span>
                        </div>
                        <div class="panel-body pn">
                            <div class="table-responsive" style="height:900px; overflow:auto;">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th style="width:30%;"><?=$lang[93]?></th>
											<th style="width:60%;"><?=$lang[33]?></th>
											<th style="width:10%;"><?=$lang[446]?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?
										if($list_documents>0){
										for($r=0;$r<count($list_documents);$r++){
										?><tr>
											<td><?=$list_documents[$r]->FileName?></td>
											<td><?=$list_documents[$r]->Description?></td>
											<td><a target="_blank" class="btn btn-system btn-xs" href="<?=$this->config->base_url();?>coach_images/<?=$list_documents[$r]->file?>"> <i class="fa fa-download"></i> 
											<?=$lang[446]?>  </a></td>
											
										
											
										</tr>
										<?php }} else{?>
										<tr> <td colspan="3" style="text-align:center;"> Document(s) not found </td> </tr>
										<?php }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- end col-md-12 -->
				
			</form>      
        </div>
	</div>
            <!-- End: Content -->  
</section>
    <!-- End: Content-Wrapper -->




